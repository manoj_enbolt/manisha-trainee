<?php
	require 'vendor/autoload.php';

	$session = new SpotifyWebAPI\Session(
	    '63d9025b55c442e7bc997c3da69270d5',
	    '23e1df1005fc4adba455314fcabc5af4',
	    'http://localhost:9000'
	);

	// Request a access token using the code from Spotify
	$session->requestAccessToken($_GET['code']);

	$accessToken = $session->getAccessToken();
	$refreshToken = $session->getRefreshToken();

	// Store the access and refresh tokens somewhere. In a database for example.

	// Send the user along and fetch some data!
	header('Location: app.php');
	die()
?>