<?php
	require 'vendor/autoload.php';

	$session = new SpotifyWebAPI\Session(
	    '63d9025b55c442e7bc997c3da69270d5',
	    '23e1df1005fc4adba455314fcabc5af4',
	    'http://localhost:9000'
	);
	$api = new SpotifyWebAPI\SpotifyWebAPI();

	if (isset($_GET['code'])) {
	    $session->requestAccessToken($_GET['code']);
	    $api->setAccessToken($session->getAccessToken());
	    $_SESSION['token'] = $session->getAccessToken();

	    print_r($api->createPlaylist(['name' => 'Samjavvan']));
	  	$api->addMyAlbums(['2tNLI0zZIICrQRlC4c3CVn', '48JYNjh7GMie6NjqYHMmtT', '27cZdqrQiKt3IT00338dws']);
	  	$preview = $api->getTrack('4FeczSomVWVyU4FW7xDeAI');
	  	$preview = $preview->preview_url;
	  	header('Location: preview_song.php');

	} else {
	    $options = [
	        'scope' => [
	            'user-read-email',
	            'playlist-modify-public',
	            'playlist-modify-private',
	            'user-follow-read',
	            'user-library-modify',
	        ],
	    ];
	    header('Location: ' . $session->getAuthorizeUrl($options));
	    die();
	}
	// function addPlaylist() {
	// 	$api = new SpotifyWebAPI\SpotifyWebAPI();
	// 	$api->setAccessToken($_SESSION['token']);
	// 	$api->createPlaylist(['name' => 'jina jina']);
	// }
	
	// function addtrackMusic() {
	// 	$api = new SpotifyWebAPI\SpotifyWebAPI();
	// 	$api->setAccessToken($_SESSION['token']);
	// 	$api->addPlaylistTracks('3mxjwbrp7t2XoqvtSwGOzx', '4nPK0xA25U7zbO4RNmndJk');
	// }
	// addtrackMusic()
	// addPlaylist()
?>