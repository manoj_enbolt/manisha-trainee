<?php

	Route::get('/', function () {
	    return view('welcome');
	});
	
	Auth::routes();

	// Route::group(['middleware' => 'disablepreventback'], function() {

		Route::get('/user/edit/{id}', 'RegistrationController@edit');
		Route::patch('user/update/{id}','RegistrationController@update');
		Route::get('/view/profile', 'Admin\AdminController@showProfile');
		Route::get('all/employee/show/holiday/schedule', 'RegistrationController@ShowHolidaySchedule');

		Route::group(['middleware' => 'admin','prefix' => 'admin'], function(){
			Route::get('/user/role', 'RegistrationController@create');
			Route::get('/register', 'RegistrationController@register');
			Route::post('/user/store','RegistrationController@store');
			Route::get('/dashboard', 'Admin\AdminController@showAdminDashboard');
			Route::get('/manage/employee', 'Admin\AdminController@adminManageEmployee');
			Route::get('/view/manage/employee', 'Admin\AdminController@viewManageEmployee');
			Route::get('/view/all/employee/profile', 'Admin\AdminController@viewAllEmployeeProfile');
			Route::get('/assign/teamleader/employee/{id}', 																'Admin\AdminController@assignTeamLeaderToEmployee');
			Route::patch('/assign/teamleader', 'Admin\AdminController@assignTeamLeader');
			Route::get('/manage/holiday', 'Admin\AdminController@adminManageHoliday');
			Route::get('/create/holiady', 'Admin\AdminController@adminCreateHoliday');
			Route::post('/holiday/store', 'Admin\AdminController@holidayStore');
			Route::get('/show/holiday/schedule', 'Admin\AdminController@adminShowHolidaySchedule');
			Route::get('/edit/holiday/{id}', 'Admin\AdminController@editHoliday');
			Route::patch('/holiday/update/{id}','Admin\AdminController@holidayUpdate');
			Route::delete('/holiday/delete/{id}', 'Admin\AdminController@deleteHoliday');
			Route::get('/show/reamilng/total/leave', 'Admin\AdminController@adminShowRemainingLeave');
		});

		Route::group(['middleware' => 'teamleader','prefix' => 'teamleader'], function(){
			Route::get('/dashboard', 'Teamleader\TeamLeaderController@showTeamleaderDashboard');
			Route::get('/manage/employee', 'Teamleader\TeamLeaderController@teamleaderManageEmployee'); 
			Route::get('/show/employeelist', 'Teamleader\TeamLeaderController@teamleaderShowEmployeeList');
			Route::get('/show/employee/leave/list','
						Teamleader\TeamLeaderController@teamleaderShowEmployeeLeaveList'); 
			Route::get('/leave/response/{id}', 'Teamleader\TeamLeaderController@giveLeaveResponseToEmployee');
			Route::get('show/teamleader/leave/response/{id}',															'Teamleader\TeamLeaderController@showgiveLeaveResponseToEmployee');
			Route::patch('/give/leave/response', 																		'Teamleader\TeamLeaderController@teamleaderGiveResponseLeaveToEmployee');
			Route::get('/apply/leave', 'Teamleader\TeamLeaderController@teamleaderLeaveApply');
			Route::post('leave/store', 'Teamleader\TeamLeaderController@teamleaderStoreLeave');
			Route::get('/view/pending/leave/list', 																		'Teamleader\TeamLeaderController@showTeamleaderPendingLeave');
			Route::get('/show/leave/panding/alldata/{id}', 																'Teamleader\TeamLeaderController@teamleaderShowPendingLeaveAllData');
			Route::get('/view/response/leave', 'Teamleader\TeamLeaderController@showResponseLeave');
			Route::get('/show/own/leave/response/{id}', 																'Teamleader\TeamLeaderController@showMyLeaveResponse');
			Route::get('/leave/managment', 'Teamleader\TeamLeaderController@showPendingLeave');
		});
		
		Route::group(['middleware' => 'hr','prefix' => 'hr'], function(){
			Route::get('/dashboard', 'HR\HRController@showHRDashboard');
			Route::get('/give/teamleader/leave/response/{id}', 														'HR\HRController@giveLeaveResponseToTeamLeader');
			Route::get('/leave/response/{id}', 'HR\HRController@giveLeaveResponseToEmployee');
			Route::patch('/give/leave/response/employee', 															'HR\HRController@hrGiveResponseLeaveToEmployee');
			Route::patch('/give/leave/response/teamleader', 															'HR\HRController@hrGiveResponseLeaveToTeamLeader');
			Route::get('/employee/attach/document/list', 'HR\HRController@employeeAttachDocumentList');
			Route::get('/attach/employee/document/{id}', 'HR\HRController@hrAttachEmployeeDocument');
			Route::post('/attachment/form/store', 'HR\HRController@attachmentFormStore');
			Route::get('/show/employee/document/list', 'HR\HRController@showEmployeeDocumentList');
			Route::get('/show/attach/employee/document/{id}', 'HR\HRController@showAttachEmployeeDocument');
			Route::get('leave/managment','HR\HRController@showLeaveManagmentMenu');
			Route::get('/employee/pending/leave/list', 'HR\HRController@hrShowEmployeePendingLeaveList');
			Route::get('/teamleader/pending/leave/list', 'HR\HRController@hrShowTeamleaderPendingLeaveList');
		});

		Route::group(['middleware' => 'employee','prefix' => 'employee'], function(){
			Route::get('/dashboard', 'Employee\EmployeeController@showEmployeeDashboard');
			Route::get('/apply/leave', 'Employee\EmployeeController@leaveApply'); 
			Route::post('/leave/store', 'Employee\EmployeeController@storeLeave');
			Route::get('/view/pending/leave/list', 																		'Employee\EmployeeController@showEmployeePendingLeave');
			Route::get('/view/response/leave', 'Employee\EmployeeController@showResponseLeave');
			Route::get('/pending/leave/edit/{id}', 'Employee\EmployeeController@employeeEditLeave');
			Route::patch('/leave/update/{id}', 'Employee\EmployeeController@employeeLeaveUpdate');
			Route::get('/show/leave/panding/alldata/{id}', 																'Employee\EmployeeController@employeeShowPendingLeaveAllData');
			Route::get('/leave/response/{id}', 'Employee\EmployeeController@showMyLeaveResponse');
			Route::get('/show/my/document/menu', 'Employee\EmployeeController@employeeShowDocumentMenu');
			Route::get('/show/own/document', 'Employee\EmployeeController@employeeShowOwnDocument');
			Route::get('/edit/own/document/{id}', 'Employee\EmployeeController@employeeEditOwnDocument');
			Route::patch('/document/update/{id}', 'Employee\EmployeeController@employeeDocumentUpdate');
			Route::get('/ssc_marksheet/document/download/{id}', 'Employee\EmployeeController@employeeSSCDocumentDownload');
			Route::get('/hsc_marksheet/document/download/{id}', 'Employee\EmployeeController@employeeHSCMarksheetDownload');
			Route::get('/certificate/document/download/{id}', 'Employee\EmployeeController@employeeCertificateDownload');
			Route::get('/electioncard/document/download/{id}', 'Employee\EmployeeController@employeeElectioncardDownload');
			Route::get('/aadharcard/document/download/{id}', 'Employee\EmployeeController@employeeAadharCardDownload');
			Route::get('/pancard/document/download/{id}', 'Employee\EmployeeController@employeePancardDownload');
		});
		Route::get('/home', 'HomeController@index')->name('home');
	// });