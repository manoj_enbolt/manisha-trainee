@extends('head')
@section('content')
  @if(!$document)
    <div class="app-content content" style="padding-top: 70px; padding-left: 40px;">
    <div class="content-wrapper">
      <div class="content-body"><!-- ../../../theme-assets/images/carousel/22.jpg -->
        <section id="header-footer"><!-- Header footer section start -->
          <div class="row">
            <div class="col-xl-12">
              <div class="card card-statistics">
                <div class="card-body">
                  <div class="clearfix">
                    <div class="float-left">
                       <h4 class="card-title">Ooops...!!!!.<br><br>Sorry...No Any Document Data Available Here..!!!!</h4>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
      </div>
    </div>
  </div>
  @else
  <div class="app-content content" style="padding-top: 70px; padding-left: 40px;">
    @if(Session::has('success'))
        <div class="alert alert-success alert-dismissible" role="alert">
            {!! Session::get('success') !!}
        </div>
      @endif
    <div class="content-wrapper">
      <div class="content-body"><!-- ../../../theme-assets/images/carousel/22.jpg -->
        <section id="header-footer"><!-- Header footer section start -->
          <div class="row">
            <div class="col-xl-3 col-lg-3 col-md-3 col-sm-6 grid-margin stretch-card">
              <div class="card card-statistics">
                <div class="card-body">
                  <div class="clearfix">
                    <div class="float-left">
                      <h3><i class="ft-eye"></i></h3>
                    </div>
                    <div class="float-right">
                      <h4 class="mb-0 text-right"> Show My Document</h4>
                      <div class="fluid-container">
                        <p class="font-weight-medium text-right mb-0">Employee Document</p>
                      </div>
                    </div>
                  </div>
                   <p class="text-muted mt-3 mb-0">
                    <a href="{{ url('/employee/show/own/document')  }}">
                      <i class="mdi mdi-alert-octagon mr-1" aria-hidden="true"></i>Show MY Documents
                    </a>
                  </p>
                </div>
              </div>
            </div>
            <div class="col-xl-3 col-lg-3 col-md-3 col-sm-6 grid-margin stretch-card">
              <div class="card card-statistics">
                <div class="card-body">
                  <div class="clearfix">
                    <div class="float-left">
                      <h3><i class="ft-edit"></i></h3>
                    </div>
                    <div class="float-right">
                      <h4 class="mb-0 text-right">Edit Documents</h4>
                      <div class="fluid-container">
                        <p class="font-weight-medium text-right mb-0">Change Document Data</p>
                      </div>
                    </div>
                  </div>
                  <p class="text-muted mt-3 mb-0">
                    <a href="{{ url('/employee/edit/own/document/'.$document->id) }}">
                      <i class="mdi mdi-alert-octagon mr-1" aria-hidden="true"></i> Change MY Document
                    </a>
                  </p>
                </div>
              </div>
            </div>
          </div>
        </section>
      </div>
    </div>
  </div>
  @endif
@endsection