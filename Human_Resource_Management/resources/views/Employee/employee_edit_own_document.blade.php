@extends('head')
@section('content')
   <div class="content" style="padding-top: 80px; padding-left: 50px;">
      @if(Session::has('success'))
        <div class="alert alert-success alert-dismissible" role="alert">
            {!! Session::get('success') !!}
        </div>
      @endif
        <div class="container-fluid">
          <div class="row">
            <div class="col-md-8">
              <div class="card">
                <div class="card-header card-header-primary">
                  <h4 class="card-title">Edit My  Documnts</h4>
                  <p class="card-category">Enter Details below..</p><hr>
                </div>
                <div class="card-body">
                  {!! Form::model($document, ['method' => 'PATCH', 'url' => '/employee/document/update/'.$document->id,'files' => true]) !!}
                  {{ Form::hidden('id', $id) }}
                    <div class="row">
                      <div class="col-md-6">
                        <div class="form-group">
                          <p class="font-weight-medium  mb-0">Name : {{ $user->first_name }} </p>
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="form-group">
                          <p class="font-weight-medium  mb-0">Email : {{ $user->email }}</p>
                        </div>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-6">
                        <div class="form-group" {{ $errors->has('ssc_marksheet') ? ' is-invalid' : '' }}">
                          <label class="bmd-label-floating"> Attach S.S.C Marksheet</label>
                          {!! Form::file('ssc_marksheet',['class'=>'form-control', 'placeholder'=>'Attach AadharCard']) !!}
                          {!! $errors->first('ssc_marksheet','<span class="help-block" style="color:red;">:message</span>') !!}
                        </div>
                      </div>
                        <div class="col-md-6">
                          <img style="width: 100px; height: 100px; margin-left: 30px;" alt="{{ $document->ssc_marksheet }}" src="{{ asset('/storage/upload').'/'.$document->ssc_marksheet }}">
                        </div>
                      </div><br>
                    <div class="row">
                      <div class="col-md-6">
                        <div class="form-group" {{ $errors->has('hsc_marksheet') ? ' is-invalid' : '' }}">
                          <label class="bmd-label-floating">Attach H.S.C Marksheet</label>

                          {!! Form::file('hsc_marksheet',['class'=>'form-control', 'placeholder'=>'Attach H.S.C Marksheet']) !!}
                          {!! $errors->first('hsc_marksheet','<span class="help-block" style="color:red;">:message</span>') !!}
                        </div>
                      </div>
                      <div class="col-md-6">
                         <img style="width: 100px; height: 100px; margin-left: 30px;" alt="{{ $document->hsc_marksheet }}" src="{{ asset('/storage/upload').'/'.$document->hsc_marksheet }}">
                        </div>
                      </div><br>
                    <div class="row">
                       <div class="col-md-6">
                        <div class="form-group" {{ $errors->has('certificate') ? ' is-invalid' : '' }}">
                          <label class="bmd-label-floating">Attach Other Degree Certificate</label>

                          {!! Form::file('certificate',['class'=>'form-control', 'placeholder'=>'Attach Other Degree Certificate']) !!}
                          {!! $errors->first('certificate','<span class="help-block" style="color:red;">:message</span>') !!}
                        </div>
                      </div>
                       <div class="col-md-6">
                        <img style="width: 100px; height: 100px; margin-left: 30px;" alt="{{ $document->certificate }}" src="{{ asset('/storage/upload').'/'.$document->certificate }}">
                        </div>
                      </div><br>
                     <div class="row">
                      <div class="col-md-6">
                        <div class="form-group" {{ $errors->has('election_card') ? ' is-invalid' : '' }}">
                          <label class="bmd-label-floating">Attach Election Card</label>
                          {!! Form::file('election_card',['class'=>'form-control', 'placeholder'=>'Profile Image']) !!}
                          {!! $errors->first('election_card','<span class="help-block" style="color:red;">:message</span>') !!}
                        </div>
                      </div>
                      <div class="col-md-6">
                        <img style="width: 100px; height: 100px; margin-left: 30px;" alt="{{ $document->election_card }}" src="{{ asset('/storage/upload').'/'.$document->election_card }}">
                        </div>
                      </div><br>
                    <div class="row">
                      <div class="col-md-6">
                        <div class="form-group" {{ $errors->has('aadhar_card') ? ' is-invalid' : '' }}">
                          <label class="bmd-label-floating"> Attach Aadhar Card</label>
                          {!! Form::file('aadhar_card',['class'=>'form-control', 'placeholder'=>'Attach AadharCard']) !!}
                          {!! $errors->first('aadhar_card','<span class="help-block" style="color:red;">:message</span>') !!}
                        </div>
                      </div>
                      <div class="col-md-6">
                       <img style="width: 100px; height: 100px; margin-left: 30px;" alt="{{ $document->aadhar_card }}" src="{{ asset('/storage/upload').'/'.$document->aadhar_card }}">
                        </div>
                      </div><br>
                    <div class="row">
                      <div class="col-md-6">
                        <div class="form-group" {{ $errors->has('pancard') ? ' is-invalid' : '' }}">
                          <label class="bmd-label-floating">Attach PanCard</label>
                          {!! Form::file('pancard',['class'=>'form-control', 'placeholder'=>'Attach PanCard']) !!}
                          {!! $errors->first('pancard','<span class="help-block" style="color:red;">:message</span>') !!}
                        </div>
                      </div>
                       <div class="col-md-6">
                       <img style="width: 100px; height: 100px; margin-left: 30px;" alt="{{ $document->pancard }}" src="{{ asset('/storage/upload').'/'.$document->pancard }}">
                        </div>
                      </div><br>
                      {!! Form::submit('SUBMIT',['class' => 'btn btn-primary pull-right']) !!}&nbsp
                  {!! Form::close() !!}
                </div>
              </div>
            </div>
          </div>
        </div>
    </div>
@endsection