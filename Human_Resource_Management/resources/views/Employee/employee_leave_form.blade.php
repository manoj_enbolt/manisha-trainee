@extends('head')
@section('content')
	<div class="content" style="padding-top: 50px; padding-left: 50px;">
      @if(Session::has('success'))
        <div class="alert alert-success alert-dismissible" role="alert">
            {!! Session::get('success') !!}
        </div>
      @endif
        <div class="container-fluid">
          <div class="row">
            <div class="col-md-8">
              <div class="card">
                <div class="card-header card-header-primary">
                  <h4 class="card-title">Apply For Leave</h4>
                  <p class="card-category">Your Leave Details down below...</p><hr>
                </div>
                <div class="card-body">
                  {!! Form::open(['url' => '/employee/leave/store','method' => 'post']) !!}
                    {{ csrf_field() }}
                    <h4>Details For Leave</h4><hr>
                    <div class="row" id="dateshow">
                      <div class="col-md-5">
                        <div class="form-group" {{ $errors->has('start_leave') ? ' is-invalid' : '' }}">
                          <label class="bmd-label-floating">Leave Start</label>
                          {!! Form::date('start_leave',NULL, ['class' => 'form-control','placeholder' => 'Leave Start']) !!}
                          {!! $errors->first('start_leave','<span class="help-block" style="color:red;">:message</span>') !!}
                        </div>
                      </div>
                      <div class="col-md-5">
                        <div class="form-group" {{ $errors->has('end_leave') ? ' is-invalid' : '' }}">
                          <label class="bmd-label-floating">Leave End</label>
                          {!! Form::date('end_leave',NULL, ['class' => 'form-control','placeholder' => 'Leave End']) !!}
                          {!! $errors->first('end_leave','<span class="help-block" style="color:red;">:message</span>') !!}
                        </div>
                      </div>
                    </div>
                      <div class="row" style="padding-left: 10px;" id="halfday-checkbox">
                      <div class="col-md-5">
                        <div class="form-group" {{ $errors->has('halfday') ? ' is-invalid' : '' }}">
                          {!! Form::checkbox('halfday','Halfday Leave') !!}&nbsp&nbsp&nbspHalfday Leave
                          {!! $errors->first('halfday','<span class="help-block" style="color:red;">:message</span>') !!}
                        </div>
                      </div>
                    </div>
                        <div class="row" id="leave-time" style="display: none">
                          <div class="col-md-5">
                            <div class="form-group" {{ $errors->has('absence_date') ? ' is-invalid' : '' }}">
                              <label class="bmd-label-floating"> Absence Date </label>
                              {!! Form::date('absence_date',NULL, ['class' => 'form-control','placeholder' => 'Absence Date']) !!}
                              {!! $errors->first('absence_date','<span class="help-block" style="color:red;">:message</span>') !!}
                            </div>
                          </div>
                          <div class="col-md-5">
                            <div class="form-group" {{ $errors->has('leave_time') ? ' is-invalid' : '' }}">
                              <label class="bmd-label-floating">Leave Time</label>
                              {!! Form::select('leave_time',['' => '--- Leave Time---', 'Morning' => 'Morning', 'Evening' => 'Evening'],null,['class'=>'form-control']) !!}
                              {!! $errors->first('leave_time','<span class="help-block" style="color:red;">:message</span>') !!}
                            </div>
                          </div>
                        </div>
                 
                    <div class="row">
                      <div class="col-md-12">
                        <div class="form-group"{{ $errors->has('reason') ? ' is-invalid' : '' }}">
                          <label class="bmd-label-floating">For Leave Reason</label>
                          {!! Form::textarea('reason',NULL, ['rows' => 2, 'cols' => 54,'id' => 'reason','class' => 'form-control','placeholder' => 'For Leave Reason']) !!}
                          {!! $errors->first('reason','<span class="help-block" style="color:red;">:message</span>') !!}
                        </div>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-12">
                        <div class="form-group"{{ $errors->has('address') ? ' is-invalid' : '' }}">
                          <label class="bmd-label-floating">Address</label>
                          {!! Form::textarea('address',NULL, ['rows' => 2, 'cols' => 54,'id' => 'address','class' => 'form-control','placeholder' => 'Address']) !!}
                          {!! $errors->first('address','<span class="help-block" style="color:red;">:message</span>') !!}
                        </div>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-6">
                        <div class="form-group" {{ $errors->has('phone_number') ? ' is-invalid' : '' }}">
                          <label class="bmd-label-floating">Emergency Phone Number</label>
                          {!! Form::text('phone_number',NULL, ['class' => 'form-control','placeholder' => 'Emergency Phone Number']) !!}
                          {!! $errors->first('phone_number','<span class="help-block" style="color:red;">:message</span>') !!}
                        </div>
                      </div>
                       <div class="col-md-6">
                        <div class="form-group" {{ $errors->has('assign_teamleader') ? ' is-invalid' : '' }}">
                          <label class="bmd-label-floating">Select Your TeamLeader</label>
                          {!! Form::select('assign_teamleader', ['' => '--- Select TeamLeader ---']+$assignable_users,null, array('class' => 'form-control')) !!}
                          {!! $errors->first('assign_teamleader','<span class="help-block" style="color:red;">:message</span>') !!}
                        </div>
                      </div>
                    </div>
                    {!! Form::submit('SUBMIT',['class' => 'btn btn-primary pull-right']) !!}&nbsp
                  {!! Form::close() !!}
                </div>
              </div>
            </div>
          </div>
        </div>
    </div>
@endsection