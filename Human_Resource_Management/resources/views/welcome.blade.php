<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="keywords" content="">
    <link rel="icon" type="image/png" href="{{ asset('theme-assets/images/logo/em-hrm icon.png') }}">
    <title>Human Resource Management</title>
    <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/style.css') }}" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
    <link href="{{ asset('css/owl.carousel.css') }}" rel="stylesheet">
    <link href="{{ asset('css/owl.theme.default.css') }}" rel="stylesheet">
    <link href="{{ asset('css/fontello.css') }}" rel="stylesheet">
    <link href="{{ asset('css/font-awesome.min.css') }}" rel="stylesheet">
</head>
<body>

    <div class="header-wrapper">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-md-8 col-sm-10 col-xs-12">
                    <div class="logo">
                        <a href="#">
                           <img src="https://easetemplate.com/free-website-templates/hrms/images/logo.png" alt="">&nbsp&nbsp&nbsp&nbsp Human Resources Managment
                       </a>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                    <div class="navigation">
                        <div id="navigation">
                            <ul>
                                <li>
                                    <a href="{{ route('login') }}">Login</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="slider">
        <div class="owl-carousel owl-one owl-theme">
            <div class="item">
                <div class="slider-img">
                    <img src="https://easetemplate.com/free-website-templates/hrms/images/slider_1.jpg" alt="">
                </div>
            </div>
            <div class="item">
                <div class="slider-img">
                    <img src="https://easetemplate.com/free-website-templates/hrms/images/slider_2.jpg" alt="">
                </div>
            </div>
            <div class="item">
                <div class="slider-img">
                    <img src="https://easetemplate.com/free-website-templates/hrms/images/slider_3.jpg" alt="">
                </div>
            </div>
        </div>
    </div>

    <div class="space-medium bg-light">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="section-title">
                        <h2>HR Consultation Services</h2>
                        <a href="#" class="btn-link">View Services</a>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                    <div class="service-block">
                        <div class="service-img">
                            <a href="#" class="imghover">
                                <img src="https://easetemplate.com/free-website-templates/hrms/images/service_img_1.jpg" alt="" class="img-responsive">
                            </a>
                        </div>
                        <div class="service-content">
                            <h3 class="service-title">
                                <a href="#" class="title">Employee & TeamLeader Managment</a>
                            </h3>
                            <p>Cras vittepus dolor uspedisse spien sapie facilisi.</p>
                            <a href="#" class="btn-link">More Details</a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                    <div class="service-block">
                        <div class="service-img">
                            <a href="#" class="imghover">
                                <img src="https://easetemplate.com/free-website-templates/hrms/images/service_img_2.jpg" alt=""  class="img-responsive">
                            </a>
                        </div>
                        <div class="service-content">
                            <h3 class="service-title"><a href="#" class="title">Show Leave Details</a></h3>
                            <p>Donec ac malesuada liberos phasellus euismod odio aser odio pellentesque quis. </p>
                            <a href="#" class="btn-link">More Details</a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                    <div class="service-block">
                        <div class="service-img">
                            <a href="#" class="imghover">
                                <img src="https://easetemplate.com/free-website-templates/hrms/images/service_img_3.jpg" alt=""  class="img-responsive">
                            </a>
                        </div>
                        <div class="service-content">
                            <h3 class="service-title">
                                <a href="#" class="title">Employment DashBoard</a>
                            </h3>
                            <p>Nullam turpis magna ierdum egetf ringillaeget hendrerits eget auguullam interdu.</p>
                            <a href="#" class="btn-link">More Details</a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                    <div class="service-block">
                        <div class="service-img">
                            <a href="#" class="imghover">
                                <img src="https://easetemplate.com/free-website-templates/hrms/images/service_img_4.jpg" alt="" class="img-responsive">
                            </a>
                        </div>
                        <div class="service-content">
                            <h3 class="service-title">
                                <a href="#" class="title">Employee Leave</a>
                            </h3>
                            <p>Morbi tempus tristiquenisl at elementum nisl cimentum ins fusce et consequat purus. </p>
                            <a href="#" class="btn-link">More Details</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div style="height: 50px; line-height: 50px; padding-top: 20px;">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <p>Copyright © All Rights Reserved 2020 Template Design by
                        <a href="https://easetemplate.com/" target="_blank" class="copyrightlink">EaseTemplate
                        </a>
                    </p>
                </div>
            </div>
        </div>
    </div>

    <script type="text/javascript" src="{{ asset('js/jquery.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/bootstrap.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/menumaker.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/jquery.sticky.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/sticky-header.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/owl.carousel.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/multiple-carousel.js') }}"></script>
    <script type="text/javascript">
    $("#dots").click(function() {
        $(".top-header").toggle("slow", function() {
            // Animation complete.
        });
    });
    </script>
    
</body>
</html>