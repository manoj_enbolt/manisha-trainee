<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>
<body>
	<h3>Leave Apply Request Employee</h3><hr><br>

	<p>Leave Start Date: {{ $start_leave }}</p>
	<p>Leave End Date: {{ $end_leave }}</p>
	<p>Half day: {{ $halfday }}</p>
	<p>Absence Date: {{ $absence_date }}</p>
	<p>Leave Time: {{ $leave_time }}</p>
	<p>For Leave Reason : {{ $reason }}</p>
	<p> Leave Time Address: {{ $address }}</p>
	<p> Emrgency Phone Number : {{ $phone_number }}</p>
	<p> Send By : {{ Auth::user()->email}}</p>

	<a href="{{ url('/hr/leave/response', $id) }}" style="font-size: 20px;">Give Leave Response</a>

</body>
</html>
