@extends('head')
@section('content')
	<div class="app-content content" style="padding-top: 70px; padding-left: 40px;">
	    <div class="content-wrapper">
			<div class="content-body">
				<div class="col-lg-6 grid-margin stretch-card">
					<div class="card">
						<div class="card-body">
							<div class="clearfix">
								<div class="float-left" style="padding-left: 40px; padding-top: 30px;">
									<h4 class="card-title"> Subject : {{ $leave->reason }}</h4>
								</div>
							</div><hr><br>
							<div class="row" style="padding-left: 20px;">
                                <div class="col-xs-12 col-sm-10">
			                        <div class="fluid-container">
			                          	<p class="font-weight-medium  mb-0">
			                          		Start Leave Date: {{ $leave->start_leave }} 
			                          	</p><br>
			                          	<p class="font-weight-medium  mb-0">
			                          		End Leave Date: {{ $leave->end_leave }}
			                          	</p><br>
		                          		<p class="font-weight-medium  mb-0">
			                          		HalfDay: {{ $leave->halfday }} 
			                          	</p><br>			                          	
			                          	<p class="font-weight-medium  mb-0">
			                          		Absence Date: {{ $leave->absence_date }} 
			                          	</p><br>
			                          	<p class="font-weight-medium  mb-0">
			                          		Leave Time: {{ $leave->leave_time }} 
			                          	</p><br>
			                          	<p class="font-weight-medium  mb-0">
			                          		Reason: {{ $leave->reason }} 
			                          	</p><br>
			                          	<p class="font-weight-medium  mb-0">
			                          		Address: {{ $leave->address }} 
			                          	</p><br>
			                          	<p class="font-weight-medium  mb-0">
			                          		Emergency Phone Number: {{ $leave->phone_number }} 
			                          	</p><br>
			                        </div>
                                </div>	
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection