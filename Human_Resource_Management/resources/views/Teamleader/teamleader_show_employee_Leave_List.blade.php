@extends('head')
@section('content')
	<div class="app-content content" style="padding-top: 70px; padding-left: 40px;">
	    <div class="content-wrapper">
			<div class="content-body">
				<div class="col-lg-12 grid-margin stretch-card">
	              <div class="card">
	                <div class="card-body">
	                	<div class="clearfix">
	                      <div class="float-left">
	                        <h4 class="card-title"> Employee Leave Request</h4>
	                      </div>
	                      <div class="float-right">
	                      </div>
	                    </div><br>
	                  	<div class="table-responsive">
		                    <table class="table table-striped">
		                     	<thead>
			                        <tr>
			                        	<th>#</th>
			                          	<th>Name</th>
			                          	<th>Email</th>
			                          	<th>View</th>
			                        </tr>
		                      	</thead>
		                      	<tbody>
		                      	 	<?php $no=1; ?>
		                      	 	@foreach ($my_employee as $user)
		                        		<tr>
											<td>{{ $no++ }}</td>
											<td>{{ $user->first_name }}</td>
											<td>{{ $user->email}}</td>
											<td><a href=" {{-- {{ url('/teamleader/leave/response', $leave->id) }} --}}">Show Leave Details</a></td>
			                          	</tr>
			                        @endforeach
			                    </tbody>
		                    </table>
	                  	</div>
	                </div>
	              </div>
	            </div>
	        </div>
	   	</div>
	</div>
@endsection