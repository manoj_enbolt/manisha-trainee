@extends('head')
@section('content')
  <div class="app-content content" style="padding-top: 70px; padding-left: 40px;">
      <div class="content-wrapper">
        <div class="content-body"><!-- ../../../theme-assets/images/carousel/22.jpg -->
          <section id="header-footer"><!-- Header footer section start -->
            <div class="row">
              <div class="col-xl-3 col-lg-3 col-md-3 col-sm-6 grid-margin stretch-card">
                <div class="card card-statistics">
                  <div class="card-body">
                    <div class="clearfix">
                      <div class="float-left">
                        <h1 style="color: red;"><i class="ft-eye" ></i></h1>
                      </div>
                      <div class="float-right">
                        <p class="mb-0 text-right">My Employee</p>
                        <div class="fluid-container">
                          <p class="font-weight-medium text-right mb-0"></p>
                        </div>
                      </div>
                    </div>
                    <p class="text-muted mt-3 mb-0">
                      <a href="{{ url('/teamleader/show/employeelist')  }}">
                        <i class="mdi mdi-alert-octagon mr-1" aria-hidden="true"></i> Show My Employee List
                      </a>
                    </p>
                  </div>
                </div>
              </div>
             {{--  <div class="col-xl-3 col-lg-3 col-md-3 col-sm-6 grid-margin stretch-card">
                <div class="card card-statistics">
                  <div class="card-body">
                    <div class="clearfix">
                      <div class="float-left">
                        <h1 style="color: red;"><i class="ft-edit" ></i></h1>
                      </div>
                      <div class="float-right">
                        <p class="mb-0 text-right">Leave Managment</p>
                        <div class="fluid-container">
                          <p class="font-weight-medium text-right mb-0"> </p>
                        </div>
                      </div>
                    </div>
                    <p class="text-muted mt-3 mb-0">
                      <a href="{{ url('/teamleader/show/employee/leave/list')  }}">
                        <i class="mdi mdi-alert-octagon mr-1" aria-hidden="true"></i>Employee Leave Managment
                      </a>
                    </p>
                  </div>
                </div>
              </div> --}}
              {{-- <div class="col-xl-3 col-lg-3 col-md-3 col-sm-6 grid-margin stretch-card">
                <div class="card card-statistics">
                  <div class="card-body">
                    <div class="clearfix">
                      <div class="float-left">
                        <h1 style="color: green;"><i class="ft-eye" ></i></h1>
                      </div>
                      <div class="float-right">
                        <p class="mb-0 text-right"> Employee </p>
                        <div class="fluid-container">
                          <p class="font-weight-medium text-right mb-0">View Employee Profile</p>
                        </div>
                      </div>
                    </div>
                    <p class="text-muted mt-3 mb-0">
                      <a href="{{ url('/admin/view/all/employee/profile')  }}">
                        <i class="mdi mdi-alert-octagon mr-1" aria-hidden="true"></i> View Profile
                      </a>
                    </p>
                  </div>
                </div>
              </div> --}}
            </div>
          </section>
        </div>
      </div>
    </div>
@endsection