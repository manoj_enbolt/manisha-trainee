@extends('head')
@section('content')
	<div class="app-content content" style="padding-top: 70px; padding-left: 40px;">
	    <div class="content-wrapper">
			<div class="content-body">
				<div class="col-lg-12 grid-margin stretch-card">
	              <div class="card">
	                <div class="card-body">
	                	<div class="clearfix">
	                      <div class="float-left">
	                        <h4 class="card-title">My Employee</h4>
	                      </div>
	                      <div class="float-right">
	                      </div>
	                    </div><br>
	                  	<div class="table-responsive">
		                    <table class="table table-striped">
		                     	<thead>
			                        <tr>
			                        	<th>#</th>
			                        	<th>User</th>
			                          	<th>Name</th>
			                          	<th>Email</th>
			                        </tr>
		                      	</thead>
		                      	<tbody>
		                      	 	<?php $no=1; ?>
		                      	 	@foreach ($users as $user)
		                        		<tr>
											<td>{{ $no++ }}</td>
											<td class="py-1">
												<img style="width: 30px; height: 30px; border-radius: 50%" alt="{{ $user->profile_image }}" src="{{ asset('/storage/upload').'/'.$user->profile_image }}">
											</td>
											<td>{{ $user->first_name }}</td>
											<td>{{ $user->email}}</td>
			                          	</tr>
			                        @endforeach
			                    </tbody>
		                    </table>
	                  	</div>
	                </div>
	              </div>
	            </div>
	        </div>
	   	</div>
	</div>
@endsection