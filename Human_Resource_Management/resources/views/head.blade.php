<!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta name="description" content="Chameleon Admin is a modern Bootstrap 4 webapp &amp; admin dashboard html template with a large number of components, elegant design, clean and organized code.">
    <meta name="keywords" content="admin template, Chameleon admin template, dashboard template, gradient admin template, responsive admin template, webapp, eCommerce dashboard, analytic dashboard">
    <meta name="author" content="ThemeSelect">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <link rel="icon" type="image/png" href="{{ asset('theme-assets/images/logo/em-hrm icon.png') }}">

    <title>Human Resource Management</title>

    <link href="https://fonts.googleapis.com/css?family=Muli:300,300i,400,400i,600,600i,700,700i%7CComfortaa:300,400,700" rel="stylesheet">
    <link href="https://maxcdn.icons8.com/fonts/line-awesome/1.1/css/line-awesome.min.css" rel="stylesheet">

    <link rel="stylesheet" href="{{ asset('theme-assets/css/vendors.css') }}">

    <link rel="stylesheet" href="{{ asset('theme-assets/vendors/css/charts/chartist.css') }}">

    <link rel="stylesheet" href="{{ asset('theme-assets/css/app-lite.css') }}">

    <link rel="stylesheet" href="{{ asset('theme-assets/css/core/menu/menu-types/vertical-menu.css') }}">

    <link rel="stylesheet" href="{{ asset('theme-assets/css/core/colors/palette-gradient.css') }}">

    <link rel="stylesheet" href="{{ asset('theme-assets/css/pages/dashboard-ecommerce.css') }}">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script type="text/javascript">
      $(document).ready(function() {
         $("#halfday-checkbox").click(function() {
          $("#leave-time").toggle();
           $("#dateshow").toggle();
         });
      });
    </script>
  </head>

  <body class="vertical-layout vertical-menu 4-columns   menu-expanded fixed-navbar" data-open="click" data-menu="vertical-menu" data-color="bg-chartbg" data-col="2-columns">
    <!-- Top Header Menu start-->
    <nav class="header-navbar navbar-expand-md navbar navbar-with-menu navbar-without-dd-arrow fixed-top navbar-semi-light">
      <div class="navbar-wrapper">
        <div class="navbar-container content">
          <div class="collapse navbar-collapse show" id="navbar-mobile">
            <ul class="nav navbar-nav mr-auto float-left">
              <li class="nav-item d-block d-md-none">
                <a class="nav-link nav-menu-main menu-toggle hidden-xs" href="#">
                  <i class="ft-menu"></i>
                </a>
              </li>
              <li class="nav-item d-none d-md-block">
                <a class="nav-link nav-link-expand" href="#">
                  <i class="ficon ft-maximize"></i>
                </a>
              </li>
              <li class="nav-item dropdown navbar-search">
                <a class="nav-link dropdown-toggle hide" data-toggle="dropdown" href="#">
                  <i class="ficon ft-search"></i>
                </a>
                <ul class="dropdown-menu">
                  <li class="arrow_box">
                    <form>
                      <div class="input-group search-box">
                        <div class="position-relative has-icon-right full-width">
                          <input class="form-control" id="search" type="text" placeholder="Search here...">
                          <div class="form-control-position navbar-search-close"><i class="ft-x"></i></div>
                        </div>
                      </div>
                    </form>
                  </li>
                </ul>
              </li>
            </ul>
            <ul class="nav navbar-nav float-right">
              <li class="dropdown dropdown-user nav-item">
                  <a class="dropdown-toggle nav-link dropdown-user-link" href="#" data-toggle="dropdown">
                    <span class="avatar avatar-online">
                      <img style="width: 50px; height: 50px; border-radius: 50%" alt="{{ Auth::user()->profile_image }}" src="{{ asset('/storage/upload').'/'.Auth::user()->profile_image }}">
                    </span>
                  </a>
                  <div class="dropdown-menu dropdown-menu-right">
                    <div class="arrow_box_right">
                      <a class="dropdown-item" href="{{ url('/view/profile') }}">
                        <span class="avatar avatar-online">
                          <span class="user-name text-bold-700 ml-1">
                            {{ Auth::user()->first_name }}</span>
                        </span>
                      </a>
                      <div class="dropdown-divider"></div>
                      <a class="dropdown-item" href="{{ url('/user/edit/id') }}">
                        <i class="ft-user"></i> Edit Profile
                      </a>
                      <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                        <i class="ft-power"></i> Logout
                      </a>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    @csrf</form>
                    <li class="devider"></li>
                    </div>
                </div>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </nav>
    <!-- Top Header Menu end-->

    <!--Side Menu Start-->
    <div class="main-menu menu-fixed menu-light menu-accordion menu-shadow " data-scroll-to-active="true" data-img="theme-assets/images/backgrounds/02.jpg">
      <div class="navbar-header">
        <ul class="nav navbar-nav flex-row">       
          <li class="nav-item mr-auto">
            <a class="navbar-brand" href="#">
              <img class="brand-logo" alt="Chameleon admin logo" src="{{ asset('theme-assets/images/logo/em-hrm icon.png') }}"/>
              <h3 class="brand-text">HRM System</h3>
            </a>
          </li>
          <li class="nav-item d-md-none">
            <a class="nav-link close-navbar">
              <i class="ft-x"></i>
            </a>
          </li>
        </ul>
      </div>
      <div class="main-menu-content">
        <div class="image">
          <a href="{{ URL::to( '/storage/upload/' . Auth::user()->profile_image)  }}" target="_blank">
            <img style="width: 70px; height: 70px; border-radius: 50%; margin-left: 90px;" alt="{{ Auth::user()->profile_image }}" src="{{ asset('/storage/upload').'/'.Auth::user()->profile_image }}">
          </a>
        </div>
        <ul class="navigation navigation-main" id="main-menu-navigation" data-menu="menu-navigation">
          <li class="nav-item">
            <a>
              <span class="menu-title" data-i18n="" style="margin-left: 50px;">
                Welcome {{ Auth::user()->first_name }}
              </span>
            </a>
          </li>
           {{-- <li class="nav-item">
            <a>
              <span class="menu-title" data-i18n="" style="margin-left: 50px;">You are : 
                @if(!empty($user->getRoleNames()))
                  @foreach($user->getRoleNames() as $role)
                    {{ $role }}
                  @endforeach
                @endif
              </span>
            </a>
          </li> --}}<hr>
          @role('Admin')
           {{--  <li class="nav-item">
              <a href="{{ url('/admin/dashboard') }}">
                <i class="ft-home"></i><span class="menu-title" data-i18n="">Dashboard</span>
              </a>
            </li> --}}
            <li class="nav-item">
              <a href="{{ url('/admin/manage/employee') }}">
                <i class="ft-users"></i><span class="menu-title" data-i18n="">Manage Employees</span>
              </a>
            </li>
            <li class="nav-item">
              <a href="{{ url('/admin/manage/holiday') }}">
                <i class="ft-calendar"></i><span class="menu-title" data-i18n=""> Manage Holiday</span>
              </a>
            </li>
          @endrole
          @role('HR')
            <li class="nav-item">
              <a href="{{ url('/hr/dashboard') }}">
                <i class="ft-home"></i><span class="menu-title" data-i18n="">Dashboard</span>
              </a>
            </li>
            <li class="nav-item">
              <a href="{{ url('all/employee/show/holiday/schedule') }}">
                <i class="ft-calendar"></i><span class="menu-title" data-i18n=""> Holiday Schedule</span>
              </a>
            </li>
            <li class="nav-item">
              <a href="{{ url('/hr/leave/managment') }}">
                <i class="ft-folder"></i><span class="menu-title" data-i18n=""> Leave Managment</span>
              </a>
            </li>
          @endrole
          @role('TeamLeader')
             <li class="nav-item">
              <a href="{{ url('/teamleader/dashboard') }}">
                <i class="ft-home"></i><span class="menu-title" data-i18n="">Dashboard</span>
              </a>
            </li>
            <li class="nav-item">
              <a href="{{ url('/teamleader/manage/employee') }}">
                <i class="ft-users"></i><span class="menu-title" data-i18n="">My Employees</span>
              </a>
            </li>
            <li class="nav-item">
              <a href="{{ url('/teamleader/apply/leave') }}">
                <i class="ft-plus"></i><span class="menu-title" data-i18n="">Apply Leave</span>
              </a>
            </li>
             <li class="nav-item">
              <a href="{{ url('all/employee/show/holiday/schedule') }}">
                <i class="ft-calendar"></i><span class="menu-title" data-i18n="">Holiday Schedule</span>
              </a>
            </li>
             <li class="nav-item">
              <a href="{{ url('/teamleader/leave/managment') }}">
                <i class="ft-folder"></i><span class="menu-title" data-i18n=""> Leave Managment</span>
              </a>
            </li>
          @endrole
          @role('Employee')
          <li class="nav-item">
            <a href="{{ url('/employee/dashboard') }}">
              <i class="ft-home"></i><span class="menu-title" data-i18n="">Dashboard</span>
            </a>
          </li>
            <li class="nav-item">
              <a href="{{ url('/employee/apply/leave') }}">
                <i class="ft-plus"></i><span class="menu-title" data-i18n="">Apply Leave</span>
              </a>
            </li>
             <li class="nav-item">
              <a href="{{ url('all/employee/show/holiday/schedule') }}">
                <i class="ft-calendar"></i><span class="menu-title" data-i18n="">Holiday Schedule</span>
              </a>
            </li>
            <li class="nav-item">
              <a href="{{ url('employee/show/my/document/menu') }}">
                <i class="ft-file-text"></i><span class="menu-title" data-i18n="">My Document</span>
              </a>
            </li>
          @endrole
        </ul>
      </div>
    </div>

    @yield('content');
    <footer class="footer footer-static footer-light navbar-border navbar-shadow">
      <div class="clearfix blue-grey lighten-2 text-sm-center mb-0 px-2">
        <span class="float-md-left d-block d-md-inline-block">2018  &copy; Copyright 
          <a class="text-bold-800 grey darken-2" href="https://themeselection.com" target="_blank">ThemeSelection</a>
        </span>
      </div>
    </footer>

    <script src="{{asset('theme-assets/vendors/js/vendors.min.js') }}" type="text/javascript"></script>

    <script src="{{asset('theme-assets/vendors/js/charts/chartist.min.js') }}" type="text/javascript"></script>
    
    <script src="{{asset('theme-assets/js/core/app-menu-lite.js') }}" type="text/javascript"></script>

    <script src="{{asset('theme-assets/js/core/app-lite.js') }}" type="text/javascript"></script>
    
    <script src="{{asset('theme-assets/js/scripts/pages/dashboard-lite.js') }}" type="text/javascript"></script>
  </body>
</html>