@extends('head')
@section('content')
    <div class="content" style="padding-top: 80px; padding-left: 50px;">
      @if(Session::has('success'))
        <div class="alert alert-success alert-dismissible" role="alert">
            {!! Session::get('success') !!}
        </div>
      @endif
        <div class="container-fluid">
          <div class="row">
            <div class="col-md-8">
              <div class="card">
                <div class="card-header card-header-primary">
                  <h4 class="card-title">Add User</h4>
                  <p class="card-category">Enter Details below..</p><hr>
                </div>
                <div class="card-body">
                  {!! Form::open(['url' => '/admin/user/store','method' => 'post','files' => true]) !!}
                    <div class="row">
                      <div class="col-md-4">
                        <div class="form-group" {{ $errors->has('first_name') ? ' is-invalid' : '' }}">
                          <label class="bmd-label-floating">First Name</label>
                          {!! Form::text('first_name',NULL, ['class' => 'form-control','placeholder' => 'First Name']) !!}
                          {!! $errors->first('first_name','<span class="help-block" style="color:red;">:message</span>') !!}
                        </div>
                      </div>
                      <div class="col-md-4">
                        <div class="form-group" {{ $errors->has('last_name') ? ' is-invalid' : '' }}">
                          <label class="bmd-label-floating">Last Name</label>
                          {!! Form::text('last_name',NULL, ['class' => 'form-control','placeholder' => 'Last Name']) !!}
                          {!! $errors->first('last_name','<span class="help-block" style="color:red;">:message</span>') !!}
                        </div>
                      </div>
                       <div class="col-md-4">
                        <div class="form-group" {{ $errors->has('date_of_birth') ? ' is-invalid' : '' }}">
                          <label class="bmd-label-floating">Date Of Birth</label>
                          {!! Form::date('date_of_birth',NULL, ['class' => 'form-control','placeholder' => 'Date Of Birth']) !!}
                          {!! $errors->first('date_of_birth','<span class="help-block" style="color:red;">:message</span>') !!}
                        </div>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-6">
                        <div class="form-group" {{ $errors->has('email') ? ' is-invalid' : '' }}">
                          <label class="bmd-label-floating">Username</label>
                          {!! Form::text('email','', ['class' => 'form-control','placeholder' => 'abc@xyz.com']) !!}
                          {!! $errors->first('email','<span class="help-block" style="color:red;">:message</span>') !!}
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="form-group" {{ $errors->has('password') ? ' is-invalid' : '' }}">
                          <label class="bmd-label-floating">Password</label>
                          {!! Form::password('password', ['class' => 'form-control','placeholder' => 'Password'])!!}
                          {!! $errors->first('password','<span class="help-block" style="color:red;">:message</span>') !!}
                        </div>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-12">
                        <div class="form-group"{{ $errors->has('address') ? ' is-invalid' : '' }}">
                          <label class="bmd-label-floating">Adress</label>
                          {!! Form::textarea('address',NULL, ['rows' => 2, 'cols' => 54,'id' => 'address','class' => 'form-control','placeholder' => 'Address']) !!}
                          {!! $errors->first('address','<span class="help-block" style="color:red;">:message</span>') !!}
                        </div>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-4">
                        <div class="form-group" {{ $errors->has('profile_image') ? ' is-invalid' : '' }}">
                          <label class="bmd-label-floating">Profile Image</label>
                          {!! Form::file('profile_image',['class'=>'form-control', 'placeholder'=>'Profile Image']) !!}
                          {!! $errors->first('profile_image','<span class="help-block" style="color:red;">:message</span>') !!}
                        </div>
                      </div>
                      <div class="col-md-4">
                        <div class="form-group" {{ $errors->has('gender') ? ' is-invalid' : '' }}">
                          <label class="bmd-label-floating">Gender</label>
                         <div class="radio-list">
                            <div class="radio-inline" style="padding-top: 10px;">
                          {!! Form::radio('gender','male') !!} Male &nbsp&nbsp&nbsp
                          {!! Form::radio('gender','female') !!} Female<br>
                          {!! $errors->first('gender','<span class="help-block" style="color:red;">:message</span>') !!}
                        </div>
                      </div>
                        </div>
                      </div>
                      <div class="col-md-4">
                        <div class="form-group" {{ $errors->has('role') ? ' is-invalid' : '' }}">
                          <label class="bmd-label-floating">Select Role</label>
                          {!! Form::select('roles[]', ['' => '--- Select Role ---']+$roles,[], array('class' => 'form-control')) !!}
                          {!! $errors->first('role','<span class="help-block" style="color:red;">:message</span>') !!}
                        </div>
                      </div>
                    </div>
                      {!! Form::submit('SUBMIT',['class' => 'btn btn-primary pull-right']) !!}&nbsp
                  {!! Form::close() !!}
                </div>
              </div>
            </div>
          </div>
        </div>
    </div>
@endsection