@extends('head')
@section('content')
	<div class="app-content content" style="padding-top: 70px; padding-left: 40px;">
		@if(Session::has('success'))
        <div class="alert alert-success alert-dismissible" role="alert">
            {!! Session::get('success') !!}
        </div>
      @endif
	    <div class="content-wrapper">
			<div class="content-body">
				<div class="col-lg-8 grid-margin stretch-card">
					<div class="card">
						<div class="card-body">
							<div class="clearfix">
								<div class="float-left">
									<h4 class="card-title">Assign TeamLeader</h4>
								</div>
							</div><hr><br>
							<div class="row">
                                <div class="col-xs-4 col-sm-4">
                                	<img style="width: 130px; height: 130px; border-radius: 50%; margin-left: 50px; margin-top: 20px;" alt="{{ $user->profile_image }}" src="{{ asset('/storage/upload').'/'.$user->profile_image }}">
                                </div>
                                <div class="col-xs-12 col-sm-8">
			                        <div class="fluid-container">
			                        	<p class="font-weight-medium  mb-0">
			                          		Name : {{ $user->first_name }} 
			                          	</p><br>
			                          	<p class="font-weight-medium  mb-0">
			                          		UserName : {{ $user->email }} 
			                          	</p><br>
			                          	<p class="font-weight-medium  mb-0">
			                        		Role : 
				                          	@if(!empty($user->getRoleNames()))
										        @foreach($user->getRoleNames() as $role)
										        	{{ $role }}
										        @endforeach
		      								@endif
			                          	</p><br>
			                          	
			                          	@if($user->handle_user_id == NULL)
							                <div class="form-group" {{ $errors->has('assign_teamleader') ? ' is-invalid' : '' }}">
							                	{!! Form::open(['url' => '/admin/assign/teamleader','id' => 'support-form-comment','method' => 'post']) !!}
	                                            	{{method_field('PATCH')}}
	                                            	{{ Form::hidden('user_id', $user->id ) }}
							                      	<p class="font-weight-medium  mb-0">Assign TeamLeader : </p><br>
							                        {!! Form::select('assign_teamleader', ['' => '--- Select TeamLeader ---']+$assignable_users,null, array('class' => 'form-control')) !!}
							                        {!! $errors->first('assign_teamleader','<span class="help-block" style="color:red;">:message</span>') !!}<br><br>
							                        {!! Form::submit('SUBMIT',['class' => 'btn btn-primary']) !!}
	                                           {!! Form::close() !!}
						                	</div>
						                	@else
						                		<p class="font-weight-medium  mb-0">
			                          				Assign TeamLeader  : {{ $handle_by->first_name }} 
			                          			</p><br>
			                          	@endif
			                        </div>
                                </div>	
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection