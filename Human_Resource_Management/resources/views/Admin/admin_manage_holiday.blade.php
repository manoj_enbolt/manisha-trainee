@extends('head')
@section('content')
	<div class="app-content content" style="padding-top: 70px; padding-left: 40px;">
    	<div class="content-wrapper">
    		<div class="content-body">
        		<section id="header-footer"><!-- Header footer section start -->
        			<div class="row">
        				<div class="col-xl-3 col-lg-3 col-md-3 col-sm-6 grid-margin stretch-card">
            				<div class="card card-statistics">
                  			<div class="card-body">
                    		  <div class="clearfix">
                      			<div class="float-left">
                        		  <h1 style="color: red;"><i class="ft-plus" ></i></h1>
                      			</div>
                      			<div class="float-right">
                        			<p class="mb-0 text-right">Create New Holiday</p>
                        			<div class="fluid-container">
                          			<p class="font-weight-medium text-right mb-0"></p>
                        			</div>
                      			</div>
                    			</div>
                    			<p class="text-muted mt-3 mb-0">
                      			<a href="{{ url('/admin/create/holiady')  }}">
                        			<i class="mdi mdi-alert-octagon mr-1" aria-hidden="true"></i>Create Holiday
                      			</a>
                    			</p>
                  			</div>
                		</div>
              		</div>
              		<div class="col-xl-3 col-lg-3 col-md-3 col-sm-6 grid-margin stretch-card">
                			<div class="card card-statistics">
                  				<div class="card-body">
                    				<div class="clearfix">
                      					<div class="float-left">
                        					<h1 style="color: green;"><i class="ft-eye" ></i></h1>
                      					</div>
	                      				<div class="float-right">
	                        				<p class="mb-0 text-right">Show Holiday Schedule</p>
	                        				<div class="fluid-container">
	                          					<p class="font-weight-medium text-right mb-0"> </p>
	                        				</div>
	                      				</div>
                    				</div>
                    				<p class="text-muted mt-3 mb-0">
                      					<a href="{{ url('/admin/show/holiday/schedule')  }}">
                        					<i class="mdi mdi-alert-octagon mr-1" aria-hidden="true"></i>Show Holiday Schedule
                      					</a>
                    				</p>
                  				</div>
                			</div>
              		</div>
                  {{-- <div class="col-xl-3 col-lg-3 col-md-3 col-sm-6 grid-margin stretch-card">
                    <div class="card card-statistics">
                        <div class="card-body">
                          <div class="clearfix">
                            <div class="float-left">
                              <h1 style="color: blue;"><i class="ft-file" ></i></h1>
                            </div>
                            <div class="float-right">
                              <p class="mb-0 text-right">Manage Total Leave</p>
                              <div class="fluid-container">
                                <p class="font-weight-medium text-right mb-0"></p>
                              </div>
                            </div>
                          </div>
                          <p class="text-muted mt-3 mb-0">
                            <a href="{{ url('/admin/show/reamilng/total/leave')  }}">
                              <i class="mdi mdi-alert-octagon mr-1" aria-hidden="true"></i>Show Total Leave 
                            </a>
                          </p>
                        </div>
                    </div>
                  </div> --}}
            		</div>
          		</section>
        	</div>
      	</div>
    </div>
@endsection