@extends('head')
@section('content')
	<div class="app-content content" style="padding-top: 70px; padding-left: 40px;">
	    <div class="content-wrapper">
			<div class="content-body">
				<div class="col-lg-12 grid-margin stretch-card">
	              <div class="card">
	                <div class="card-body">
	                	<div class="clearfix">
	                      <div class="float-left">
	                        <h4 class="card-title">Assign Employees To TeamLeader</h4>
	                      </div>
	                      <div class="float-right">

	                      </div>
	                    </div><br>
	                  	<div class="table-responsive">
		                    <table class="table table-striped">
		                     	<thead>
			                        <tr>
			                        	<th>#</th>
			                        	<th>User</th>
			                          	<th>Name</th>
			                          	<th>Email</th>
			                          	<th>Role</th>
			                          	<th>Handle By TeamLeader</th>
			                        </tr>
		                      	</thead>
		                      	<tbody>
		                      	 	<?php $no=1; ?>
		                      	 	@foreach ($users as $user)
		                        		<tr>
											<td>{{ $no++ }}</td>
											<td class="py-1">
												<img style="width: 30px; height: 30px; border-radius: 50%" alt="{{ $user->profile_image }}" src="{{ asset('/storage/upload').'/'.$user->profile_image }}">
											</td>
											<td>{{ $user->first_name }}</td>
											<td>{{ $user->email}}</td>
											<td>
												@if(!empty($user->getRoleNames()))
										        @foreach($user->getRoleNames() as $role)
										        	{{ $role }}
										        @endforeach
		      								@endif
											</td>
											@if($user->handle_user_id == NULL)
											{{ Form::hidden('handle_user_id', $user->handle_user_id ) }}
												<td>
													<a href="{{ url('/admin/assign/teamleader/employee', $user->id) }}">Assign TeamLeader</a>
												</td>
												@else
													<td>Assign</td>
													{{-- <td>{{ $user->handle_by}}</td> --}}
											@endif
			                          	</tr>
			                        @endforeach
			                    </tbody>
		                    </table>
	                  	</div>
	                </div>
	              </div>
	            </div>
	        </div>
	    </div>
	</div>
@endsection