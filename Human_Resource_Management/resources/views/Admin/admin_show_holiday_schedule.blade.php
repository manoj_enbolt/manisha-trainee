@extends('head')
@section('content')
	<div class="app-content content" style="padding-top: 70px; padding-left: 40px;">
		@if(Session::has('success'))
          <div class="alert alert-success alert-dismissible" role="alert">
              {!! Session::get('success') !!}
          </div>
        @endif
	    <div class="content-wrapper">
			<div class="content-body">
				<div class="col-lg-10 grid-margin stretch-card">
	            	<div class="card">
	                	<div class="card-body">
	                		<div class="clearfix">
	                      		<div class="float-left">
	                        		<h4 class="card-title">Show Holiday Schedule List</h4>
	                      		</div>
	                    	</div>
	                  		<div class="table-responsive">
		                    	<table class="table table-striped">
		                     		<thead>
				                        <tr>
				                        	<th>No</th>
				                        	<th>Holiday Date</th>
				                          	<th>Holiday</th>
				                          	<th>Action</th>
				                        </tr>
		                      		</thead>
		                      		<tbody>
		                      	 		<?php $no=1; ?>
		                      	 		@foreach ($holidays as $holiday)
			                        		<tr>
												<td>{{ $no++ }}</td>
												<td>{{ $holiday->holiday_date->format('d-m-Y') }}</td>
												<td>{{ $holiday->holiday_description}}</td>
												<td>
													<a href="{{ url('/admin/edit/holiday', $holiday->id) }}">
														Edit
														{{-- <h5><i class="ft-edit"></i></h5> --}}
													</a>
													{{-- {!! Form::open(['method' => 'DELETE','url' => ['/admin/holiday/delete',$holiday->id]]) !!}
													<button type="submit" class="btn">
												<h5><i class="ft-trash" ></i></h5>
											</button>
														
													{!! Form::close() !!} --}}
												</td>
				                          	</tr>
			                        	@endforeach
			                    	</tbody>
		                    	</table>
	                  		</div>
	                	</div>
	              	</div>
	            </div>
	        </div>
	    </div>
   </div>
@endsection