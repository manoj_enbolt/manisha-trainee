@extends('head')
@section('content')
	<div class="app-content content" style="padding-top: 70px; padding-left: 40px;">
	    <div class="content-wrapper">
			<div class="content-body">
				<div class="col-lg-6 grid-margin stretch-card">
					<div class="card">
						<div class="card-body">
							<div class="clearfix">
								<div class="float-left">
									<h4 class="card-title">My Profile</h4>
								</div>
							</div><hr><br>
							<div class="row">
                                <div class="col-xs-4 col-sm-4">
                                	<a href="{{ URL::to( '/storage/upload/' . $user->profile_image)  }}" target="_blank">
                                		<img style="width: 100px; height: 100px; border-radius: 50%; margin-left: 30px;" alt="{{ $user->profile_image }}" src="{{ asset('/storage/upload').'/'.$user->profile_image }}">
                                	</a>
                                </div>
                                <div class="col-xs-12 col-sm-8">
			                        <div class="fluid-container">
			                        	<p class="font-weight-medium  mb-0">
			                          		Name : {{ $user->first_name }} 
			                          	</p><br>
			                          	<p class="font-weight-medium  mb-0">
			                          		UserName : {{ $user->email }} 
			                          	</p><br>
			                          	<p class="font-weight-medium  mb-0">
			                        		Role : 
				                          	@if(!empty($user->getRoleNames()))
										        @foreach($user->getRoleNames() as $role)
										        	{{ $role }}
										        @endforeach
		      								@endif
			                          	</p><br>
			                          	@if($user->handle_user_id == NULL)
			                          		@else

			                          	<p class="font-weight-medium  mb-0">
			                          		My TeamLeader: {{ $teamleader->first_name }} 
			                          	</p><br>
			                          	@endif
			                          	<p class="font-weight-medium  mb-0">
			                          		Date Of Birth: {{ $user->date_of_birth }} 
			                          	</p><br>
			                          	<p class="font-weight-medium  mb-0">
			                          		Gender: {{ $user->gender }} 
			                          	</p><br>
			                          	<p class="font-weight-medium  mb-0">
			                          		Address: {{ $user->address }} 
			                          	</p><br>
			                        </div>
                                </div>	
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection