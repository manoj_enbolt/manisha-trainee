@extends('head')
@section('content')
  <div class="app-content content" style="padding-top: 70px; padding-left: 40px;">
    @if(Session::has('success'))
        <div class="alert alert-success alert-dismissible" role="alert">
            {!! Session::get('success') !!}
        </div>
      @endif
    {{-- <div class="content-wrapper">
      <div class="content-body"><!-- ../../../theme-assets/images/carousel/22.jpg -->
        <section id="header-footer"><!-- Header footer section start -->
          <div class="row">
            <div class="col-xl-3 col-lg-3 col-md-3 col-sm-6 grid-margin stretch-card">
              <div class="card card-statistics">
                <div class="card-body">
                  <div class="clearfix">
                    <div class="float-left">
                      <h3><i class="ft-user"></i></h3>
                    </div>
                    <div class="float-right">
                      <h3 class="mb-0 text-right"> View Profile</h3>
                      <div class="fluid-container">
                        <p class="font-weight-medium text-right mb-0">My Profile</p>
                      </div>
                    </div>
                  </div>
                   <p class="text-muted mt-3 mb-0">
                    <a href="{{ url('/view/profile')  }}">
                      <i class="mdi mdi-alert-octagon mr-1" aria-hidden="true"></i> View Profile
                    </a>
                  </p>
                </div>
              </div>
            </div>
            <div class="col-xl-3 col-lg-3 col-md-3 col-sm-6 grid-margin stretch-card">
              <div class="card card-statistics">
                <div class="card-body">
                  <div class="clearfix">
                    <div class="float-left">
                      <h1 style="color: red;"><i class="ft-edit" ></i></h1>
                    </div>
                    <div class="float-right">
                      <h2 class="mb-0 text-right">Profile</h2>
                      <div class="fluid-container">
                        <p class="font-weight-medium text-right mb-0">My Profile</p>
                      </div>
                    </div>
                  </div>
                  <p class="text-muted mt-3 mb-0">
                    <a href="{{ url('/user/edit', $user->id)  }}">
                      <i class="mdi mdi-alert-octagon mr-1" aria-hidden="true"></i> Edit Profile
                    </a>
                  </p>
                </div>
              </div>
            </div>
          </div>
        </section>
      </div>
    </div> --}}
  </div>
@endsection