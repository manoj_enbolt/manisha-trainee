@extends('head')
@section('content')
   <div class="content" style="padding-top: 50px; padding-left: 50px;">
      @if(Session::has('success'))
          <div class="alert alert-success alert-dismissible" role="alert">
              {!! Session::get('success') !!}
          </div>
        @endif
        <div class="container-fluid">
          <div class="row">
              <div class="col-md-8">
                  <div class="card">
                    <div class="card-header card-header-primary">
                        <h4 class="card-title">Edit Holiday</h4>
                        <p class="card-category">Enter Details for Holiday...</p><hr>
                    </div>
                    <div class="card-body">
                        {!! Form::model($holidays, ['method' => 'PATCH', 'url' => '/admin/holiday/update/'.$holidays->id]) !!}

                          <div class="row">
                              <div class="col-md-6">
                                <div class="form-group" {{ $errors->has('holiday_date') ? ' is-invalid' : '' }}">
                                    <label class="bmd-label-floating">Select Holiday Date:</label>
                                    {!! Form::date('holiday_date', $holiday_date, ['class' => 'form-control','placeholder' => 'Select Holiday Date']) !!}
                                    {!! $errors->first('holiday_date','<span class="help-block" style="color:red;">:message</span>') !!}
                                </div>
                              </div>
                            </div>
                            <div class="row">
                              <div class="col-md-12">
                                <div class="form-group"{{ $errors->has('holiday_description') ? ' is-invalid' : '' }}">
                                    <label class="bmd-label-floating"> Holiday Description</label>
                                    {!! Form::textarea('holiday_description',NULL, ['rows' => 2, 'cols' => 54,'id' => 'holiday_description','class' => 'form-control','placeholder' => 'Holiday Description']) !!}
                                    {!! $errors->first('holiday_description','<span class="help-block" style="color:red;">:message</span>') !!}
                                </div>
                              </div>
                          </div>

                          {!! Form::submit('SUBMIT',['class' => 'btn btn-primary pull-right']) !!}&nbsp
                        {!! Form::close() !!}
                    </div>
                  </div>
              </div>
            </div>
        </div>
    </div>
@endsection