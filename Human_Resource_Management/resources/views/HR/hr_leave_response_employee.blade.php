@extends('head')
@section('content')
	<div class="app-content content" style="padding-top: 70px; padding-left: 40px;">
	    <div class="content-wrapper">
	    	@if(Session::has('success'))
                <div class="alert alert-success" role="alert">
                    {!! Session::get('success') !!}
                </div>
            @endif
			<div class="content-body">
				<div class="col-lg-6 grid-margin stretch-card">
					<div class="card">
						<div class="card-body">
							<div class="clearfix">
								<div class="float-left">
									<h4 class="card-title">Employee Leave Request</h4>
								</div>
							</div><hr><br>
							<div class="row" style="padding-left: 20px;">
                                <div class="col-xs-12 col-sm-10">
			                        <div class="fluid-container">
			                        	<p class="font-weight-medium  mb-0">
			                          		Leave Request Send By  : {{ $name->first_name }} 
			                          	</p><br>
			                          	<p class="font-weight-medium  mb-0">
			                          		Sender Email : {{ $name->email }} 
			                          	</p><br>
			                          	<p class="font-weight-medium  mb-0">
			                          		Start Leave Date: {{ $leave->start_leave }} 
			                          	</p><br>
			                          	<p class="font-weight-medium  mb-0">
			                          		End Leave Date: {{ $leave->end_leave }} 
			                          	</p><br>
		                          		<p class="font-weight-medium  mb-0">
			                          		HalfDay: {{ $leave->halfday }} 
			                          	</p><br>			                          	
			                          	<p class="font-weight-medium  mb-0">
			                          		Absence Date: {{ $leave->absence_date }} 
			                          	</p><br>
			                          	<p class="font-weight-medium  mb-0">
			                          		Leave Time: {{ $leave->leave_time }} 
			                          	</p><br>
			                          	<p class="font-weight-medium  mb-0">
			                          		Reason: {{ $leave->reason }} 
			                          	</p><br>
			                          	<p class="font-weight-medium  mb-0">
			                          		Address: {{ $leave->address }} 
			                          	</p><br>
			                          	<p class="font-weight-medium  mb-0">
			                          		Emergency Phone Number: {{ $leave->phone_number }} 
			                          	</p><br>
			                          	<p class="font-weight-medium  mb-0">
			                          		TeamLeader Response: {{ $leave->leave_response }}
			                          	</p><br>
			                          	@if($leave->leave_response == 'TL Approve Leave')
			                          		<div>
							                	{!! Form::open(['url' => '/hr/give/leave/response/employee','method' => 'post']) !!}
	                                            	{{method_field('PATCH')}}
	                                            	{{ Form::hidden('leave_id', $leave->id ) }}
	                                            	<div class="form-group" {{ $errors->has('leave_response') ? ' is-invalid' : '' }}">
								                      	<p class="font-weight-medium  mb-0">Leave Response : </p><br>
								                        {!! Form::select('leave_response', ['' => '--- Give Response ---','HR Approve Leave' => 'Approve Leave', 'HR Reject Leave' => 'Reject Leave'], array('class' => 'form-control')) !!}
								                        {!! $errors->first('leave_response','<span class="help-block" style="color:red;">:message</span>') !!}<br><br>
								                    </div>
							                        <div class="form-group" {{ $errors->has('comment') ? ' is-invalid' : '' }}">
							                        	{!! Form::textarea('comment',NULL, ['class' => 'form-control summernote-editor','rows' => 3, 'cols' => 50,'id' => 'comment','placeholder' => 'Give Reply..']) !!}
                                        				{!! $errors->first('comment','<span class="help-block" style="color:red;">:message</span>') !!}<br>
							                        	{!! Form::submit('SUBMIT',['class' => 'btn btn-primary', 'style' => 'margin-left:50px;']) !!}
							                        </div>
	                                           	{!! Form::close() !!}
						                	</div>
						                	@else 
							                	{{-- <p class="font-weight-medium  mb-0"> Give Leave Response By TeamLeader: {{ $leave->leave_response }}</p><br> --}}
						                @endif
			                        </div>
                                </div>	
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection