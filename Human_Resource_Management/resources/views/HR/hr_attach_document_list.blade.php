@extends('head')
@section('content')
<div class="app-content content" style="padding-top: 70px; padding-left: 40px;">
     @if(Session::has('success'))
        <div class="alert alert-success alert-dismissible" role="alert">
            {!! Session::get('success') !!}
        </div>
      @endif
    <div class="content-wrapper">
    <div class="content-body">
      <div class="col-lg-12 grid-margin stretch-card">
              <div class="card">
                <div class="card-body">
                  <div class="clearfix">
                      <div class="float-left">
                        <h4 class="card-title">Employee Document List</h4>
                      </div>
                    </div>
                    <div class="table-responsive">
                      <table class="table table-striped">
                        <thead>
                            <tr>
                              <th>No</th>
                              <th>User</th>
                              <th>Name</th>
                              <th>Email</th>
                              <th>Attach Document</th>
                            </tr>
                          </thead>
                          <tbody>
                              <?php $no=1; ?>
                              @foreach ($users as $user)
                                <tr>
                                  <td>{{ $no++ }}</td>
                                  <td class="py-1">
                                    <img style="width: 30px; height: 30px; border-radius: 50%" alt="{{ $user->profile_image }}" src="{{ asset('/storage/upload').'/'.$user->profile_image }}">
                                  </td>
                                  <td>{{ $user->first_name }}</td>
                                  <td>{{ $user->email}}</td>
                                  @if(!in_array($user->id, $document_ids))
                                    <td><a href="{{ url('/hr/attach/employee/document', $user->id) }}"><i class="ft-paperclip"></i></a></td>
                                    @else
                                     <td>Already Attach Document</td>
                                   @endif
                                </tr>
                              @endforeach
                              {{-- @else
                                <h4>Document Already Attached</h4>
                            @endif --}}
                        </tbody>
                      </table>
                    </div>
                </div>
              </div>
            </div>
           </div>
       </div>
   </div>
@endsection