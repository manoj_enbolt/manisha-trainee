@extends('head')
@section('content')
	<div class="app-content content" style="padding-top: 70px; padding-left: 40px;">
	    <div class="content-wrapper">
			<div class="content-body">
				<div class="col-lg-12 grid-margin stretch-card">
	              <div class="card">
	                <div class="card-body">
	                	<div class="clearfix">
	                      <div class="float-left">
	                        <h4 class="card-title">{{ $users->first_name }} Document</h4>
	                      </div>
	                      <div class="float-right">
	                      </div>
	                    </div><br>
	                  	<div class="table-responsive">
		                    <table class="table table-striped">
		                     	<thead>
			                        <tr>
			                        	<th>#</th>
			                          	<th>Document Name</th>
			                          	<th>Show Document</th>
			                          	<th>Download</th>
			                        </tr>
		                      	</thead>
		                      	<tbody>
		                      		@if($document->count() > 0)
		                        		<tr>
		                        			<td>1</td>
											<td>S.S.C Marksheet</td>
											<td>
												<a href="{{ URL::to( '/storage/upload/' . $document->ssc_marksheet)  }}" target="_blank">
													<img style="width: 100px; height: 100px; margin-left: 30px;" alt="{{ $document->ssc_marksheet }}" src="{{ asset('/storage/upload').'/'.$document->ssc_marksheet }}">
												</a>
											</td>
											<td>
												<a href="{{ url('/employee/ssc_marksheet/document/download', $document->id) }}" class="btn btn-outline-primary" style="margin-top: 30px;">
													<i class="ft-download"></i>
												</a>
											</td>
										</tr>
										<tr>
											<td>2</td>
											<td> H.S.C Marksheet</td>
											<td>
												<a href="{{ URL::to( '/storage/upload/' . $document->hsc_marksheet)  }}" target="_blank">
													<img style="width: 100px; height: 100px; margin-left: 30px;" alt="{{ $document->hsc_marksheet }}" src="{{ asset('/storage/upload').'/'.$document->hsc_marksheet }}">
												</a>
											</td>
											<td>
												<a href="{{ url('/employee/hsc_marksheet/document/download', $document->id) }}" class="btn btn-outline-primary" style="margin-top: 30px;">
													<i class="ft-download"></i>
												</a>
										</tr>
										<tr>
											<td>3</td>
											<td> Certificate</td>
											<td>
												<a href="{{ URL::to( '/storage/upload/' . $document->certificate)  }}" target="_blank">
													<img style="width: 100px; height: 100px; margin-left: 30px;" alt="{{ $document->certificate }}" src="{{ asset('/storage/upload').'/'.$document->certificate }}">
												</a>
											</td>
											<td>
												<a href="{{ url('/employee/certificate/document/download', $document->id) }}" class="btn btn-outline-primary" style="margin-top: 30px;">
													<i class="ft-download"></i>
												</a>
											</td>
										</tr>
										<tr>
											<td>4</td>
											<td> Election Card</td>
											<td>
												<a href="{{ URL::to( '/storage/upload/' . $document->election_card)  }}" target="_blank">
													<img style="width: 100px; height: 100px; margin-left: 30px;" alt="{{ $document->election_card }}" src="{{ asset('/storage/upload').'/'.$document->election_card }}">
												</a>
											</td>
											<td>
												<a href="{{ url('/employee/electioncard/document/download', $document->id) }}" class="btn btn-outline-primary" style="margin-top: 30px;">
													<i class="ft-download"></i>
												</a>
											</td>
										</tr>
										<tr>
											<td>5</td>
											<td> Aadhar Card</td>
											<td>
												<a href="{{ URL::to( '/storage/upload/' . $document->aadhar_card)  }}" target="_blank">
													<img style="width: 100px; height: 100px; margin-left: 30px;" alt="{{ $document->aadhar_card }}" src="{{ asset('/storage/upload').'/'.$document->aadhar_card }}">
												</a>
											</td>
											<td>
												<a href="{{ url('/employee/aadharcard/document/download', $document->id) }}" class="btn btn-outline-primary" style="margin-top: 30px;">
													<i class="ft-download"></i>
												</a>
											</td>
										</tr>
										<tr>
											<td>6</td>
											<td> Pancard Card</td>
											<td>
												<a href="{{ URL::to( '/storage/upload/' . $document->pancard)  }}" target="_blank">
													<img style="width: 100px; height: 100px; margin-left: 30px;" alt="{{ $document->pancard }}" src="{{ asset('/storage/upload').'/'.$document->pancard }}">
												</a>
											</td>
											<td>
												<a href="{{ url('/employee/pancard/document/download', $document->id) }}" class="btn btn-outline-primary" style="margin-top: 30px;">
													<i class="ft-download"></i>
												</a>
											</td>
			                          	</tr>  
			                        	@else 
			                        		<h2>No Any Document Available</h2>
			                        @endif
			                    </tbody>
		                    </table>
	                  	</div>
	                </div>
	              </div>
	            </div>
	        </div>
	   	</div>
	</div>
@endsection