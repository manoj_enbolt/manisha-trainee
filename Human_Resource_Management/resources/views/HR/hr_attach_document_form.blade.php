@extends('head')
@section('content')
    <div class="content" style="padding-top: 80px; padding-left: 50px;">
      @if(Session::has('success'))
        <div class="alert alert-success alert-dismissible" role="alert">
            {!! Session::get('success') !!}
        </div>
      @endif
        <div class="container-fluid">
          <div class="row">
            <div class="col-md-8">
              <div class="card">
                <div class="card-header card-header-primary">
                  <h4 class="card-title"> Attach Documents</h4>
                  <p class="card-category">Enter Details below..</p><hr>
                </div>
                <div class="card-body">
                  {!! Form::open(['url' => '/hr/attachment/form/store','method' => 'post','files' => true]) !!}
                  {{ Form::hidden('user_id', $user->id ) }}
                    <div class="row">
                      <div class="col-md-6">
                        <div class="form-group">
                          <p class="font-weight-medium  mb-0">Name : {{ $user->first_name }} </p>
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="form-group">
                          <p class="font-weight-medium  mb-0">Email : {{ $user->email }}</p>
                        </div>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-4">
                        <div class="form-group" {{ $errors->has('ssc_marksheet') ? ' is-invalid' : '' }}">
                          <label class="bmd-label-floating"> Attach S.S.C Marksheet</label>
                          {!! Form::file('ssc_marksheet',['class'=>'form-control', 'placeholder'=>'Attach AadharCard']) !!}
                          {!! $errors->first('ssc_marksheet','<span class="help-block" style="color:red;">:message</span>') !!}
                        </div>
                      </div>
                      <div class="col-md-4">
                        <div class="form-group" {{ $errors->has('hsc_marksheet') ? ' is-invalid' : '' }}">
                          <label class="bmd-label-floating">Attach H.S.C Marksheet</label>

                          {!! Form::file('hsc_marksheet',['class'=>'form-control', 'placeholder'=>'Attach H.S.C Marksheet']) !!}
                          {!! $errors->first('hsc_marksheet','<span class="help-block" style="color:red;">:message</span>') !!}
                        </div>
                      </div>
                       <div class="col-md-4">
                        <div class="form-group" {{ $errors->has('certificate') ? ' is-invalid' : '' }}">
                          <label class="bmd-label-floating">Attach Other Degree Certificate</label>

                          {!! Form::file('certificate',['class'=>'form-control', 'placeholder'=>'Attach Other Degree Certificate']) !!}
                          {!! $errors->first('certificate','<span class="help-block" style="color:red;">:message</span>') !!}
                        </div>
                      </div>
                    </div>
                     <div class="row">
                      <div class="col-md-12">
                        <div class="form-group" {{ $errors->has('election_card') ? ' is-invalid' : '' }}">
                          <label class="bmd-label-floating">Attach Election Card</label>
                          {!! Form::file('election_card',['class'=>'form-control', 'placeholder'=>'Profile Image']) !!}
                          {!! $errors->first('election_card','<span class="help-block" style="color:red;">:message</span>') !!}
                        </div>
                      </div>
                  	</div>
                    <div class="row">
                      <div class="col-md-6">
                        <div class="form-group" {{ $errors->has('aadhar_card') ? ' is-invalid' : '' }}">
                          <label class="bmd-label-floating"> Attach Aadhar Card</label>
                          {!! Form::file('aadhar_card',['class'=>'form-control', 'placeholder'=>'Attach AadharCard']) !!}
                          {!! $errors->first('aadhar_card','<span class="help-block" style="color:red;">:message</span>') !!}
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="form-group" {{ $errors->has('pancard') ? ' is-invalid' : '' }}">
                          <label class="bmd-label-floating">Attach PanCard</label>
                          {!! Form::file('pancard',['class'=>'form-control', 'placeholder'=>'Attach PanCard']) !!}
                          {!! $errors->first('pancard','<span class="help-block" style="color:red;">:message</span>') !!}
                        </div>
                      </div>
                    </div>
                      {!! Form::submit('SUBMIT',['class' => 'btn btn-primary pull-right']) !!}&nbsp
                  {!! Form::close() !!}
                </div>
              </div>
            </div>
          </div>
        </div>
    </div>
@endsection