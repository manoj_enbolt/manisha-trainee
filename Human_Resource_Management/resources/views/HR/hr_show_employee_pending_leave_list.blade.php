@extends('head')
@section('content')
	<div class="app-content content" style="padding-top: 70px; padding-left: 40px;">
	    <div class="content-wrapper">
			<div class="content-body">
				<div class="col-lg-12 grid-margin stretch-card">
	              <div class="card">
	                <div class="card-body">
	                	<div class="clearfix">
	                      <div class="float-left">
	                        <h4 class="card-title"> Employee Pending Leave</h4>
	                      </div>
	                      <div class="float-right">
	                      </div>
	                    </div><br>
	                  	<div class="table-responsive">
		                    <table class="table table-striped">
		                     	<thead>
			                        <tr>
			                        	<th>#</th>
			                          	<th>Subject</th>
			            				<th>Action</th>
			                        </tr>
		                      	</thead>
		                      	<tbody>
		                      		@if($leave_pending->count() > 0)
			                      	 	<?php $no=1; ?>
			                      	 	@foreach ($leave_pending as $leave)
			                        		<tr>
												<td>{{ $no++ }}</td>
												<td>{{ $leave->reason }}</td>
												<td><a href=" {{ url('/hr/leave/response', $leave->id) }}"> Approve Leave </a></td>
				                          	</tr>
				                        @endforeach
				                       	@else 
				                       		<h4 class="card-title">Not Any Leave Available In Pending List..</h4>
				                    @endif
			                    </tbody>
		                    </table>
	                  	</div>
	                </div>
	              </div>
	            </div>
	        </div>
	   	</div>
	</div>
@endsection