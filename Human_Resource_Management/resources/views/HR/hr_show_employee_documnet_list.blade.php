@extends('head')
@section('content')
<div class="app-content content" style="padding-top: 70px; padding-left: 40px;">
    <div class="content-wrapper">
    <div class="content-body">
      <div class="col-lg-12 grid-margin stretch-card">
              <div class="card">
                <div class="card-body">
                  <div class="clearfix">
                      <div class="float-left">
                        <h4 class="card-title">Show Employee Document</h4>
                      </div>
                    </div>
                    <div class="table-responsive">
                      <table class="table table-striped">
                        <thead>
                            <tr>
                              <th>No</th>
                              <th>Employee Name</th>
                              <th>Employee Email</th>
                              <th>Show Attach Document</th>
                            </tr>
                          </thead>
                          <tbody>
                            <?php $no=1; ?>
                            @foreach ($employee_name as $user)
                              <tr>
                                <td>{{ $no++ }}</td>
                                <td>{{ $user->first_name }}</td>
                                <td>{{ $user->email}}</td>
                                <td>
                                	<a href="{{ url('/hr/show/attach/employee/document', $user->id) }}">Show Document List
                                	</a>
                                </td>
                              </tr>
                            @endforeach
                        </tbody>
                      </table>
                    </div>
                </div>
              </div>
            </div>
           </div>
       </div>
   </div>
@endsection