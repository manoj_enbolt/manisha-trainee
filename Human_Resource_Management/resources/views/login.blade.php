<!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta name="description" content="Chameleon Admin is a modern Bootstrap 4 webapp &amp; admin dashboard html template with a large number of components, elegant design, clean and organized code.">
    <meta name="keywords" content="admin template, Chameleon admin template, dashboard template, gradient admin template, responsive admin template, webapp, eCommerce dashboard, analytic dashboard">
    <meta name="author" content="ThemeSelect">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <link rel="icon" type="image/png" href="{{ asset('theme-assets/images/logo/em-hrm icon.png') }}">

    <title>Human Resource Management</title>

    <link href="https://fonts.googleapis.com/css?family=Muli:300,300i,400,400i,600,600i,700,700i%7CComfortaa:300,400,700" rel="stylesheet">
    <link href="https://maxcdn.icons8.com/fonts/line-awesome/1.1/css/line-awesome.min.css" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('theme-assets/css/vendors.css') }}">

    <link rel="stylesheet" href="{{ asset('theme-assets/vendors/css/charts/chartist.css') }}">

    <link rel="stylesheet" href="{{ asset('theme-assets/css/app-lite.css') }}">

    <link rel="stylesheet" href="{{ asset('theme-assets/css/core/menu/menu-types/vertical-menu.css') }}">

    <link rel="stylesheet" href="{{ asset('theme-assets/css/core/colors/palette-gradient.css') }}">

    <link rel="stylesheet" href="{{ asset('theme-assets/css/pages/dashboard-ecommerce.css') }}">
  </head>

  <body class="vertical-layout vertical-menu 2-columns   menu-expanded fixed-navbar" data-open="click" data-menu="vertical-menu" data-color="bg-chartbg" data-col="2-columns">
    <div class="content" style="padding-top: 40px; padding-left: 100px;">
        <div class="container-fluid">
          <div class="row">
            <div class="col-md-8">
              <div class="card">
                <div class="card-header card-header-primary">
                  <h4 class="card-title">Sign In Panel HRM</h4>
                  <p class="card-category">Enter below Details..</p><hr>
                </div>
                @if(Session::has('success'))
                    <div class="alert alert-success alert-dismissible" role="alert">
                        {!! Session::get('success') !!}
                    </div>
                @endif
                <div class="card-body">
                    {!! Form::open(['method' => 'post','style' => 'padding-left: 40px;']) !!}

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group" {{ $errors->has('email') ? ' is-invalid' : '' }}>
                                    <label class="bmd-label-floating">Email address</label>
                                    {!! Form::text('email','', ['class' => 'form-control','placeholder' => 'abc@xyz.com']) !!}<br>
                                    {!! $errors->first('email','<span class="help-block" style="color:red;">:message</span>') !!}
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group" {{ $errors->has('password') ? ' is-invalid' : '' }}>
                                    <label class="bmd-label-floating">Password</label>
                                    {!! Form::password('password', ['class' => 'form-control','placeholder' => 'Password'])!!}<br>
                                    {!! $errors->first('password','<span class="help-block" style="color:red;">:message</span>') !!}
                                </div>
                            </div>
                        </div>

                      {!! Form::submit('SUBMIT',['class' => 'btn btn-primary']) !!}&nbsp
                      {!! Form::reset('CANCEL',['class' => 'btn btn-default']) !!}

                    {!! Form::close() !!}
                </div>
              </div>
            </div>
          </div>
        </div>
    </div>
    <script src="{{asset('theme-assets/vendors/js/vendors.min.js') }}" type="text/javascript"></script>

    <script src="{{asset('theme-assets/vendors/js/charts/chartist.min.js') }}" type="text/javascript"></script>
    
    <script src="{{asset('theme-assets/js/core/app-menu-lite.js') }}" type="text/javascript"></script>

    <script src="{{asset('theme-assets/js/core/app-lite.js') }}" type="text/javascript"></script>
    
    <script src="{{asset('theme-assets/js/scripts/pages/dashboard-lite.js') }}" type="text/javascript"></script>
  </body>
</html>