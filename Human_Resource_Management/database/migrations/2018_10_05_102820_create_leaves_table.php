    <?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLeavesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('leaves', function (Blueprint $table) {
            $table->increments('id');
            $table->date('start_leave');
            $table->date('end_leave');
            $table->string('halfday')->default('0');
            $table->date('absence_date');
            $table->string('leave_time')->default('fullday');
            $table->string('reason');
            $table->string('address');
            $table->string('phone_number');
            $table->integer('sender_id')->unsigned();
            $table->foreign('sender_id')->references('id')->on('users');
            $table->integer('team_leader_id')->unsigned();
            $table->foreign('team_leader_id')->references('id')->on('users');
            $table->string('comment')->nullable()->change();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('leaves');
    }
}
