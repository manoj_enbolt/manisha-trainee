<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Document extends Model
{
 	protected $table = "documents";
    protected $fillable = [
    	'ssc_marksheet', 
    	'hsc_marksheet',
    	'certificate',
    	'election_card',
    	'aadhar_card',
    	'pancard',
    	'sender_id',
    	'user_id',
    ];   
}
