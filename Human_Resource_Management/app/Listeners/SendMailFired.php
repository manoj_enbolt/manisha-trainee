<?php

namespace App\Listeners;

use App\Events\SendMail;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\User;
use App\Leave;
use Mail;
use DB;

class SendMailFired
{
    // public $data;
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
    
    }

    /**
     * Handle the event.
     *
     * @param  SendMail  $event
     * @return void
     */
    public function handle(SendMail $data)
    {
        $leave = Leave::find($data->data['id'])->toArray();
        $user = User::where('id', $data->data['sender_id'])->first();
        $team_leader = User::where('id', $leave['team_leader_id'])->first();
       
        Mail::send('email', $leave, function($message) use ($leave, $user, $team_leader) {
                    $message->from('mmanishaprajapati@gmail.com');
                    $message->to($team_leader['email'])->subject('Leave Apply Request');
                }); 
    }
}
