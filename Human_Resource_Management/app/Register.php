<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\Model;

class Register extends Authenticatable
{
   use Notifiable;
	
    protected $fillable = [
        'first_name', 'last_name', 'date_of_birth','email','password','address','profile_image','gender','role',
    ];
}
