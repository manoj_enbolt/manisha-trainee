<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Holiday extends Model
{
	protected $dates = ['holiday_date',];
    protected $table = "holidays";
    protected $fillable = ['holiday_date', 'holiday_description',];
}
