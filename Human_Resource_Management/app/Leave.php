<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Leave extends Model
{
    // protected $dates = ['start_leave', 'end_leave', 'another_date_field'];
    protected $table = "leaves";
    protected $fillable = [
    	'start_leave', 
    	'end_leave',
    	'halfday',
    	'absence_date',
    	'leave_time',
    	'reason',
    	'address',
    	'phone_number',
    	'sender_id',
        'team_leader_id',
        'comment',
        'leave_response',
    ];
}
