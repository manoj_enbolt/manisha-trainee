<?php

namespace App\Http\Middleware;

use Closure;

class CheckRoleMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
       if ( Auth::check() && Auth::user()->isAdmin() )
        {
            return $next($request);
        }
        //If user role is student
        if(Auth::check() && auth()->user()->role === 'Employee')
        {
             return view('Employee.employee_dashboard');
             
        }

        //If user role is teacher
        if(Auth::check() && auth()->user()->role ==='TeamLeader
')
        {

             return view('TeamLeader.teamleader_dashboard');
             

        }
        //default redirect
        return redirect('welcome');
    }
}
