<?php

namespace App\Http\Controllers\Employee;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Events\SendMail;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldBeQueued;
// use Illuminate\Support\Facades\Mail;
use App\User;
use App\Leave;
use App\Document;
use App\Mail\email;
use Auth; 
use DB;
use Mail;
use Event;

class EmployeeController extends Controller
{
	/**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function showEmployeeDashboard()
    {
        $user = User::find(Auth::id());
        return view('Employee.employee_dashboard', compact('user'));
    }

	public function leaveApply() //show leave form with assign teamleader name
    {
        $user = User::find(Auth::id());
        $handle_by = User::find($user->handle_user_id);
        $leave_check = Leave::find($user->halfday);
        $assignable = User::whereHAS('roles', function($q) {
            $q->where('name', 'TeamLeader');
        })->get();

        foreach ($assignable as $value) {
            $assignable_users[$value->id] = $value->first_name;
        }
        return view('Employee.employee_leave_form', compact('user', 'handle_by', 'leave_check', 'assignable', 'assignable_users'));
    }

    public function storeLeave(Request $request) // store leave datatable and send mail to specify teamleader and HR 
    {
        if (!$request->absence_date) {
            $request->absence_date = $request->start_leave;
        }

        if($request->halfday == NULL) {
            $request['absence_date'] = $request->start_leave;
            $request['leave_time'] = 'FullDay';
        } else {
            $request['start_leave'] = $request->absence_date;
            $request['end_leave'] = $request->absence_date;
        }

        $this->validate($request, [
            'start_leave' => 'required|date_format:Y-m-d|after:yesterday|after:today',
            'end_leave' => 'required|date_format:Y-m-d|after:today',
            'absence_date' => 'required|date_format:Y-m-d|after:yesterday',
            'leave_time' => 'required',
            'reason' => 'required',
            'address' => 'required',
            'phone_number' => 'required|numeric|digits:10',
            'assign_teamleader' => 'required',
        ]);

        $leave = $request->all();
        $leave['sender_id'] = Auth::user()->id;
        $leave['team_leader_id'] = $request->assign_teamleader;

        $data = Leave::create($leave);

        Event::fire(new SendMail($data));
        return redirect()->back()->with('success', 'Your Leave Request Send Successfully',compact('handle_by'));
    }

    public function showEmployeePendingLeave() // show pending leave list
    {   
        $user = User::find(Auth::id());
        $leave_pending = Leave::select('leaves.*')
                ->where(['leave_response' => 'Pending Leave', 'sender_id' => $user->id])
                ->get();
        return view('Employee.employee_show_pending_leave_list', compact('user', 'leave_pending'));
    }

    public function showResponseLeave() //show leave response reject or approve
    {
        $user = User::find(Auth::id());
        $leave_approve = Leave::select('leaves.*')
                    ->where('sender_id', $user->id)
                    ->whereIn('leave_response', ['TL Approve Leave', 'TL Reject Leave','HR Approve Leave', 'HR Reject Leave'])
                    ->get();
        return view('Employee.employee_show_approve_leave_list', compact('user', 'leave_approve'));
    }

    public function employeeEditLeave($id)
    {
        $user = User::find(Auth::id());
        $leave = Leave::find($id);
        $handle_by = User::find($user->handle_user_id);
        $leave_check = Leave::find($user->halfday);
        $assignable = User::whereHAS('roles', function($q) {
            $q->where('name', 'TeamLeader');
        })->get();

        foreach ($assignable as $value) {
            $assignable_users[$value->id] = $value->first_name;
        }
        if($leave->sender_id == Auth::user()->id) {
            return view('Employee.employee_leave_edit_form', compact('user', 'leave', 'handle_by', 'leave_check', 'assignable', 'assignable_users'));
        } else {
            return view('Employee.employee_not_view_other_user_leave_data');
        }
        
    }

    public function employeeLeaveUpdate(Request $request, $id)
    {
        if (!$request->absence_date) {
            $request->absence_date = $request->start_leave;
        }

        if($request->halfday == NULL) {
            $request['absence_date'] = $request->start_leave;
            $request['leave_time'] = 'FullDay';
        } else {
            $request['start_leave'] = $request->absence_date;
            $request['end_leave'] = $request->absence_date;
        }

        $this->validate($request, [
            'start_leave' => 'required|date_format:Y-m-d|after:yesterday|after:today',
            'end_leave' => 'required|date_format:Y-m-d|after:today',
            'absence_date' => 'required|date_format:Y-m-d|after:yesterday',
            'leave_time' => 'required',
            'reason' => 'required',
            'address' => 'required',
            'phone_number' => 'required|numeric|digits:10',
            'assign_teamleader' => 'required',
        ]);

        $leave = $request->all();
        $leave['sender_id'] = Auth::user()->id;
        $leave['team_leader_id'] = $request->assign_teamleader;

        $data = Leave::find($id)->update($leave);

        $leave_id = Leave::where('id', $request->id)->first()->toArray();
        $user = User::where('id', \Auth::id())->first();
        $team_leader = User::where('id', $leave_id['team_leader_id'])->first();
// dd($team_leader['email']);
        Mail::send('email', $leave_id, function($message) use ($leave_id, $user, $team_leader) {
                    $message->from('mmanishaprajapati@gmail.com');
                    $message->to($team_leader['email'])->subject('Leave Apply Request');
                }); 
        // Event::fire(new SendMail($data));
        return redirect()->back()->with(['success' => 'Your Leave Upadete & Request Send Successfully', 'leave_id' => $leave_id, 'user' => $user, 'team_leader' => $team_leader]);
    }

    public function employeeShowPendingLeaveAllData($id)
    {
        $user = User::find(Auth::id());
        $leave = Leave::find($id);
        if($leave->sender_id == Auth::user()->id) {
            return view('Employee.employee_show_pending_leave_data', compact('user', 'leave'));
        } else {
            return view('Employee.employee_not_view_other_user_leave_data');
        }
       
    }

    public function showMyLeaveResponse($id)
    {
        $user = User::find(Auth::id());
        $leave = Leave::find($id);
        if($leave->sender_id == Auth::user()->id) {
            return view('Employee.employee_show_leave_response_data', compact('user', 'leave'));   
        } else {
            return view('Employee.employee_not_view_other_user_leave_response');
        }
    }

    public function employeeShowDocumentMenu()
    {
        $document = Document::where('user_id', Auth::user()->id)->first();
        // dd($document);
        return view('Employee.employee_show_document_menu', compact('document'));
    }

    public function employeeShowOwnDocument()
    {
        $user = User::find(Auth::id());
        $users = User::find($user->id);
        $document = Document::where('user_id', $users->id)->first();
        return view('Employee.employee_show_own_document', compact('user', 'document'));
    }

    public function employeeEditOwnDocument($id)
    {
        $user = User::find(Auth::id());
        $document = Document::find($id);
        if($document->user_id == Auth::user()->id) {
            return view('Employee.employee_edit_own_document', compact('user', 'document'));
        } else {
            return view('Employee.employee_not_access_document');
        }
    }

    public function employeeDocumentUpdate(Request $request, $id)
    {
        $this->validate($request,[
            'ssc_marksheet' => 'mimes:jpeg,jpg,png,gif,pdf,doc,docx',
            'hsc_marksheet' => 'mimes:jpeg,jpg,png,gif,pdf,doc,docx',
            'certificate' => 'mimes:jpeg,jpg,png,gif,pdf,doc,docx',
            'election_card' => 'mimes:jpeg,jpg,png,gif,pdf,doc,docx',
            'aadhar_card' => 'mimes:jpeg,jpg,png,gif,pdf,doc,docx',
            'pancard' => 'mimes:jpeg,jpg,png,gif,pdf,doc,docx',
        ]);
        $user = User::find(Auth::id());
        // $input = $request->all();

        if($request->hasFile('ssc_marksheet')) {
            $ssc_marksheet_filename = $request->ssc_marksheet->getClientOriginalName();
            $request->ssc_marksheet->storeAs('/public/upload', $ssc_marksheet_filename);
            $input['ssc_marksheet'] = $request->ssc_marksheet->getClientOriginalName();
        }
        if($request->hasFile('hsc_marksheet')) {
            $hsc_marksheet_filename = $request->hsc_marksheet->getClientOriginalName();
            $request->hsc_marksheet->storeAs('/public/upload', $hsc_marksheet_filename);
            $input['hsc_marksheet'] = $request->hsc_marksheet->getClientOriginalName();
        }
        if($request->hasFile('certificate')) {
            $certificate_filename = $request->certificate->getClientOriginalName();
            $request->certificate->storeAs('/public/upload', $certificate_filename);
            $input['certificate'] = $request->certificate->getClientOriginalName();
        }
        if($request->hasFile('election_card')) {
            $election_card_filename = $request->election_card->getClientOriginalName();
            $request->election_card->storeAs('/public/upload', $election_card_filename);
            $input['election_card'] = $request->election_card->getClientOriginalName();
        }
        if($request->hasFile('aadhar_card')) {
            $aadhar_card_filename = $request->aadhar_card->getClientOriginalName();
            $request->aadhar_card->storeAs('/public/upload', $aadhar_card_filename);
            $input['aadhar_card'] = $request->aadhar_card->getClientOriginalName();
        }
        if($request->hasFile('pancard')) {
            $pancard_filename = $request->pancard->getClientOriginalName();
            $request->pancard->storeAs('/public/upload', $pancard_filename);
            $input['pancard'] = $request->pancard->getClientOriginalName();
        }


        $input['sender_id'] = Auth::user()->id;
        $input['user_id'] = Auth::user()->id;

        Document::find($id)->update($input);
        return redirect('employee/show/my/document/menu')->with(['success' => 'Your Document Update Succesfully..','user' => $user]);
    }

    // public function employeeDocumentDownload($id)
    // {
    //     $document = Document::where('id', $id)->firstOrFail();
    //     $filepath = storage_path() . "/app/public/upload/" . $document->ssc_marksheet;
    //     $headers = array(
    //                 'Content-Type:  csv' ,
    //                 'Content-Disposition: attachment; filename='.$document->ssc_marksheet,
    //             );
    //     if (file_exists($_SERVER["DOCUMENT_ROOT"].'/storage/upload/9607499_orig.jpg')) {
    //     // dd($filepath);
    //         return \Response::download( $filepath, $document->ssc_marksheet, $headers );
    //     } else {
    //         exit( 'Request file does not exit');
    //     }
    // }

    public function employeeSSCDocumentDownload($id)
    {
        $document = Document::where('id', $id)->firstOrFail();
        $path = public_path(). '/storage/upload/' .$document->ssc_marksheet;
        return response()->download($path, $document->original_filename, [
            'Content-Type' => $document->mime
         ]);
    }

    public function employeeHSCMarksheetDownload($id)
    {
        $document = Document::where('id', $id)->firstOrFail();
        $path = public_path(). '/storage/upload/' . $document->hsc_marksheet;
        return response()->download($path, $document->original_filename, [
            'Content-Type' => $document->mime
        ]);
    }

    public function employeeCertificateDownload($id)
    {
        $document = Document::where('id', $id)->firstOrFail();
        $path = public_path(). '/storage/upload/' . $document->certificate;
        return response()->download($path, $document->original_filename, [
            'Content-Type' => $document->mime
        ]);
    }

    public function employeeElectioncardDownload($id)
    {
        $document = Document::where('id', $id)->firstOrFail();
        $path = public_path(). '/storage/upload/'. $document->election_card;
        return response()->download($path, $document->original_filename, [
            'Content-Type' => $document->mime
        ]);
    }

    public function employeeAadharCardDownload($id)
    {
        $document = Document::where('id', $id)->firstOrFail();
        $path = public_path(). '/storage/upload/'. $document->aadhar_card;
        return response()->download($path, $document->original_filename, [
            'Content-Type' => $document->mime
        ]);
    }

    public function employeePancardDownload($id)
    {
        $document = Document::where('id', $id)->firstOrFail();
        $path = public_path(). '/storage/upload/'. $document->pancard;
        return response()->download($path, $document->original_filename, [
            'Content-Type' => $document->mime
        ]);
    }
}