<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Holiday;
use Spatie\Permission\Models\Role;
use DB;
use Auth;

class RegistrationController extends Controller
{
    public function register()
	{
        $user = User::find(Auth::id());
        $roles = Role::pluck('name','name')->all();
		return view('register', compact('user', 'roles'));
	}
	public function store(Request $request)
    {
        $this->validate($request,[
            'first_name' => 'required|string|regex:/^[a-zA-Z]+$/u',
            'last_name' => 'required|string',
            'date_of_birth' => 'required|before:' . date('Y-m-d') . '|date_format:Y-m-d',
            'email' => 'required',
            'password' => 'required|min:5',
            'address' => 'required',
            'profile_image' => 'mimes:jpeg,jpg,png,gif|required|max:10000',
            'gender' => 'required',
            // 'role' => 'required',
        ]);
        if($request->hasFile('profile_image'))
        {
            $filename = $request->profile_image->getClientOriginalName();
            $request->profile_image->storeAs('/public/upload',$filename);
        }
        $input = $request->all();
        $input['password'] = bcrypt($request->password);
        $input['profile_image'] = $request->profile_image->getClientOriginalName();

        $user = User::create($input);
        $user->assignRole($request->input('roles'));
        return redirect('/admin/register')->with('success','User Added Succesfully');
    }

    public function edit($id)
    {
        $user = User::find(Auth::id());
        $users = User::find($id);
        return view('edit',compact('user','users'));
    }
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'first_name' => 'required|string|regex:/^[a-zA-Z]+$/u',
            'last_name' => 'required|string|regex:/^[a-zA-Z]+$/u',
            'date_of_birth' => 'required|before:' . date('Y-m-d') . '|date_format:Y-m-d',
            'email' => 'required',
            'password' => 'required|min:5',
            'address' => 'required',
            'profile_image' => 'required',
            'gender' => 'required',
        ]);
        $user = User::find(Auth::id());
        if($request->hasFile('profile_image'))
        {
            $filename = $request->profile_image->getClientOriginalName();
            $request->profile_image->storeAs('/public/upload',$filename);
        }
        $input = $request->all();
        $input['password'] = bcrypt($request->password);
        $input['profile_image'] = $request->profile_image->getClientOriginalName();

        User::find($id)->update($input);
        return redirect()->back()->with(['success' => 'Your Profile Update Succesfully..','user' => $user]);
    }

    public function ShowHolidaySchedule()
    {
        $user = User::find(Auth::id());
        $holidays_schedule = Holiday::all();
        // dd($holidays_schedule);
        return view('show_holiday_schedule', compact('user', 'holidays_schedule'));
    }
}
