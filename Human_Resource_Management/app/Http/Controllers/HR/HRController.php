<?php

namespace App\Http\Controllers\HR;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Leave;
use App\Document;
use DB;
use Auth;

class HRController extends Controller
{
    public function showHRDashboard()
    {
    	$user = User::find(Auth::id());
        $users = User::whereHAS('roles', function($q) {
            $q->where('name', 'Employee');
        })->get();
    	return view('HR.hr_dashboard', compact('user', 'users'));
    }

    public function employeeAttachDocumentList()
    {
        $user = User::find(Auth::id());
        $users = User::whereHAS('roles', function($q) {
                    $q->where('name', 'Employee');
                })->get();
        $document_ids = Document::pluck('user_id')->toArray();

        return view('HR.hr_attach_document_list', compact('user','users', 'document', 'document_ids'));
    }

    public function hrAttachEmployeeDocument($id)
    {   
        $user = User::find($id);
        return view('HR.hr_attach_document_form', compact('user'));
        // if($user->handle_user_id == 0) {
        //     return view('HR.hr_not_attach_other_employee_document');
        // } else {
        //     return view('HR.hr_attach_document_form', compact('user'));
        // }
    }

    public function attachmentFormStore(Request $request)
    {
        $this->validate($request,[
            'ssc_marksheet' => 'required|mimes:jpeg,jpg,png,gif,pdf,doc,docx',
            'hsc_marksheet' => 'required|mimes:jpeg,jpg,png,gif,pdf,doc,docx',
            'certificate' => 'required|mimes:jpeg,jpg,png,gif,pdf,doc,docx',
            'election_card' => 'required|mimes:jpeg,jpg,png,gif,pdf,doc,docx',
            'aadhar_card' => 'required|mimes:jpeg,jpg,png,gif,pdf,doc,docx',
            'pancard' => 'required|mimes:jpeg,jpg,png,gif,pdf,doc,docx',
        ]);
        if($request->hasFile('ssc_marksheet')) {
            $ssc_marksheet_filename = $request->ssc_marksheet->getClientOriginalName();
            $request->ssc_marksheet->storeAs('/public/upload', $ssc_marksheet_filename);
        }
        if($request->hasFile('hsc_marksheet')) {
            $hsc_marksheet_filename = $request->hsc_marksheet->getClientOriginalName();
            $request->hsc_marksheet->storeAs('/public/upload', $hsc_marksheet_filename);
        }
        if($request->hasFile('certificate')) {
            $certificate_filename = $request->certificate->getClientOriginalName();
            $request->certificate->storeAs('/public/upload', $certificate_filename);
        }
        if($request->hasFile('election_card')) {
            $election_card_filename = $request->election_card->getClientOriginalName();
            $request->election_card->storeAs('/public/upload', $election_card_filename);
        }
        if($request->hasFile('aadhar_card')) {
            $aadhar_card_filename = $request->aadhar_card->getClientOriginalName();
            $request->aadhar_card->storeAs('/public/upload', $aadhar_card_filename);
        }
        if($request->hasFile('pancard')) {
            $pancard_filename = $request->pancard->getClientOriginalName();
            $request->pancard->storeAs('/public/upload', $pancard_filename);
        }
        $input = $request->all();
    
        $input['ssc_marksheet'] = $request->ssc_marksheet->getClientOriginalName();
        $input['hsc_marksheet'] = $request->hsc_marksheet->getClientOriginalName();
        $input['certificate'] = $request->certificate->getClientOriginalName();
        $input['election_card'] = $request->election_card->getClientOriginalName();
        $input['aadhar_card'] = $request->aadhar_card->getClientOriginalName();
        $input['pancard'] = $request->pancard->getClientOriginalName();

        $input['sender_id'] = Auth::user()->id;
        $input['user_id'] = $request->user_id;
        $document = Document::create($input);
        return redirect('/hr/employee/attach/document/list')->with('success', 'Document Attach Successfully');
    }

    public function showEmployeeDocumentList(Request $request)
    {
        $user = User::find(Auth::id());
        $documents = Document::all();
        $employee_name = DB::table('documents')
                        ->select('documents.*', 'users.*')
                        ->join('users', 'documents.user_id', '=', 'users.id')
                        ->get();
        return view('HR.hr_show_employee_documnet_list', compact('user', 'documents', 'employee_name'));
    }

    public function showAttachEmployeeDocument($id)
    {
        $user = User::find(Auth::id());
        $users = User::find($id);
        $document = Document::where('user_id', $id)->first();
        return view('HR.hr_show_attach_employee_document', compact('user', 'users', 'document'));
    }

    public function giveLeaveResponseToTeamLeader($id)
    {
        $user = User::find(Auth::id());
        $leave = Leave::find($id);
        $teamleader = DB::table('leaves')
                        ->where(['leaves.id' => $leave->id,  'team_leader_id' => '0' ])
                        ->select('leaves.*', 'users.*')
                        ->leftjoin('users', 'leaves.sender_id', '=', 'users.id')
                        ->first();
        if($leave->team_leader_id == 0 && $leave->leave_response != 'Pending Leave') {
            return view('HR.hr_leave_response_teamleader', compact('user', 'leave', 'teamleader'));
        }
        else {
            return view('HR.hr_not_show_employee_pending_leave');
        }
        
    }

    public function giveLeaveResponseToEmployee($id)
    {
    	$user = User::find(Auth::id());
    	$leave = Leave::find($id);
    	$name = DB::table('leaves')
                ->where(['leaves.id' => $leave->id,])
                ->select('leaves.*', 'users.first_name', 'users.email')
                ->join('users', 'leaves.sender_id', '=', 'users.id')
                ->first();

        if($leave->team_leader_id != 0 && $leave->leave_response == 'TL Approve Leave') {
            return view('HR.hr_leave_response_employee', compact('user', 'leave', 'name'));
        } else {
            return view('HR.hr_not_show_teamleader_pending_leave');
        }
    	
    }

    public function hrGiveResponseLeaveToEmployee(Request $request)
    {
    	$this->validate($request, [
            'leave_response' => 'required',
            'comment' => 'required',
        ]);

         Leave::where('id', $request->leave_id)->update([
            'leave_response' => $request->leave_response,
            'comment' => $request->comment
        ]);
        $leave = Leave::where('id', $request->leave_id)->first();
        return redirect()->back()->with(['success' => 'Leave Response Submitted..', 'leave' => $leave]);
    }

    public function hrGiveResponseLeaveToTeamLeader(Request $request)
    {
        $this->validate($request, [
            'leave_response' => 'required',
            'comment' => 'required',
        ]);

        Leave::where('id', $request->leave_id)->update([
            'leave_response' => $request->leave_response,
            'comment' => $request->comment,
        ]);

        $leave = Leave::where('id', $request->leave_id)->first();
        return redirect()->back()->with(['success' => 'Leave Response Submitted..', 'leave' => $leave]);
    }

    public function showLeaveManagmentMenu()
    {
        $user = User::find(Auth::id());
        return view('HR.hr_leave_managment_menu', compact('user'));
    }

    public function hrShowEmployeePendingLeaveList()
    {
        $user = User::find(Auth::id());
        $leave_pending = Leave::select('leaves.*')
                        ->where('leave_response', '=', 'TL Approve Leave')
                        ->get();
        return view('HR.hr_show_employee_pending_leave_list', compact('user', 'leave_pending'));
    }

    public function hrShowTeamleaderPendingLeaveList()
    {
        $user = User::find(Auth::id());
        $pending_leave = Leave::select('leaves.*')
                            ->where(['leave_response' => 'Pending Leave',
                                    'team_leader_id' => '0'])
                                    ->get();
        return view('HR.hr_show_teamleader_pending_leave_list', compact('user', 'pending_leave'));
    }
}