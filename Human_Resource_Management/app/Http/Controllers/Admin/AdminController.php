<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Spatie\Permission\Models\Role;
use App\User;
use App\Holiday;
use DB;
use Auth;

class AdminController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function showAdminDashboard()
    {
        $user = User::find(Auth::id());
        return view('Admin.admin_dashboard', compact('user'));
    }

    public function adminManageEmployee()	
    {
    	$user = User::find(Auth::id());
    	return view('Admin.admin_manage_employee',compact('user'));
    }

    public function showProfile() //show admin own profile and edit it
    {
    	$user = User::find(Auth::id());
        $teamleader = User::find($user->handle_user_id);
        return view('Admin.admin_profile')->with(['user' => $user,'teamleader' => $teamleader]); 
    }

    public function viewManageEmployee(Request $request) //admin view data Employe 
    {
    	$user = User::find(Auth::id());
    	$users = User::whereHAS('roles', function($q) {
    		$q->where('name', 'Employee');
    	})->get();
        // $handle_by = User::find($user->handle_user_id);
        // // $handle_by = User::all();
        
        // dd($user->handle_user_id);
    	// $handle_by = User::where('handle_user_id', $user->handle_user_id);
    	// dd($handle_by);
    	// $employee = User::where('id', $user->id);
    	// $handle_by = User::where('handle_user_id', $user->assign_teamleader);
    	// dd($handle_by);
    	return view('Admin.admin_view_employee_profile', compact('user', 'employee', 'users', 'handle_by'));
    }

    public function viewAllEmployeeProfile(Request $request)	//View All Employee
    {
    	$users = User::orderBy('id','DESC')->paginate(5);
        return view('Admin.admin_view_all_employee_profile',compact('users'))
            ->with('i', ($request->input('page', 1) - 1) * 5);
    }

    public function assignTeamLeaderToEmployee($id) //assign teamleader to employee
    {	
    	$user = User::find($id);
    	$assignable = User::whereHAS('roles', function($q) {
    		$q->where('name', 'TeamLeader');
    	})->get();

    	foreach ($assignable as $value) {
    		$assignable_users[$value->id] = $value->first_name;
    	}
        $handle_by = User::find($user->handle_user_id);
    	return view('Admin.admin_assign_teamleader', compact('user', 'assignable', 'assignable_users', 'handle_by'));
    }

    public function assignTeamLeader(Request $request) //assign teamLeader
    {
    	$this->validate($request,[
            'assign_teamleader' => 'required',
        ]);
    	$user = User::where('id', $request->user_id)
    		->update(['handle_user_id' => $request->assign_teamleader]);
    	return redirect()->back()->with(['success' => 'Assign TeamLeader To Employee','user' => $user]);
    }

    public function adminManageHoliday() //show holiday manage menu 
    {
        $user = User::find(Auth::id());
        return view('Admin.admin_manage_holiday', compact('user'));
    }

    public function adminCreateHoliday() // create new holiday
    {   
        $user = User::find(Auth::id());
        return view('Admin.admin_create_holiday', compact('user'));
    }

    public function holidayStore(Request $request) // holiday create and store databse
    {
        $this->validate($request, [
            'holiday_date' => 'required|date_format:Y-m-d|after:today',
            'holiday_description' => 'required',
        ]);
        $input = $request->all();

        Holiday::create($input);
        return redirect()->back()->with('success', 'Holiday Created Successfully');
    }

    public function adminShowHolidaySchedule() //show holiday schedule
    {
        $user = User::find(Auth::id());
        $holidays = Holiday::all();
        return view('Admin.admin_show_holiday_schedule', compact('user', 'holidays'));
    }

    public function editHoliday($id)
    {
        $user = User::find(Auth::id());
        $holidays = Holiday::find($id);
        $holiday_date = date('Y-m-d', strtotime($holidays->holiday_date)); 
        return view('Admin.admin_edit_holiday', compact('user', 'holidays', 'holiday_date'));
    }

    public function holidayUpdate(Request $request, $id)
    {
        $this->validate($request,[
            'holiday_date' => 'required|date_format:Y-m-d|after:today',
            'holiday_description' => 'required',
        ]);

        $user = User::find(Auth::id());
        $input = $request->all();

        Holiday::find($id)->update($input);
        return redirect('/admin/show/holiday/schedule')->with(['success' => 'Holiday Update Succesfully..','user' => $user]);
    }

    public function deleteHoliday(Request $request, $id)
    {
        $user = User::find(Auth::id());
        Holiday::find($id)->delete();
        return redirect('/admin/show/holiday/schedule')->with(['success' => 'Holiday Delete Succesfully..','user' => $user]);
    }

    public function adminShowRemainingLeave()
    {
        $user = User::find(Auth::id());
        return view('Admin.admin_show_remailng_leave_view', compact('user'));
    }
}