<?php

namespace App\Http\Controllers\Teamleader;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Leave;
use Auth;
use DB;
use Mail;

class TeamLeaderController extends Controller
{
	/**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function showTeamleaderDashboard() //show Teamleader dashboard menu
    {
    	$user = User::find(Auth::id());
    	return view('Teamleader.teamleader_dashboard', compact('user'));
    }

    public function teamleaderManageEmployee() //show my employee menu
    {
    	$user = User::find(Auth::id());
    	return view('Teamleader.teamleader_manage_employee',compact('user'));
    }

    public function showTeamleaderPendingLeave() //show teamleader own pending leave list
    {
        $user = User::find(Auth::id());
        $leave_pending = DB::table('leaves')
                        ->select('leaves.*')
                        ->where(['leave_response' => 'Pending Leave', 'sender_id' => $user->id])
                        ->get();
        return view('Teamleader.teamleader_show_pending_leave_list', compact('user', 'leave_pending'));
    }

    public function teamleaderShowPendingLeaveAllData($id) //show particular pending leave
    {
        $user = User::find(Auth::id());
        $leave = Leave::find($id);

        if($leave->sender_id == Auth::user()->id) {
            return view('Teamleader.teamleader_show_pending_leave_data', compact('user', 'leave'));
        } else {
            return view('Teamleader.teamleader_not_show_other_user_pending_data');
        }
        
    }

    public function showResponseLeave() //teamleader show leave response reject or approve
    {
        $user = User::find(Auth::id());
        $leave_approve = Leave::select('leaves.*')
                        ->where('sender_id', '=', $user->id )
                        ->whereIn('leave_response',['HR Approve Leave', 'HR Reject Leave'])
                        ->get();
        return view('Teamleader.teamleader_show_approve_leave_list', compact('user', 'leave_approve'));
    }

    public function showMyLeaveResponse($id)
    {
        $user = User::find(Auth::id());
        $leave = Leave::find($id);

        if($leave->sender_id == Auth::user()->id) {
            return view('Teamleader.teamleader_show_leave_response_data', compact('user', 'leave'));
        } else {
            return view('Teamleader.teamleader_not_show_leave_response');
        } 
    }

    public function teamleaderShowEmployeeList() // show employee data which are he's under
    {
    	$user = User::find(Auth::id());
    	$users = DB::table('users')
    			->where('handle_user_id', $user->id)
    			->get();
    	return view('Teamleader.teamleader_show_employee_list',compact('user', 'users'));
    }

    public function teamleaderShowEmployeeLeaveList() // show leave list to teamleader which are created his employee
    {
        $user = User::find(Auth::id());
        $my_employee = DB::table('leaves')
                ->where('team_leader_id', Auth::user()->id)
                ->select('leaves.*', 'users.*')
                ->join('users', 'leaves.sender_id', '=', 'users.id')
                ->get();
        return view('Teamleader.teamleader_show_employee_Leave_List',compact('user', 'my_employee'));
    }

    public function giveLeaveResponseToEmployee($id) //give response to employee who are send leave by mail 
    {
        $leave = Leave::find($id);
        $user = User::find(Auth::id());
        $sender_name = DB::table('leaves')
                    ->where(['leaves.id' => $leave->id,])
                    ->select('leaves.*', 'users.first_name', 'users.email')
                    ->join('users', 'leaves.sender_id', '=', 'users.id')
                    ->first();
        if($leave->team_leader_id == Auth::user()->id) {
            return view('Teamleader.teamleader_leave_response', compact('user', 'leave', 'sender_name'));
        } else {
            return view('Teamleader.teamleader_not_access_leave');
        } 
    }

    public function showgiveLeaveResponseToEmployee($id)
    {
        $leave = Leave::find($id);
        $user = User::find(Auth::id());
        $sender_name = DB::table('leaves')
                    ->select('leaves.*', 'users.first_name', 'users.email')
                    ->join('users', 'leaves.sender_id', '=', 'users.id')
                    ->first();
        // dd($leave);
        return view('Teamleader.show_teamleader_leave_response', compact('user', 'leave', 'sender_name'));   
    }

    public function teamleaderGiveResponseLeaveToEmployee(Request $request) //give the response to employee leave apporove or reject
    {
        $this->validate($request, [
            'leave_response' => 'required',
            'comment' => 'required',
        ]);

        Leave::where('id', $request->leave_id)->update([
            'leave_response' => $request->leave_response,
            'comment' => $request->comment
        ]);
        $leave = Leave::where('id', $request->leave_id)->first()->toArray();
        $user = User::where('id', \Auth::id())->first();
        $team_leader = User::where('id', $user['handle_user_id'])->first();

        Mail::send('email_send_HR', $leave, function($message) use ($leave, $user, $team_leader) {
                    $message->from('mmanishaprajapati@gmail.com');
                    $message->to('manishaprajapati1965@gmail.com')->subject('Leave Apply Request');
                }); 
        return redirect()->back()->with(['success' => 'Leave Response Submitted..', 'leave' => $leave, 'user' => $user, 'team_leader' => $team_leader]);
    }

    public function teamleaderLeaveApply()
    {
        $user = User::find(Auth::id());
        $leave_check = Leave::find($user->halfday);
        return view('Teamleader.teamleader_leave_form', compact('user', 'leave_check'));
    }

    public function teamleaderStoreLeave(Request $request)
    {
        if (!$request->absence_date) {
            $request->absence_date = $request->start_leave;
        }

        if($request->halfday == NULL) {
            $request['absence_date'] = $request->start_leave;
            $request['leave_time'] = 'FullDay';
        } else {
            $request['start_leave'] = $request->absence_date;
            $request['end_leave'] = $request->absence_date;
        }
        $this->validate($request, [
            'start_leave' => 'required|date_format:Y-m-d|after:yesterday|after:today',
            'end_leave' => 'required|date_format:Y-m-d|after:today',
            'absence_date' => 'required|date_format:Y-m-d|after:yesterday',
            'leave_time' => 'required',
            'reason' => 'required',
            'address' => 'required',
            'phone_number' => 'required|numeric|digits:10',
        ]);

        $leave = $request->all();
        $leave['sender_id'] = Auth::user()->id;
        $new_leave = Leave::create($leave);

        $user = User::where('id', \Auth::id())->first();
        $leave = Leave::where('id', $new_leave->id)->first()->toArray();

        Mail::send('teamleader_email_send_HR', $leave, function($message) use ($leave, $user) {
                    $message->from('mmanishaprajapati@gmail.com');
                    $message->to('manishaprajapati1965@gmail.com')->subject('Leave Apply Request');
                }); 
        return redirect()->back()->with('success', 'Your Leave Request Send Successfully',compact('leave_check'));
    }

    public function showPendingLeave()
    {
        $user = User::find(Auth::id());
        $leave_pending = Leave::select('leaves.*')
                ->where(['leave_response' => 'Pending Leave', 'team_leader_id' => $user->id])
                ->get();
        return view('Teamleader.teamleader_show_pending_leave', compact('user', 'leave_pending'));
    }
}
