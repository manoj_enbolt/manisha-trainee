<!DOCTYPE html>
<html lang="en">
<head>
   <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('plugins/images/logoss.png') }}">
    <title>Support System</title>

    <link href="{{ asset('https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('Theme/bootstrap/dist/css/bootstrap.min.css') }}" rel="stylesheet">

    <link href="{{ asset('Theme/plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.css') }}" rel="stylesheet">

    <link href="{{ asset('plugins/bower_components/toast-master/css/jquery.toast.css') }}" rel="stylesheet">

    <link href="{{ asset('plugins/bower_components/chartist-js/dist/chartist.min.css') }}" rel="stylesheet">

    <link href="{{ asset('Theme/css/animate.css') }}" rel="stylesheet">

    <link href="{{ asset('Theme/css/style.css') }}" rel="stylesheet">

    <link href="{{ asset('Theme/css/colors/blue-dark.css') }}" id="theme" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
</head>
<body class="fix-header">
     <div id="wrapper">
        <nav class="navbar navbar-default navbar-static-top m-b-0">
            <div class="navbar-header"><!--header start-->
                <div class="top-left-part">
                    <a class="logo" href="index.html">
                        <b>
                            <img src="{{ asset('plugins/images/admin-logo.png')}}" alt="home" class="dark-logo" />
                            <img src="{{ asset('plugins/images/admin-logo-dark.png')}}" alt="home" class="light-logo" />
                        </b>
                        <span class="hidden-xs">
                            <i><b><font style="color: white;">Support System</font></b></i>
                        </span>
                    </a>
                </div>
               <div class="nav navbar-top-links navbar-right pull-right">
                    <a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                            <button class="btn btn-inverse waves-effect waves-light m-r-10" style="margin-top: 10px;">
                                <i><b><font style="color: white;">Logout</font></b></i>
                            </button>
                    </a>                           
                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">@csrf</form>
                </div>
            </div><!--header end-->
        </nav>
        <div class="navbar-default sidebar" role="navigation"><!--sidebar start-->
            <div class="sidebar-nav slimscrollsidebar">
                <div class="sidebar-head">
                    <h3>
                        <span class="fa-fw open-close">
                            <i class="ti-menu hidden-xs"></i>
                            <i class="ti-close visible-xs"></i>
                        </span>
                        <span class="hide-menu">Support System</span>
                    </h3>
                </div>
                <ul class="nav" id="side-menu">
                    <li class="user-pro">
                        <a href="#" class="waves-effect">
                            <img src="{{ asset('plugins/images/logoss.png') }}" style="height: 100px; width: 100px; margin-left: 50px;">
                            <span style="color: white; margin-left: 50px;"><b>Support System</b></span>
                        </a>
                    </li>
                    <li>
                        <a href="{{ url('admin/display-support-data-user') }}" class="waves-effect">
                            <i class="fa fa-clock-o fa-fw" aria-hidden="true"></i>Support
                        </a>
                    </li>
                    <li>
                        <a href="{{ url('/admin') }}" class="waves-effect">
                            <i class="fa fa-user fa-fw" aria-hidden="true"></i>Profile</a>
                    </li>
                    <li>
                        <a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                    <i class="fa fa-power-off" style="margin-left: 10px;"></i><span> <B> LOGOUT</B></span>
                            </a>
                    </li>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">@csrf</form>
                        <li class="devider"></li>
                </ul> 
            </div>
        </div><!--sidebar end-->
        
        @yield('content')

        <footer class="footer text-center" style="background-color: #B6B6B6;">
            2017 &copy; Ample Admin brought to you by themedesigner.in
        </footer>
    </div>

    <script src="{{ asset('https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js') }}"></script>

    <script src="{{ asset('https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/jquery/dist/jquery.min.js') }}"></script>

    <script src="{{ asset('Theme/bootstrap/dist/js/bootstrap.min.js') }}"></script>

    <script src="{{ asset('plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.js') }}"></script>
</body>
</html>