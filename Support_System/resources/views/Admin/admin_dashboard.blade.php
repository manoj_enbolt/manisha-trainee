@extends('Admin.admin_header')
@section('content')
    <div id="page-wrapper"><!--Main Body Start-->
	    <div class="container-fluid"><!-- container-fluid start -->
	        <div class="row bg-title">
	            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
	                <h4 class="page-title">Admin Dashboard</h4>
	            </div>
	        </div>
	       <div class="row" style="padding-left: 100px; padding-top: 30px;">
				<div class="col-md-6 col-sm-12 col-lg-4">
		        	<div class="panel" style="padding: 20px; width: 500px; height: 450px;">
		            	<div class="p-30">
		                	<div class="row">
			                    <div class="col-xs-4 col-sm-4">
			                    	<a href="javascript:void(0)">
			                    		<img src="{{ asset('plugins/images/users/varun.jpg') }}" alt="varun" class="img-circle img-responsive">
			                    	</a>
			                    	<h2 class="m-b-0" style="padding-left: 20px;font-style: italic; ">{{$user->name }}</h2>
			                    </div>
			                    <div class="col-xs-12 col-sm-8" style="padding-top: 50px; padding-left: 50px;">
			                        <small>E-Mail Address:<h4>{{$user->email}}</h4></small><br>
			                        {{-- <a class="btn btn-info  btn-md m-r-5" style="padding-left: 10px;">
										<i class="glyphicon glyphicon-pencil"></i>&nbsp CHANGE PROFILE
									</a> --}}
			                    </div>
		                	</div><br>
			            	<div class="row text-center m-t-30">
			                    <div class="col-xs-4 b-r">
			                        <h3>14</h3>
			                        <h5>POSTS</h5>
			                    </div>
			                    <div class="col-xs-4 b-r">
			                        <h3>54</h3>
			                        <h5>VIEWS</h5>
			                    </div>
			                    <div class="col-xs-4">
			                        <h3>145</h3>
			                        <h5>TASKS</h5>
			                    </div>
			           	 	</div>
			           	 	{{-- <div class="col-xs-12 col-sm-8" style="padding-top: 20px; padding-left: 50px;">
			           	 		<a class="btn btn-danger  btn-md m-r-5">
									<i class="glyphicon glyphicon-plus"></i>&nbsp ADD NEW DATA
								</a>
			               	</div> --}}
		            	</div>
		        	</div>
    			</div>
   			</div>
	 	</div><!-- container-fluid end -->
    </div><!--Main Body end-->
@endsection