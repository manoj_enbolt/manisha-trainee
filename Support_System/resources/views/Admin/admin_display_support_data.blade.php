@extends('Admin.admin_header')
@section('content')
	<div id="page-wrapper"><!--Main Body Start-->
        <div class="container-fluid"><!-- container-fluid start -->
            <div class="row bg-title">
                <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                    <h3 class="page-title"><b>Dispaly The all Support Request</b></h3>
                </div>
            </div>
             @if(Session::has('success'))
                <div class="alert alert-success" role="alert">
                    {!! Session::get('success') !!}
                </div>
            @endif
			<div class="row">
				<div class="col-sm-12">
					<div class="table-responsive">
						<table class="table table-hover manage-u-table">
							<thead>
								<tr style="font-size: 15px;">
									<th style="width: 100px;" class="text-center">No</th>
									<th class="text-center">Subject</th>
									<th class="text-center">Description</th>
									<th class="text-center">Sender Name</th>
									<th class="text-center">Assign Task</th>
									<th class="text-center">Status</th>
								</tr>
							</thead>
							<tbody>
								<?php $no=1; ?>
								@foreach ($supports as $support)
									<tr>
										<td class="text-center"><span class="font-muted">{{ $no++ }}</span></td>
										<td class="text-center"><span class="font-muted">{{ $support->subject }}</span></td>
										<td class="text-center"><span class="font-muted">{{ $support->description }}</span></td>
										<td class="text-center"><span class="font-muted">{{ $support->support_name }}</span></td>
										@if($support->handle_user_id == 'NULL')
											<td class="text-center"><span class="font-muted">
												Not assign
											</span></td>
											@else
												<td class="text-center"><span class="font-muted">{{ $support->handle_user }}</span></td>
										@endif
										<td class="text-center"><span class="font-muted">{{ $support->status }}</span></td>
										<td><a class="btn btn-info btn-md m-r-5" href="{{url('/admin/show-support-data',$support->id)}}" style="margin-left: 50px; border-radius: 50%;">SHOW</a></td>
									</tr>
								@endforeach
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div><!-- container-fluid end -->
    </div><!--Main Body end-->
@endsection