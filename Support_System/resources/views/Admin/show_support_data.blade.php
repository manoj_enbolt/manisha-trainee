@extends('Admin.admin_header')
@section('content')
	<div id="page-wrapper"><!--Main Body Start-->
        <div class="container-fluid"><!-- container-fluid start -->
            <div class="panel panel-default" style="margin: 30px;">
                <div class="panel-body">
                    <div class="content">
                        <h2 class="header">{{ $support->subject }}</h2>
                        @if(Session::has('success'))
                            <div class="alert alert-success" role="alert">
                                {!! Session::get('success') !!}
                            </div>
                        @endif
                        <div class="panel well well-sm">
                            <div class="panel-body">
                                <div class="col-md-12">
                                    <div class="col-md-9" id="support-data">
                                        <h3><strong>Create By</strong>:
                                            <span style="color: blue">{{ $user->name }}</span>
                                        </h3>
                                        <p>
                                            <strong>Created On</strong>:
                                            <span style="color: green">
                                                {{ date('F d, Y', strtotime($support->created_at)) }}
                                            </span>
                                        </p>
                                        <p>
                                            <strong>Last modified</strong>:
                                            <span style="color: #e10000">
                                                {{ date('F d, Y', strtotime($support->updated_at)) }}
                                            </span>
                                        </p>
                                        <p>
                                            <strong>Description</strong>:
                                            <span>
                                                {{ $support->description }}
                                            </span>
                                        </p>
                    
                                        <p>
                                            <strong>Status</strong>:
                                            <span>{{ $support->status }}</span>
                                        </p>
                                        <div class="row">
                                            @if($support->status == 'Pending')
                                                <div class="col-lg-3">
                                                     <p>
                                                        <strong>Task Assign To:</strong>:
                                                    </p>
                                                </div>
                                                <div class="col-lg-6" id ="status-change">
                                                    {!! Form::open(['url' => '/task-assign','id' => 'support-form-comment','method' => 'post']) !!}
                                                    {{method_field('PATCH')}}
                                                    {{-- {{ Form::hidden('_method', 'PATCH' ) }} --}}
                                                    {{ Form::hidden('support_id', $support->id ) }}
                                                    {{ Form::select('task_assign',$assignable_users,null,['placeholder' => '-- Select User --']) }}&nbsp&nbsp&nbsp
                                                    {{ Form::submit('Submit!', array('id' => 'submit')) }}
                                                    {!! Form::close() !!}
                                                </div>
                                            @else
                                                 <p>
                                                    <strong style="padding-left: 12px;">Task Assign To:</strong>:
                                                    <span>{{ $handled_user->name }}</span>
                                                </p>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <p>
                                            <strong>Attachment</strong>:
                                            <span>
                                                <img style="height: 100px;width: 150px; padding-left: 70px;" alt="{{ $support->image }}" src="{{ asset('/storage/upload').'/'.$support->image }}">
                                            </span>
                                        </p>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    @if($support->status == 'Close')
                                    @else
                                    {!! Form::open(['url' => '/admin/support-form/comment/store','id' => 'support-form-comment','method' => 'post']) !!}
                                        <div class="panel panel-default">
                                            <div class="panel-body">
                                                {{ Form::hidden('support_id', $support->id ) }}
                                                <fieldset>
                                                    <legend>Reply</legend>
                                                    <div class="form-group">
                                                        <div class="col-lg-12" {{ $errors->has('comment') ? ' is-invalid' : '' }}">
                                                            {!! Form::textarea('comment',NULL, ['class' => 'form-control summernote-editor','rows' => 3, 'cols' => 50,'id' => 'comment','placeholder' => 'Give Reply..']) !!}
                                                            {!! $errors->first('comment','<span class="help-block" style="color:red;">:message</span>') !!}
                                                        </div>
                                                    </div><br>
                                                    <div class="text-right col-md-12">
                                                        {!! Form::button('SUBMIT' ,  ['class' => 'btn btn-primary', 'type' =>'submit'])!!}
                                                    </div>
                                                </fieldset>
                                            </div>
                                        </div>  
                                    {!! Form::close() !!}
                                    @endif
                                
                                    <div class="commentss" id="comments">
                                        <h4><b>Show All Comments..</b></h4><hr>
                                        @if($comments->count() > 0)
                                            @foreach ($comments as $comment)
                                                <ul class="chat-list slimscroll" style="overflow: hidden;" tabindex="5005">
                                                    @if(\Auth::user()->id == $comment->sender_id)
                                                        <li class="odd">
                                                            <div class="chat-image">
                                                                <img alt="male" src="{{ asset('plugins/images/users/varun.jpg') }}">
                                                            </div>
                                                            <div class="chat-body">
                                                                <div class="chat-text">
                                                                    <h4>Me</h4>
                                                                    <p>{{ $comment->comment }}</p><br>
                                                                </div>
                                                            </div>
                                                        </li> 
                                                        @else
                                                           <li>
                                                                <div class="chat-image">
                                                                    <img alt="Female" src="{{ asset('plugins/images/users/hritik.jpg') }}">
                                                                </div>
                                                                <div class="chat-body">
                                                                    <div class="chat-text">
                                                                        <h4>{{ $comment->name }}</h4>
                                                                        <p>{{ $comment->comment }}</p><br>
                                                                    </div>
                                                                </div>
                                                            </li> 
                                                    @endif
                                                </ul>
                                            @endforeach
                                            @else
                                                Not have any Comment
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- container-fluid end -->
    </div><!--Main Body end-->
@endsection