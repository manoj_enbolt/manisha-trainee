@extends('head')
@section('welcome')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">
                        <h3><b>Login</b></h3><hr><br>
                    </div>
                    @if(Session::has('success'))
                        <div class="alert alert-success alert-dismissible" role="alert">
                            {!! Session::get('success') !!}
                        </div>
                    @endif
                    <div class="card-body">
                        {!! Form::open() !!}
                            <div class="form-group row" {{ $errors->has('email') ? ' is-invalid' : '' }}">
                                <label for="email" class="col-md-4 col-form-label text-md-right">E-Mail Address</label>

                                <div class="col-md-6">
                                    {!! Form::text('email',NULL, ['class' => 'form-control form-rounded','placeholder' => 'E-Mail Adress']) !!}
                                    {!! $errors->first('email','<span class="help-block" style="color:red;">:message</span>') !!}
                                </div>
                            </div>


                            <div class="form-group row" {{ $errors->has('password') ? ' is-invalid' : '' }}">
                                <label for="password" class="col-md-4 col-form-label text-md-right">Password</label>

                                <div class="col-md-6">
                                    {!! Form::password('password',['class' => 'form-control form-rounded','placeholder' => 'Password']) !!}
                                    {!! $errors->first('password','<span class="help-block" style="color:red;">:message</span>') !!}
                                </div>
                            </div>

                            <div class="form-group row mb-0">
                                <div class="col-md-8 offset-md-4">
                                    {!! Form::button('Login', ['class' => 'btn btn-primary', 'type' =>'submit'])!!}
                                    <a class="btn btn-link" href="{{ route('password.request') }}">
                                        Forgot Your Password..?
                                    </a>
                                </div>
                            </div>

                            <div class="form-group row mb-0">
                                <div class="col-md-8 offset-md-4">
                                    <p>Don't have an account?
                                        <a class="btn btn-link" href="{{ route('register') }}">
                                            REGISTRATION...
                                        </a>
                                    </p>
                                </div>
                            </div>

                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection