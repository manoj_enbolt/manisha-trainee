@extends('User.user_header')
@section('content')
	<div class="container" style="padding-left: 100px; padding-top: 30px;  height: 685px;">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">
                        <h3><b>Support Form</b></h3><hr><br>
                    </div>
                    @if(Session::has('success'))
                    	<div class="alert alert-success alert-dismissible" role="alert">
							{!! Session::get('success') !!}
						</div>
					@endif
                    <div class="card-body">
                        {!! Form::open(['url' => '/user/support-form/store','id' => 'support-form','method' => 'post', 'files' => true ]) !!}

                            <div class="form-group row" {{ $errors->has('subject') ? ' is-invalid' : '' }}">
                                <label for="subject" class="col-md-4 col-form-label text-md-right">Subject</label>
                                <div class="col-md-6">
                                    {!! Form::text('subject',NULL, ['class' => 'form-control form-rounded','placeholder' => 'Subject']) !!}
                                    {!! $errors->first('subject','<span class="help-block" style="color:red;">:message</span>') !!}
                                </div>
                            </div>

                            <div class="form-group row" {{ $errors->has('description') ? ' is-invalid' : '' }}">
                                <label for="description" class="col-md-4 col-form-label text-md-right">Description</label>
                                <div class="col-md-6">
                                    {!! Form::textarea('description',NULL, ['rows' => 3, 'cols' => 54,'id' => 'description','class' => 'form-control form-rounded','placeholder' => 'Description']) !!}
                                    {!! $errors->first('description','<span class="help-block" style="color:red;">:message</span>') !!}
                                </div>
                            </div>

                             <div class="form-group row" {{ $errors->has('image') ? ' is-invalid' : '' }}">
                                <label for="image" class="col-md-4 col-form-label text-md-right">Choosen File</label>
                                <div class="col-md-6">
                                    {!! Form::file('image') !!} 
                                    {!! $errors->first('image','<span class="help-block" style="color:red;">:message</span>') !!}
                                </div>
                            </div>

                            {{-- {{ Form::hidden('sender_id', $user->id ) }} --}}

                            <div class="form-group row mb-0" style="padding-left: 50px;">
                                <div class="col-md-8 offset-md-4">
                                    {!! Form::button('SUBMIT' ,  ['class' => 'btn btn-primary', 'type' =>'submit'])!!}&nbsp
                                    {!! Form::button('Cancel', ['class' => 'btn btn-default', 'type' =>'submit'])!!}
                                </div>
                            </div>

                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection