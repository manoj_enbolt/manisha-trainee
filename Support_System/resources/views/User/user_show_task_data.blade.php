@extends('User.user_header')
@section('content')
    <div><!--Main Body Start-->
        <div><!-- container-fluid start -->
            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="content">
                        <h2 class="header">{{ $support->subject }}</h2>
                        @if(Session::has('success'))
                            <div class="alert alert-success" role="alert">
                                {!! Session::get('success') !!}
                            </div>
                        @endif
                        <div class="panel well well-sm">
                            <div class="panel-body">
                                <div class="col-md-12">
                                    <div class="col-md-9">
                                        <h3><strong>Create By</strong>:
                                            <span style="color: blue">{{ $send_user->name }}</span>
                                        </h3>
                                        <p>
                                            <strong>Created On</strong>:
                                            <span style="color: green">
                                                {{ date('F d, Y', strtotime($support->created_at)) }}
                                            </span>
                                        </p>
                                        <p>
                                            <strong>Last modified</strong>:
                                            <span style="color: #e10000">
                                                {{ date('F d, Y', strtotime($support->updated_at)) }}
                                            </span>
                                        </p>
                                        <p>
                                            <strong>Description</strong>:
                                            <span>
                                                {{ $support->description }}
                                            </span>
                                        </p>
                                        <p>
                                            <strong>Status</strong>:
                                            <span>{{ $support->status }}</span>
                                        </p>   
                                        <p>
                                            <strong>Attachment</strong>:
                                            <span>
                                                <img style="height: 100px;width: 150px; padding-left: 70px;" alt="{{ $support->image }}" src="{{ asset('/storage/upload').'/'.$support->image }}">
                                            </span>
                                        </p>
                                    </div>
                                </div>
                            </div><br>

                            <div class="commentss" id="comments">
                                <h4><b>Show All Comments..</b></h4><hr>
                                @if($comments->count() > 0)
                                    @foreach ($comments as $comment)
                                        <ul class="chat-list slimscroll" style="overflow: hidden;" tabindex="5005">
                                            @if(\Auth::user()->id == $comment->sender_id)
                                            <li class="odd">
                                                    <div class="chat-image">
                                                        <img alt="Female" src="{{ asset('plugins/images/users/hritik.jpg') }}" style="height: 50px; width: 50px; border-radius: 50">
                                                    </div>
                                                    <div class="chat-body">
                                                        <div class="chat-text">
                                                            <h4>Me</h4>
                                                            <p>{{ $comment->comment }}</p><br>
                                                        </div>
                                                    </div>
                                                </li> 
                                                
                                            @else
                                               <li>
                                                    <div class="chat-image">
                                                        <img alt="male" src="{{ asset('plugins/images/users/varun.jpg') }}" style="height: 50px; width: 50px;">
                                                    </div>
                                                    <div class="chat-body">
                                                        <div class="chat-text">
                                                            <h4>{{ $comment->name }}</h4>
                                                            <p>{{ $comment->comment }}</p><br>
                                                        </div>
                                                    </div>
                                                </li> 
                                            @endif
                                        </ul>
                                    @endforeach
                                    @else
                                        Not have any Comment
                                @endif
                            </div>

                            <div class="col-md-12">
                                @if($support->status == 'Close')
                                @else
                                {!! Form::open(['url' => '/user/support-form/tickits/store','id' => 'support-form-comment','method' => 'post']) !!}
                                    {{ Form::hidden('support_id', $support->id ) }}
                                    <h4>Reply</h4><br>
                                    <div class="form-group" {{ $errors->has('comment') ? ' is-invalid' : '' }}">
                                        {!! Form::textarea('comment',NULL, ['class' => 'form-control summernote-editor','rows' => 3, 'cols' => 50,'id' => 'comment','placeholder' => 'Give Reply..']) !!}
                                        {!! $errors->first('comment','<span class="help-block" style="color:red;">:message</span>') !!}
                                    </div>
                                    <div class="text-right col-md-12" style="padding-bottom: 30px;">
                                        {!! Form::button('SUBMIT' ,  ['class' => 'btn btn-primary', 'type' =>'submit'])!!}
                                    </div>
                                {!! Form::close() !!}
                                @endif
                            </div><br>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- container-fluid end -->
    </div><!--Main Body end-->
@endsection