@extends('User.user_header')
@section('content')
    <div class="row" style="  height: 685px;">
         @if(Session::has('success'))
            <div class="alert alert-success" role="alert">
                {!! Session::get('success') !!}
            </div>
        @endif
        <div class="col-md-12" >
            <div class="panel panel-default" style="margin:  50px;">
                <div class="panel-heading">
                <h2>My Task</h2>
                @if (session('errors'))
                    <div class="alert alert-danger" role="alert">
                        {{ session::get('errors') }}
                    </div>
                @endif
            </div>
            <div class="panel-body">
                <div id="message"></div>
                <table class="table table-condensed table-stripe ddt-responsive" class="ticketit-table">
                    <thead>
                        <tr>
                            <td>#</td>
                            <td>Subject</td>
                            <td>Description</td>
                            <td>Status</td>
                            <td>Created By</td>
                        </tr>
                    </thead>
                    <tbody>
                        @if($supports->count() > 0)
                        <?php $no=1; ?>
                            @foreach ($supports as $support)
                                <tr>
                                    <td><span class="font-muted">{{ $no++ }}</span></td>
                                    <td><span class="font-muted">
                                        <a  href="{{url('user/show-task',$support->id)}}">
                                            {{ $support->subject }}
                                        </a></span></td>
                                    <td><span class="font-muted">{{ $support->description }}</span></td>
                                    <td><span class="font-muted">{{ $support->status }}</span></td>
                                    <td><span class="font-muted">{{ $support->name }}</span></td>
                                </tr>
                            @endforeach
                            @else
                                <h4 style="color: blue;">No Any Task Assign You..</h4>
                        @endif
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection