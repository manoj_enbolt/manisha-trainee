@extends('User.user_header')
@section('content')
	<div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">
                        <h3><b>Change Profile</b></h3><hr><br>
                    </div>
                    <div class="card-body">
                        {!! Form::model($user,['route' => ['user.update',$user->id],'files' => true,'method' => 'PATCH']) !!}

                            <div class="form-group row" {{ $errors->has('name') ? ' is-invalid' : '' }}">
                                <label for="name" class="col-md-4 col-form-label text-md-right">UserName</label>
                                <div class="col-md-6">
                                    {!! Form::text('name',NULL, ['class' => 'form-control form-rounded','placeholder' => 'Name']) !!}
                                    {!! $errors->first('name','<span class="help-block" style="color:red;">:message</span>') !!}
                                </div>
                            </div>

                            <div class="form-group row" {{ $errors->has('email') ? ' is-invalid' : '' }}">
                                <label for="email" class="col-md-4 col-form-label text-md-right">E-Mail Adress</label>
                                <div class="col-md-6">
                                    {!! Form::text('email',NULL, ['class' => 'form-control form-rounded','placeholder' => 'E-Mail Adress']) !!}
                                    {!! $errors->first('email','<span class="help-block" style="color:red;">:message</span>') !!}
                                </div>
                            </div>

                            <div class="form-group row" {{ $errors->has('password') ? ' is-invalid' : '' }}">
                                <label for="password" class="col-md-4 col-form-label text-md-right">Password</label>
                                <div class="col-md-6">
                                    {!! Form::password('password',['class' => 'form-control form-rounded','placeholder' => 'Password']) !!}
                                    {!! $errors->first('password','<span class="help-block" style="color:red;">:message</span>') !!}
                                </div>
                            </div>

                            <div class="form-group row mb-0">
                                <div class="col-md-8 offset-md-4">
                                    {!! Form::button(isset($model)? 'Update' : 'SUBMIT' , ['class' => 'btn btn-primary', 'type' =>'submit'])!!}&nbsp
                                    {!! Form::button('Cancel', ['class' => 'btn btn-default', 'type' =>'submit'])!!}
                                </div>
                            </div>

                           	{!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection