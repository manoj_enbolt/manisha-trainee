@extends('User.user_header')
@section('content')
	<div class="row" style="padding-left: 100px; padding-top: 30px; background-color: #e7e7e7; height: 685px;">
		<div class="col-md-6 col-sm-12 col-lg-4">
        	<div class="panel" style="padding: 30px; width: 500px; height: 420px;">
            	<div class="p-30">
                	<div class="row">
	                    <div class="col-xs-4 col-sm-4">
	                    	<a href="javascript:void(0)">
	                    		<img src="{{ asset('plugins/images/users/hritik.jpg') }}" alt="varun" class="img-circle img-responsive">
	                    	</a>
	                    	<h2 class="m-b-0" style="padding-left: 20px;font-style: italic; ">{{$user->name }}</h2>
	                    </div><br>
	                    <div class="col-xs-12 col-sm-8" style="padding-top: 20px; padding-left: 40px;">
	                        <small>E-Mail Address:<h4>{{$user->email}}</h4></small><br>
	                        {{-- <a class="btn btn-info  btn-md m-r-5" style="padding-left: 10px;">
								<i class="glyphicon glyphicon-pencil"></i>&nbsp CHANGE PROFILE
							</a> --}}
	                    </div>
                	</div><br>
	            	<div class="row text-center m-t-30">
	                    <div class="col-xs-4 b-r">
	                        <h3>14</h3>
	                        <h5>POSTS</h5>
	                    </div>
	                    <div class="col-xs-4 b-r">
	                        <h3>54</h3>
	                        <h5>VIEWS</h5>
	                    </div>
	                    <div class="col-xs-4">
	                        <h3>145</h3>
	                        <h5>TASKS</h5>
	                    </div>
	           	 	</div>
	           	 	{{-- <div class="col-xs-12 col-sm-8" style="padding-top: 20px; padding-left: 50px;">
	           	 		<a class="btn btn-danger  btn-md m-r-5" href="{{url('/login')}}">
							<i class="glyphicon glyphicon-plus"></i>&nbsp ADD NEW DATA
						</a>
	               	</div> --}}
            	</div>
        	</div>
    	</div>
   	</div>
@endsection