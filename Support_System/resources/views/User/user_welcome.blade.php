<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('plugins/images/logoss.png') }}">
    <title>Support System</title>
    <link href="{{ asset('Theme/bootstrap/dist/css/bootstrap.min.css') }}" rel="stylesheet">

    <link href="{{ asset('plugins/bower_components/chartist-js/dist/chartist.min.css') }}" rel="stylesheet">

    <link href="{{ asset('Theme/css/style.css') }}" rel="stylesheet">

    <link rel="stylesheet" href="{{ asset('fonts/font-awesome/font-awesome.css') }}">
</head>
<body>
    <nav class="navbar navbar-default">
        <div class="container">
            <div>
                <a class="navbar-brand" href="http://demo.procoderr.tech">
                    Support System
                </a>
            </div>
            <div class="collapse navbar-collapse" id="app-navbar-collapse">
                <ul class="nav navbar-nav navbar-right">
                    <li>
                        <a href="{{ url('/login') }}">Login</a>
                    </li>
                    <li>
                        <a href="{{ url('/register') }}">Register</a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
        
    <footer class="footer text-center" style="background-color: #B6B6B6;">
        <a href="http://demo.procoderr.tech" style="color: black;">
            Copyright &copy; Procoderr 2018 - All rights reserved
        </a>
    </footer>
</body>
</html>
