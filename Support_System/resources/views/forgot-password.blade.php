@extends('head')
@section('welcome')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">
                        <h3><b>Forgot Password</b></h3><hr><br>
                    </div>
                    @if(Session::has('success'))
                        <div class="alert alert-success alert-dismissible" role="alert">
                            {!! Session::get('success') !!}
                        </div>
                    @endif
                    <div class="card-body">
                        {!! Form::open(['url' => 'password/email','method' => 'post']) !!}
                            <div class="form-group row" {{ $errors->has('email') ? ' is-invalid' : '' }}">
                                <label for="email" class="col-md-4 col-form-label text-md-right">E-Mail Address</label>
                                <div class="col-md-6">
                                    {!! Form::text('email',NULL, ['class' => 'form-control form-rounded','placeholder' => 'E-Mail Adress']) !!}
                                    {!! $errors->first('email','<span class="help-block" style="color:red;">:message</span>') !!}
                                </div>
                            </div>

                            <div class="form-group row mb-0">
                                <div class="col-md-8 offset-md-4">
                                    {!! Form::button('Submit', ['class' => 'btn btn-primary', 'type' =>'submit'])!!}
                                </div>
                            </div>

                            <div class="form-group row mb-0">
                                <div class="col-md-8 offset-md-4">
                                    <p>Remember Password...?
                                        <a class="btn btn-link" href="{{ route('login') }}">
                                            Login...
                                        </a>
                                    </p>
                                </div>
                            </div>

                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
