@extends('head')
@section('welcome')
    <div class="container">
    	<div class="row justify-content-center">
        	<div class="col-md-8">
            	<div class="card">
	                <div class="card-header">
	                	<h3><b>Reset Password</b></h3><hr><br>
	                </div>
	                @if(Session::has('success'))
                        <div class="alert alert-success alert-dismissible" role="alert">
                            {!! Session::get('success') !!}
                        </div>
                    @endif
	            	<div class="card-body">
	                    {!! Form::open(['url' => 'password/reset','method' => 'post']) !!}
	                        @csrf
	                        <input type="hidden" name="token" value="{{ $token }}">

	                        <div class="form-group row" {{ $errors->has('email') ? ' is-invalid' : '' }}>
	                            <label for="email" class="col-md-4 col-form-label text-md-right">E-Mail Address</label>

	                            <div class="col-md-6">
	                                {!! Form::text('email',NULL, ['class' => 'form-control form-rounded','placeholder' => 'Email']) !!}
	                                {!! $errors->first('email','<span class="help-block" style="color:red;">:message</span>') !!}
	                    		</div>
	                        </div>

	                        <div class="form-group row" {{ $errors->has('password') ? ' is-invalid' : '' }}>
	                            <label for="password" class="col-md-4 col-form-label text-md-right">Password</label>

	                          	<div class="col-md-6">  
									{!! Form::password('password',['class' => 'form-control form-rounded','placeholder' => 'Password']) !!}
									{!! $errors->first('password','<span class="help-block" style="color:red;">:message</span>') !!}
								</div>
	                        </div>

	                        <div class="form-group row" {{ $errors->has('password_confirmation') ? ' is-invalid' : '' }}>
	                            <label for="password_confirmation" class="col-md-4 col-form-label text-md-right">Confirm Password</label>

	                            <div class="col-md-6">
	                            	{!! Form::password('password_confirmation',['class' => 'form-control form-rounded','placeholder' => 'Confirm Password']) !!}
	                            	{!! $errors->first('password_confirmation','<span class="help-block" style="color:red;">:message</span>') !!}
	                    		</div>
	                        </div>

	                        <div class="form-group row mb-0">
	                            <div class="col-md-6 offset-md-4">
	                                {!! Form::button('SUBMIT' , ['class' => 'btn btn-primary', 'type' =>'submit' ,'style' => 'width: 150px;'])!!}
	                            </div>
	                        </div>
	                	{!! Form::close() !!}
	            	</div>
            	</div>
        	</div>
    	</div>
	</div>
@endsection