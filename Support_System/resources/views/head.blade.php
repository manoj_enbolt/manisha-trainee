<!DOCTYPE html>  
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('plugins/images/logoss.png') }}">
    <title>Support System</title>

    <link href="{{ asset('Theme/bootstrap/dist/css/bootstrap.min.css') }}" rel="stylesheet">
    
    <link href="{{ asset('Theme/css/animate.css') }}" rel="stylesheet">
    
    <link href="{{ asset('Theme/css/style.css') }}" rel="stylesheet">
    
    <link href="{{ asset('Theme/css/colors/blue.css') }}" id="theme"  rel="stylesheet">

    <link href="{{ asset('css/html-layout1.css') }}" rel="stylesheet">
</head>
<body>
    <div class="main"><!--main div start-->
        <div class="logo">
            <div class="container">
                <h2><img src="{{ asset('plugins/images/logoss.png') }}">
                    <font style="color: blue;">Support</font> <font style="color: red;">System</font>
                </h2>
            </div>
        </div><br>
    
        <div class="row">
            <div class="col-md-6 col-md-offset-3">
                @if (session('success'))
                   <div class="alert alert-success" role="alert">
                        {{ session('success') }}
                   </div>
               @endif
                @yield('welcome')
            </div>
        </div>
    </div><!--main div end-->

    <script src="{{ asset('plugins/bower_components/jquery/dist/jquery.min.js') }}"></script>
   
    <script src="{{ asset('Theme/bootstrap/dist/js/bootstrap.min.js') }}"></script>
    
    <script src="{{ asset('plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.js') }}"></script>

    <script src="{{ asset('js/jquery.slimscroll.js') }}"></script>
    
    <script src="{{ asset('js/waves.js') }}"></script>
    
    <script src="{{ asset('js/custom.min.js') }}"></script>
    
    <script src="{{ asset('plugins/bower_components/styleswitcher/jQuery.style.switcher.js') }}"></script>
</body>
</html>