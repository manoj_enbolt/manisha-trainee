<?php

namespace App\Http\Controllers\Admin;

use App\User;
use App\Support;
use App\Comment;
use Auth;
use DB;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AdminController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function home() //View the Profile
    {
        $user = User::find(Auth::id());
        return view('Admin.admin_dashboard')->with(['user' => $user]);
    }

    public function displaySupportFormDataAllUser() //Display All User Support Data With UserName 
    {
        $supports = DB::table('supports')
                    ->select('supports.*', 'users.name as support_name', 'handle_table.name as handle_user')
                    ->join('users', 'supports.sender_id', '=', 'users.id')
                    ->leftjoin('users as handle_table', 'supports.handle_user_id', '=', 'handle_table.id')
                    ->get();
                    // dd($supports);
        return view('Admin.admin_display_support_data',compact('supports'));
    }

    public function showSupportData($id) //Show Support Data To Particular User with Support Id
    {
        $support = Support::find($id);
        $user = User::where('id', $support->sender_id)->first();
        $handled_user = User::where('id', $support->handle_user_id)->first();
        $assignable = User::where('assignBy', 1)->get();
        
        foreach ($assignable as $value) {
            $assignable_users[$value->id] = $value->name;
        }

        $comments =DB::table('comments')
                    ->where('comments.support_id' , $id)
                    ->join('users', 'comments.sender_id', '=', 'users.id')
                    ->select('comments.*','users.name')
                    ->get();
        return view('Admin.show_support_data',compact('support','comments','user', 'handled_user', 'assignable_users'));
    }

    public function commentStore(Request $request) //Store the Comment and  View It
    {
        $this->validate($request, [
            'comment' => 'required',  
        ]);
        $response = Comment::create([
            'comment' => $request['comment'],
            'support_id' => $request->support_id,
            'sender_id' => Auth::user()->id,
        ]);
        return redirect()->back()->with('success', 'Your Comment Successfully Send..');
    }

    public function statusChange(Request $request) //Change Status Pending Or Progress or Close 
    {
        Support::where('id' , $request->support_id ,$request->status)
                ->update(['status' => $request->status]);
        $support = Support::where('id' , $request->support_id)->first();
        return redirect()->back()->with(['success' => 'Status Update..', 'support' => $support]);
    }

    public function assignTaskToUser(Request $request) //Assign Task To User 
    {   
        Support::where('id' , $request->support_id)
                ->update(['status' => 'Progress', 'handle_user_id' => $request->task_assign]);
        $support = Support::where('id' , $request->support_id)->first();
        return redirect()->back()->with(['success' => 'Status Update..'.'<br>'.'Task Assign To User','support' => $support]);
    }
}