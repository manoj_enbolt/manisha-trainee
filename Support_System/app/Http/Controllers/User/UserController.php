<?php

namespace App\Http\Controllers\User;

use App\User;
use Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class UserController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function home() //View the Profile
    {
        $user = User::find(Auth::id());
        return view('User.user_dashboard')->with(['user' => $user]);
    }
}