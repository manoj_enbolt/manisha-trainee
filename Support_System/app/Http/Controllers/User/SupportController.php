<?php

namespace App\Http\Controllers\User;

use App\Support;
use App\User;
use App\Comment;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use DB;

class SupportController extends Controller
{
    public function createSupportForm() //View Support Form
    {
        return view('User.support_form');
    }

    public function store(Request $request) //Store Support Form Data On Table
    {
        $this->validate($request, [
            'subject' => 'required',
            'description' => 'required',
            'image' => 'required',
    	]);

         if($request->hasFile('image'))
        {
            $filename = $request->image->getClientOriginalName();
            $request->image->storeAs('/public/upload',$filename);
        }
        $support = $request->all();
        $support['image'] = $request->image->getClientOriginalName();
        $support['sender_id'] = Auth::user()->id;
        // dd($support);

        Support::create($support);
        return redirect('user/create-support-form')->with('success', 'Your Request Successfully Send..');
	}

    public function showTickits() //Show All Tickits Which login User Created
    {
        // $supports = Support::where('sender_id', Auth::user()->id)->get();
        $user = User::find(Auth::id());
        $supports = DB::table('supports')
                    ->where('sender_id', Auth::user()->id)
                    ->select('supports.*','users.name')
                    ->leftjoin('users', 'users.id', '=', 'supports.handle_user_id')
                    ->get();
        return view('User.user_show_all_tickits',compact('user','supports'));
    }

    public function showTickitstData($id) //Show Particular Tickit and It Comment
    {
        $support = Support::find($id);
        $user = User::find(Auth::id());
        $comments = DB::table('comments')
                    ->where('support_id', $id)
                    ->select('comments.*', 'users.name')
                    ->join('users', 'users.id', '=', 'comments.sender_id')
                    ->get();
        if ($support->sender_id == Auth::user()->id) {
            return view('User.user_show_tickits_data',compact('support','comments','user'));
        } else {
            return redirect()->back()->with('errors', 'You are Not Authorized this View...');   
        }
    }

    public function tickitsStore(Request $request) //Store Comment and Disply
    {
        $this->validate($request, [
            'comment' => 'required',  
        ]);
        $response = Comment::create([
            'comment' => $request['comment'],
            'support_id' => $request->support_id,
            'sender_id' => Auth::user()->id,
        ]);
        return redirect()->back()->with('success', 'Your Comment Successfully Send..');
    }

    public function showAssignMyTask() //View Support Form
    { 
        // $supports = Support::where('handle_user_id', Auth::user()->id)->get();
        $supports =DB::table('supports')
                    ->where('handle_user_id', Auth::user()->id)
                    ->select('supports.*', 'users.name')
                    ->join('users', 'supports.sender_id', '=', 'users.id')
                    ->get();
        return view('User.user_show_task',compact('supports'));
    }

    public function showTask($id) //Show Particular Tickit and It Comment
    {
        $support = Support::find($id);
        // dd($support);
        $comments = DB::table('comments')
                    ->where('support_id' , $id)
                    ->select('comments.*', 'users.name')
                    ->join('users', 'users.id', '=', 'comments.sender_id')
                    ->get();
        // dd($comments);
        $user = User::find(Auth::id());
        $send_user = User::where('id', $support->sender_id)->first();
        if ($support->handle_user_id == Auth::user()->id) {
            return view('User.user_show_task_data',compact('support','user','send_user', 'comments'));
        } else {
            return redirect()->back()->with('errors', 'You are Not Authorized this View...');   
        }
    }
}
