<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Support extends Model
{
	protected $table = 'supports';
    protected $fillable = ['subject', 'description', 'image', 'sender_id', 'handle_user_id', 'status'];
}
