<?php

	Auth::routes();
	Route::get('/', 'Auth\LoginController@showLoginForm')->name('login');
	Route::post('/', 'Auth\LoginController@login');

	Route::group(['middleware' => 'disablepreventback'], function() {

		Route::group(['middleware' => 'usermiddleware','prefix' => 'user'], function(){
			Route::get('/','User\UserController@home');
			Route::get('/create-support-form','User\SupportController@createSupportForm');
			Route::post('/support-form/store','User\SupportController@store');
			Route::get('/show-tickits','User\SupportController@showTickits');
			Route::get('/show-tickits-data/{id}','User\SupportController@showTickitstData');
			Route::post('/support-form/tickits/store','User\SupportController@tickitsStore');
			Route::get('/my-task','User\SupportController@showAssignMyTask');
			Route::get('/show-task/{id}','User\SupportController@showTask');
		});

		Route::group(['middleware' => 'admin','prefix' => 'admin'], function(){
			Route::get('/','Admin\AdminController@home');
			Route::get('/display-support-data-user','Admin\AdminController@displaySupportFormDataAllUser');
			Route::get('/show-support-data/{id}','Admin\AdminController@showSupportData');
			Route::post('/support-form/comment/store','Admin\AdminController@commentStore');
		});
		Route::patch('/status-change','Admin\AdminController@statusChange');
		Route::patch('/task-assign','Admin\AdminController@assignTaskToUser');
	});
?>