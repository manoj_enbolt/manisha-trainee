<!DOCTYPE html>  
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">
<link rel="icon" type="image/png" sizes="16x16" href="{{ asset('plugins/images/favicon.png') }}">
<title>Ample Admin Template - The Ultimate Multipurpose admin template</title>
<!-- Bootstrap Core CSS -->
<link href="{{ asset('Theme/bootstrap/dist/css/bootstrap.min.css') }}" rel="stylesheet">
<!-- animation CSS -->
<link href="{{ asset('Theme/css/animate.css') }}" rel="stylesheet">
<!-- Custom CSS -->
<link href="{{ asset('Theme/css/style.css') }}" rel="stylesheet">
<!-- color CSS -->
<link href="{{ asset('Theme/css/colors/blue.css') }}" id="theme"  rel="stylesheet">
</head>
<body>
<!-- Preloader -->
<div class="preloader">
  <div class="cssload-speeding-wheel"></div>
</div>
<section id="wrapper" class="login-register">
  <div class="login-box login-sidebar">
    <div class="white-box">
      {!! Form::open(['url' => 'password/reset','method' => 'post']) !!}
      
      @if (session('status'))
        <strong><div class="alert alert-danger" role="alert">{{ session('status') }}</div></strong>
      @endif
      @if ($errors->has('token'))
        <strong><ul>{!! $errors->first('token','<li style="color:red">:message</li>') !!}</ul></strong>
      @endif
        <a href="javascript:void(0)" class="text-center db"><img src="{{ asset('plugins/images/admin-logo-dark.png') }}" alt="Home" /><br/><img src="{{ asset('plugins/images/admin-text-dark.png') }}" alt="Home" /></a><br><br>
        <h3 class="box-title m-b-0"><center>Recover Password</center></h3>
        <input type="hidden" name="token" value="{{$token}}">
        <div class="form-group ">
          <div class="col-xs-12">
            {!! Form::text('email','', ['class' => 'form-control','placeholder' => 'Email']) !!}<br>
            @if ($errors->has('email'))
              <strong><ul>{!! $errors->first('email','<li style="color:red">:message</li>') !!}</ul></strong>
            @endif
          </div>
        </div>
        <div class="form-group ">
          <div class="col-xs-12">
            {!! Form::password('password', ['class' => 'form-control','placeholder' => 'Password']) !!}<br>
            @if ($errors->has('password'))
              <ul>{!! $errors->first('password','<li style="color:red">:message</li>') !!}</ul>
            @endif
          </div>
        </div>
        <div class="form-group ">
          <div class="col-xs-12">
            {!! Form::password('password_confirmation', ['class' => 'form-control','placeholder' => 'Confirm Password']) !!}<br>
            @if ($errors->has('password_confirmation'))
              <ul>{!! $errors->first('password_confirmation','<li style="color:red">:message</li>') !!}</ul>
            @endif
          </div>
        </div>
        <div class="form-group text-center m-t-20">
          <div class="col-xs-12">
            {!! Form::submit('Reset Password ',['class' => 'btn btn-info btn-lg btn-block btn-rounded text-uppercase waves-effect waves-light']) !!}
          </div>
        </div>
           <div class="row">
          <div class="col-xs-12 col-sm-12 col-md-12 m-t-10 text-center">
            <div class="social"><a href="javascript:void(0)" class="btn  btn-facebook" data-toggle="tooltip"  title="Login with Facebook"> <i aria-hidden="true" class="fa fa-facebook"></i> </a> <a href="javascript:void(0)" class="btn btn-googleplus" data-toggle="tooltip"  title="Login with Google"> <i aria-hidden="true" class="fa fa-google-plus"></i> </a> </div>
          </div>
        </div>
        <div class="form-group m-b-0">
          <div class="col-sm-12 text-center">
            <p>Now Login Here... <a href='{!! url('/login'); !!}' class="text-primary m-l-5"><b>Login</b></a></p>
          </div>
        </div>
        {!! Form::close() !!}
    </div>
  </div>
</section>
<!-- jQuery -->
<script src="{{ asset('plugins/bower_components/jquery/dist/jquery.min.js') }}"></script>
<!-- Bootstrap Core JavaScript -->
<script src="{{ asset('Theme/bootstrap/dist/js/bootstrap.min.js') }}"></script>
<!-- Menu Plugin JavaScript -->
<script src="{{ asset('plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.js') }}"></script>

<!--slimscroll JavaScript -->
<script src="{{ asset('js/jquery.slimscroll.js') }}"></script>
<!--Wave Effects -->
<script src="{{ asset('js/waves.js') }}"></script>
<!-- Custom Theme JavaScript -->
<script src="{{ asset('js/custom.min.js') }}"></script>
<!--Style Switcher -->
<script src="{{ asset('plugins/bower_components/styleswitcher/jQuery.style.switcher.js') }}"></script>
</body>
</html>