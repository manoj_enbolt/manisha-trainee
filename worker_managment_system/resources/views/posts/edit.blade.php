@extends('header')
@section('welcome')
   <div id="page-wrapper"><!--Main Body Start-->
        <div class="container-fluid"><!-- container-fluid start -->
             <div class="row bg-title">
                <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                    <h2 class="page-title"><b>Edit Worker</b></h2>
                </div>
            </div>
			<div class="row">
				<div class="col-md-6 col-md-offset-3">
					 @if (session('success'))
	                    <div class="alert alert-success" role="alert">
	                        {{ session('success') }}
	                    </div>
               	 	@endif
                	@if (session('errors'))
	                    <div class="alert alert-danger" role="alert">
	                        {{ session('errors') }}
	                    </div>
                	@endif
			{!! Form::model($worker,['route' => ['worker.update',$worker->id],'files' => true,'method' => 'PATCH']) !!}
				@include('posts.form_master')
			{!! Form::close() !!}
		</div>
	</div>
@endsection