@extends('posts.head')
@section('content')
    <div class="row">
        <div class="col-md-6 col-md-offset-3">
    		@if (session('success'))
                <div class="alert alert-success" role="alert">
                    {{ session('success') }}
                </div>
            @endif
            {!! Form::open(['route' => 'manager.store', 'method' => 'POST', 'style' => 'padding-left:100px', 'files' => true, ])!!}
                @include('posts.manager_form_master')
            {!! Form::close() !!}
		</div>
    </div>
@endsection
