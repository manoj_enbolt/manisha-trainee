@extends('posts.head')
@section('content')
	<div class="row" style="background-color: white;">
        <div class="col-md-6 col-md-offset-3">
			@if (session('success'))
	           <div class="alert alert-success" role="alert">
	                {{ session('success') }}
	           </div>
	       @endif
		   {!! Form::open(['url' => 'manager/check', 'method' => 'POST','style' => 'padding-left:100px']) !!}
				@if (session('errors'))
                    <div class="alert alert-danger" role="alert">
                        {{ session('errors') }}
	        		</div>
	      		@endif
				<div class="row" style="height: 360px;width: 500px;background-color: white;margin-top: 60px; border: 1px groove black;">
					<h3 style="padding-top: 20px; padding-left: 30px;"><b>SIGN IN TO PANEL</b></h3><hr>

					<div class="col-sm-12" style="padding-left: 30px;">
						<div class="form-group {{ $errors->has('email') ? 'has-error' : "" }}">
							{!! Form::text('email',NULL, ['class' => 'form-control form-rounded','placeholder' => 'Email']) !!}<br>
                            {!! $errors->first('email','<strong class="help-block" style="color:red;">:message</strong>') !!}
						</div>
					</div>

					<div class="col-sm-12" style="padding-left: 30px;">
						<div class="form-group {{ $errors->has('email') ? 'has-error' : "" }}">
							{!! Form::password('password',['class' => 'form-control form-rounded','placeholder' => 'Password']) !!}<br>
							{!! $errors->first('password','<strong class="help-block" style="color:red;">:message</strong>') !!}
						</div>
    				</div>
							
					<div class="form-group" style="padding-left: 150px;">
		      			{!! Form::button(isset($model)? 'Update' : 'SUBMIT' , ['class' => 'btn btn-primary btn-block btn-rounded  text-uppercase waves-effect waves-light ', 'type' =>'submit' ,'style' => 'width: 200px;'])!!}
					</div>

                    <div class="form-group m-b-0">
                        <div class="col-sm-12 text-center">
                            <p>Don't have an account?<a href="{{ url('manager.create') }}" class="text-primary m-l-5"><b><U>CREATE MANAGER</U></b></a></p>
                        </div>
                    </div>
                </div>
			{!! Form::close() !!}
		</div>
	</div>
@endsection