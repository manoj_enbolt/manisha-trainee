@extends('header')
@section('welcome')
   <div id="page-wrapper"><!--Main Body Start-->
        <div class="container-fluid"><!-- container-fluid start -->
            <div class="row bg-title">
                <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                    <h2 class="page-title"><b>Display The Worker </b></h2>
                </div>
            </div>
			<div class="row">
				<div class="col-sm-12">
					<div class="table-responsive">
						<table class="table table-hover manage-u-table">
							<thead>
								<tr style="font-size: 15px;">
									<th style="width: 100px;" class="text-center">No</th>
									<th class="text-center">Name</th>
									<th class="text-center">Date Of Birth</th>
									<th class="text-center">Salary</th>
									<th class="text-center">Mobile No</th>
									<th class="text-center">Department</th>
									<th class="text-center">Image</th>
								</tr>
							</thead>
							<?php $no=1; ?>
							@foreach ($workers as $worker)
								<tr>
									<td class="text-center"><span class="font-muted">{{ $no++ }}</span></td>
									<td class="text-center"><span class="font-muted">{{ $worker->name }}</span></td>
									<td class="text-center"><span class="font-muted">{{ $worker->dob }}</span></td>
									<td class="text-center"><span class="font-muted">{{ $worker->salary }}</span></td>
									<td class="text-center"><span class="font-muted">{{ $worker->mobileno }}</span></td>
									<td class="text-center"><span class="font-muted">{{ $worker->department }}</span></td>
									<td class="text-center"><span class="font-muted"><img style="height: 100px;width: 150px; padding-left: 70px;" alt="{{ $worker->image }}" src="{{ asset('/storage/upload').'/'.$worker->image }}"></span></td>
								</tr>
							@endforeach
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection