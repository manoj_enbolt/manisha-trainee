<!DOCTYPE html>
<html lang="en">
<head>
   <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('plugins/images/favicon.png') }}">
    <title>Worker Managment System</title>

     <link href="{{ asset('Theme/bootstrap/dist/css/bootstrap.min.css') }}" rel="stylesheet">

    <link href="{{ asset('Theme/plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.css') }}" rel="stylesheet">
   
    <link href="{{ asset('plugins/bower_components/toast-master/css/jquery.toast.css') }}" rel="stylesheet">

    <link href="{{ asset('plugins/bower_components/chartist-js/dist/chartist.min.css') }}" rel="stylesheet">
    
    <link href="{{ asset('Theme/css/animate.css') }}" rel="stylesheet">
    
    <link href="{{ asset('Theme/css/style.css') }}" rel="stylesheet">

    <link href="{{ asset('Theme/css/colors/blue-dark.css') }}" id="theme" rel="stylesheet">
</head>
<style type="text/css">
    .form-rounded 
    {
        border-radius: 1rem;
    }
</style>
<body>
    @yield('content')
    <script src="{{ asset('plugins/bower_components/jquery/dist/jquery.min.js') }}"></script>
    
    <script src="{{ asset('Theme/bootstrap/dist/js/bootstrap.min.js') }}"></script>

    <script src="{{ asset('plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.js') }}"></script>
   
    <script src="{{ asset('js/jquery.slimscroll.js') }}"></script>
    
    <script src="{{ asset('js/waves.js') }}"></script>
    
    <script src="{{ asset('plugins/bower_components/waypoints/lib/jquery.waypoints.js') }}"></script>

    <script src="{{ asset('plugins/bower_components/counterup/jquery.counterup.min.js') }}"></script>
    
    <script src="{{ asset('plugins/bower_components/raphael/raphael-min.js') }}"></script>
   
    <script src="{{ asset('plugins/bower_components/chartist-js/dist/chartist.min.js') }}"></script>

    <script src="{{ asset('js/custom.min.js') }}"></script>

    <script src="{{ asset('js/cbpFWTabs.js') }}"></script>
    
    <script type="text/javascript">
        (function() {
            [].slice.call(document.querySelectorAll('.sttabs')).forEach(function(el) {
                new CBPFWTabs(el);
            });
        })();
    </script>
</body>
</html>