@extends('header')
@section('welcome')
   <div id="page-wrapper"><!--Main Body Start-->
            <div class="container-fluid"><!-- container-fluid start -->
                <div class="row bg-title">
                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                        <h2 class="page-title"><b>Display The Manager list</b></h2>
                    </div>
                </div>
				<div class="row">
					<div class="col-sm-12">
						<div class="table-responsive">
							<table class="table table-hover manage-u-table">
								<thead>
									<tr style="font-size: 15px;">
										<th style="width: 100px;" class="text-center">No</th>
										<th class="text-center">Name</th>
										<th class="text-center">Email</th>
										<th class="text-center">Department</th>
										<th class="text-center">Mobile No</th>
										<th class="text-center">Image</th>
										<th with="140px">
											<a href="{{route('manager.create')}}" class="btn btn-info btn-outline btn-circle btn-md m-r-5" style="margin-top: 10px; margin-left: 50px;">
												<i class="glyphicon glyphicon-plus"></i>
											</a>
										</th>
									</tr>
								</thead>
								<tbody>
									<?php $no=1; ?>
									@foreach ($managers as $manager)
									<tr>
										<td class="text-center"><span class="font-muted">{{ $no++ }}</span></td>
										<td class="text-center"><span class="font-muted">{{ $manager->name }}</span></td>
										<td class="text-center"><span class="font-muted">{{ $manager->email }}</span></td>
										<td class="text-center"><span class="font-muted">{{ $manager->department }}</span></td>
										<td class="text-center"><span class="font-muted">{{ $manager->mobileno }}</span></td>
										<td class="text-center"><span class="font-muted"><img style="height: 100px;width: 150px; padding-left: 70px;" alt="{{ $manager->image }}" src="{{ asset('/storage/upload').'/'.$manager->image }}"></span></td>
									<td>
										<a class="btn btn-info btn-outline btn-circle btn-md m-r-5" href="{{route('manager.edit',$manager->id)}}" style="margin-left: 50px;">
											<i class="glyphicon glyphicon-pencil"></i>
										</a>
										{!! Form::open(['method' => 'DELETE','route' => ['manager.destroy',$manager->id]]) !!}
											<button type="submit" class="btn btn-info btn-outline btn-circle btn-md m-r-5" style="margin-top: 10px; margin-left: 50px;">
												<i class="glyphicon glyphicon-trash"></i>
											</button>
										{!! Form::close() !!}
									</td>
								</tr>
							@endforeach
								</tbody>
							
						</table>
					</div>
					</div>
				</div>
    </div><!-- container-fluid end -->
</div><!--Main Body end-->
@endsection