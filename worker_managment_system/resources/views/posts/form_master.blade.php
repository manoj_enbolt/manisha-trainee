<style type="text/css">
    .form-rounded 
    {
        border-radius: 1rem;
    }
</style>
<div style="height: 640px;width: 500px;background-color: white;margin-top: 30PX;position: fixed;">
	<h3 style="padding-top: 20px; padding-left: 30px;"><b>Enter Your Detail Below</b></h3><hr>

	<div class="row">
		<div class="col-sm-10" style="padding-left: 50px;">
			<div class="form-group {{ $errors->has('name') ? 'has-error' : "" }}">
				{!! Form::text('name',NULL, ['class' => 'form-control form-rounded', 'id' => 'name', 'placeholder' => 'Name']) !!}
				{!! $errors->first('name','<strong class="help-block" style="color:red;">:message</strong>') !!}
			</div>
		</div>
	</div>

	<div class="row">
		<div class="col-sm-5" style="padding-left: 50px;">
			<div class="form-group {{ $errors->has('dob') ? 'has-error' : "" }}">
				{!! Form::date('dob',NULL, ['class' => 'form-control form-rounded','placeholder' => 'Date Of Birth']) !!}<br>
				{!! $errors->first('dob','<strong class="help-block" style="color:red;">:message</strong>') !!}
			</div>
		</div>
		<div class="col-sm-5" style="padding-left: 50px;">
			<div class="form-group {{ $errors->has('salary') ? 'has-error' : "" }}">
				{!! Form::number('salary',NULL, ['class' => 'form-control form-rounded','placeholder' => 'Salary']) !!}<br>
				{!! $errors->first('salary','<strong class="help-block" style="color:red;">:message</strong>') !!}
			</div>
		</div>
	</div>

	<div class="row">
		<div class="col-sm-10" style="padding-left: 50px;">
			<div class="form-group {{ $errors->has('mobileno') ? 'has-error' : "" }}">
				{!! Form::text('mobileno',NULL, ['class' => 'form-control form-rounded','placeholder' => 'Mobileno']) !!}<br>
				{!! $errors->first('mobileno','<strong class="help-block" style="color:red;">:message</strong>') !!}
			</div>
		</div>
	</div>

	<div class="row">
		<div class="col-sm-10" style="padding-left: 50px;">
			<div class="form-group {{ $errors->has('image') ? 'has-error' : "" }}">
				{!! Form::file('image',['class'=>'btn-white form-control form-rounded', 'placeholder'=>'Enter image Url']) !!}<br>
				{!! $errors->first('image','<strong class="help-block" style="color:red;">:message</strong>') !!}
			</div>
		</div>
	</div>

	<div class="row">
		<div class="col-sm-4" style="padding-left: 50px;">
			{!! Form::label('department','Department') !!}
		</div>
		<div class="col-sm-8">
			<div class="form-group {{ $errors->has('department') ? 'has-error' : "" }}">
				{!! Form::select('department', ['Technical' => 'Technical', 'Solution' => 'Solution', 'Accountance' => 'Accountance', 'Manufacture' => 'Manufacture']) !!}<br>
				{!! $errors->first('department','<strong class="help-block" style="color:red;">:message</strong>') !!}
			</div>
		</div>
	</div>

	<div class="form-group text-center m-t-20">
		<div class="row">
			<div class="col-sm-10" style="padding-left: 50px;"> 
				{!! Form::button(isset($model)? 'Update' : 'SUBMIT' , ['class' => 'btn btn-primary btn-block btn-rounded  text-uppercase waves-effect waves-light ', 'type' =>'submit' ,'style' => 'width: 200px;']) !!}
			</div>
		</div>
</div>