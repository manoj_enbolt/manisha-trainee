<!DOCTYPE html>
<html lang="en">
<head>
   <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('plugins/images/favicon.png') }}">
    <title>Worker Managment System</title>

    <link href="{{ asset('https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css') }}" rel="stylesheet">

     <link href="{{ asset('Theme/bootstrap/dist/css/bootstrap.min.css') }}" rel="stylesheet">

    <link href="{{ asset('Theme/plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.css') }}" rel="stylesheet">
   
    <link href="{{ asset('plugins/bower_components/toast-master/css/jquery.toast.css') }}" rel="stylesheet">

    <link href="{{ asset('plugins/bower_components/chartist-js/dist/chartist.min.css') }}" rel="stylesheet">
    
    <link href="{{ asset('Theme/css/animate.css') }}" rel="stylesheet">
    
    <link href="{{ asset('Theme/css/style.css') }}" rel="stylesheet">

    <link href="{{ asset('Theme/css/colors/blue-dark.css') }}" id="theme" rel="stylesheet">

    <link href="{{ asset('https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css') }}" rel="stylesheet" />
</head>
<body class="fix-header">
    <div class="preloader">
        <svg class="circular" viewBox="25 25 50 50">
            <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" />
        </svg>
    </div>
     <div id="wrapper">
        <nav class="navbar navbar-default navbar-static-top m-b-0">
            <div class="navbar-header"><!--header start-->
                <div class="top-left-part">
                    <a class="logo" href="index.html">
                        <b>
                            <img src="{{ asset('plugins/images/admin-logo.png')}}" alt="home" class="dark-logo" />
                            <img src="{{ asset('plugins/images/admin-logo-dark.png')}}" alt="home" class="light-logo" />
                        </b>
                        <span class="hidden-xs">
                            <img src="{{ asset('plugins/images/admin-text.png')}}" alt="home" class="dark-logo" />
                            <img src="{{ asset('plugins/images/admin-text-dark.png')}}" alt="home" class="light-logo" />
                        </span>
                    </a>
                </div>
                 <ul class="nav navbar-top-links navbar-left">
                    <li><a href="javascript:void(0)" class="open-close waves-effect waves-light visible-xs"><i class="ti-close ti-menu"></i></a></li>
                    <li class="dropdown">
                        <a class="dropdown-toggle waves-effect waves-light" data-toggle="dropdown" href="#"> <i class="mdi mdi-gmail"></i>
                            <div class="notify"> <span class="heartbit"></span> <span class="point"></span> </div>
                        </a>
                        <ul class="dropdown-menu mailbox animated bounceInDown">
                            <li>
                                <div class="drop-title">You have 4 new messages</div>
                            </li>
                        </ul>
                    </li>
                </ul>
                <ul class="nav navbar-top-links navbar-right pull-right">
                    <li>
                        <form role="search" class="app-search hidden-sm hidden-xs m-r-10">
                            <input type="text" placeholder="Search..." class="form-control">
                                <a href=""><i class="fa fa-search"></i></a>
                        </form>
                    </li>
                    <li class="dropdown">
                        <a class="dropdown-toggle profile-pic" data-toggle="dropdown" href="#">
                            <img src="{{ asset('plugins/images/users/IMG_20180601_132416_350.jpg') }}" alt="user-img" width="36" class="img-circle">
                            <b class="hidden-xs">Profile</b>
                            <span class="caret"></span>
                        </a>
                        <ul class="dropdown-menu dropdown-user animated flipInY">
                            <li>
                                <div class="dw-user-box">
                                    <div class="u-img">
                                        <img src="{{ asset('plugins/images/users/IMG_2') }}0180601_132416_350.jpg" alt="user" />
                                    </div>
                                    <div class="u-text">
                                        <h4>Welcome User</h4>
                                        <p class="text-muted">Manisha@gmial.com</p>
                                        <a href="profile.html" class="btn btn-rounded btn-danger btn-sm">View Profile</a>
                                    </div>
                                </div>
                            </li>
                            <li role="separator" class="divider"></li>
                            <li><a href="#"><i class="ti-user"></i> My Profile</a></li>
                            <li><a href="#"><i class="ti-wallet"></i> My Balance</a></li>
                            <li><a href="#"><i class="ti-email"></i> Inbox</a></li>
                            <li role="separator" class="divider"></li>
                            <li><a href="#"><i class="ti-settings"></i> Account Setting</a></li>
                            <li role="separator" class="divider"></li>
                            <li>
                                <a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                    {{-- {{ __('Logout') }}> --}}
                                   <i class="fa fa-power-off"></i> Logout
                                </a>
                            </li>
                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">@csrf</form>
                        </ul>
                    </li>
                </ul>
            </div><!--header end-->
        </nav>
        <div class="navbar-default sidebar" role="navigation"><!--sidebar start-->
            <div class="sidebar-nav slimscrollsidebar">
                <div class="sidebar-head">
                    <h3>
                        <span class="fa-fw open-close">
                            <i class="ti-menu hidden-xs"></i>
                            <i class="ti-close visible-xs"></i>
                        </span>
                        <span class="hide-menu">MENU</span>
                    </h3>
                </div>
                <ul class="nav" id="side-menu">
                    <li class="user-pro">
                        <a href="#" class="waves-effect">
                            <img src="{{ asset('plugins/images/users/IMG_2') }}0180601_132416_350.jpg" alt="user-img" class="img-circle">
                            <span style="color: white;"><b>Welcome User</b></span>
                        </a>
                    </li>
                    <li>
                        <a href="index.html" class="waves-effect">
                            <i class="mdi mdi-av-timer fa-fw" data-icon="v"></i>
                            <span class="hide-menu">Manager<span class="fa arrow"></span>
                                <span class="label label-rouded label-inverse pull-right">2</span>
                            </span>
                        </a>

                       <ul class="nav nav-second-level">
                            {{-- <li><a href="{{ url('/manager_login') }}"><i class=" fa-fw">1</i><span class="hide-menu">Manager Login</span></a></li> --}}
                            <li><a href="{{ url('manager') }}"><i class=" fa-fw">1</i><span class="hide-menu">View Manager</span></a> </li>
                            <li><a href="{{ url('worker') }}"><i class=" fa-fw">2</i><span class="hide-menu">Create Worker</span></a> </li>
                        </ul> 
                    </li>
                    <li class="devider"></li>
                    <li>
                        <a href="forms.html" class="waves-effect">
                            <i class="mdi mdi-clipboard-text fa-fw"></i>
                            <span class="hide-menu">Student Forms<span class="fa arrow"></span></span>
                        </a>
                        <ul class="nav nav-second-level">
                            <li><a href="{{ url('student/register') }}"><span class="hide-menu">Student Registration Form</span></a></li>
                            <li><a href="form-layout.html"><span class="hide-menu">Form Layout</span></a></li>
                            <li><a href="form-advanced.html"><span class="hide-menu">Form Addons</span></a></li>
                        </ul>
                    </li>
                    <li>
                            <a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                    <i class="fa fa-power-off" style="margin-left: 10px;"></i><span> <B> LOGOUT</B></span>
                            </a>
                    </li>                       
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">@csrf</form>
                        <li class="devider"></li>
                </ul>
            </div>
        </div><!--sidebar end-->
            @yield('welcome')
        <footer class="footer text-center" style="background-color: #B6B6B6;"> 2017 &copy; Ample Admin brought to you by themedesigner.in </footer>
    </div>
    
    <script src="{{ asset('//cdn.ckeditor.com/4.10.0/basic/ckeditor.js') }}"></script>
    <script src="{{ asset('plugins/bower_components/jquery/dist/jquery.min.js') }}"></script>

     <script src="{{ asset('https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js') }}"></script>
    
    <script src="{{ asset('Theme/bootstrap/dist/js/bootstrap.min.js') }}"></script>

    <script src="{{ asset('plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.js') }}"></script>
   
    <script src="{{ asset('js/jquery.slimscroll.js') }}"></script>
    
    <script src="{{ asset('js/waves.js') }}"></script>
    
    <script src="{{ asset('plugins/bower_components/waypoints/lib/jquery.waypoints.js') }}"></script>

    <script src="{{ asset('plugins/bower_components/counterup/jquery.counterup.min.js') }}"></script>
    
    <script src="{{ asset('plugins/bower_components/raphael/raphael-min.js') }}"></script>
   
    <script src="{{ asset('plugins/bower_components/chartist-js/dist/chartist.min.js') }}"></script>

    <script src="{{ asset('https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js') }}"></script>

    <script src="{{ asset('js/custom.min.js') }}"></script>

    <script src="{{ asset('js/cbpFWTabs.js') }}"></script>
    
    @yield('script')
    <script type="text/javascript">
        (function() {
            [].slice.call(document.querySelectorAll('.sttabs')).forEach(function(el) {
                new CBPFWTabs(el);
            });
        })();
    </script>
</body>
</html>