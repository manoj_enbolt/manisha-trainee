@extends('header')
@section('welcome')
	<style type="text/css">
	    .form-rounded 
	    {
	        border-radius: 1rem;
	    }
	</style>
	
 	<div id="page-wrapper" style="background-color: white;"><!--Main Body Start-->
        <div class="container-fluid" id="replace-view"><!-- container-fluid start -->
             <div class="row bg-title">
                <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                    <h2 class="page-title"><b>Student Registration</b></h2>
                </div>
            </div>
			<div class="row">
				<div class="col-md-12">
					@if (session('success'))
                		<div class="alert alert-success" role="alert">
                    		{{ session('success') }}
                		</div>
            		@endif
            		<div class="alert alert-danger print-error-msg" style="display:none">
        				<ul></ul>
    				</div>

					{!! Form::open(['url' => 'ajax-request','id' => 'student_form','method' => 'post', 'files' => true, 'enctype' => 'multipart/form-data'])!!}

						<div class="form-body">
							<h3 class="box-title"><i>PERSON INFO</i></h3>
							<hr class="m-t-0 m-b-40">

							<div class="row">
								<div class="col-md-6">
									<div class="form-group">
										<div class="col-md-3" style="padding-left: 80px;padding-top: 10px;">
											{!! Form::label('first_name','First Name') !!}
										</div>
										<div class="col-md-9">
											{!! Form::text('first_name',NULL, ['class' => 'form-control form-rounded','placeholder' => 'First Name']) !!}<br>
										</div>
                              		</div>
                              	</div>
                              	<div class="col-md-6">
	                              	<div class="form-group">
										<div class="col-md-3" style="padding-left: 80px;padding-top: 10px;">
											{!! Form::label('last_name','Last Name') !!}
										</div>
										<div class="col-md-9">
											{!! Form::text('last_name',NULL, ['class' => 'form-control form-rounded','placeholder' => 'Last Name']) !!}<br>
										</div>
	                              	</div>
	                            </div>
	                        </div><br>

	                        <div class="row">
								<div class="col-md-6">
									<div class="form-group">
										<div class="col-md-3" style="padding-left: 70px;padding-top: 10px;">
											{!! Form::label('profile_image','Profile Image') !!}
										</div>
										<div class="col-md-9">
											{!! Form::file('profile_image',['class'=>'btn-white form-control form-rounded', 'placeholder'=>'Profile Image']) !!}
										</div>
									</div>
                              	</div>
                              	<div class="col-md-6">
	                              	<div class="form-group">
										<div class="col-md-3" style="padding-left: 60px;padding-top: 10px;">
											{!! Form::label('date_of_birth','Date Of Birth') !!}
										</div>
										<div class="col-md-9">
											{!! Form::date('date_of_birth',NULL, ['class' => 'form-control form-rounded','placeholder' => 'Date Of Birth']) !!}<br>
										</div>
	                              	</div>
	                            </div>
                            </div><br>

                            <div class="row">
                            	<div class="col-md-6">
									<div class="form-group">
										<div class="col-md-3" style="padding-left: 80px;padding-top: 10px;">
											{!! Form::label('gender','Gender') !!}
										</div>
										<div class="col-md-9">
											<div class="radio-list">
                                                <div class="radio-inline" style="padding-left: 50px;padding-top: 10px;">
													{!! Form::radio('gender','male') !!} Male
												</div>
												<div class="radio-inline" style="padding-left: 60px;padding-top: 10px;">
													{!! Form::radio('gender','female') !!} Female<br>
												</div>
											</div>
										</div>
                              		</div>
                              	</div>
                              	<div class="col-md-6">
	                              	<div class="form-group">
										<div class="col-md-3" style="padding-left: 80px;padding-top: 10px;">
											{!! Form::label('skills','Skills') !!}
										</div>
										<div class="col-md-9">
											<select class="js-example-basic-multiple" name="skills[]" multiple="multiple" style="width: 500px;">
  												<option value="Technical Skill">Technical Skill</option>
   												<option value="Problem Solving Skill">Problem Solving Skill</option>
  												<option value="Communication Skills">Communication Skills</option>
  												<option value="Adaptive Thinking">Adaptive Thinking</option>
											</select>
										</div>
	                              	</div>
	                            </div>
                            </div><br>
						</div>

						<div class="form-body">
							<h3 class="box-title"><i>ADDRESS</i></h3>
							<hr class="m-t-0 m-b-40">

							<div class="row">
								<div class="col-md-6">
									<div class="form-group">
										<div class="col-md-3" style="padding-left: 80px;padding-top: 10px;">
											{!! Form::label('address','Address') !!}
										</div>
										<div class="col-md-9">
											{!! Form::text('address',NULL, ['class' => 'form-control form-rounded','placeholder' => 'Address']) !!}<br>											
										</div>
                              		</div>
                              	</div>
                            </div><br>

                            <div class="row">
                              	<div class="col-md-6">
	                              	<div class="form-group">
										<div class="col-md-3" style="padding-left: 80px;padding-top: 10px;">
											{!! Form::label('about_me','About Me') !!}
										</div>
										<div class="col-md-9">
											{!! Form::textarea('about_me',NULL, ['rows' => 3, 'cols' => 54, 'style' => 'resize:none;','id' => 'about_me','placeholder' => 'About Me']) !!}<br>
										</div>
	                              	</div>
	                            </div>
	                        </div><br>

	                        <div class="row">
								<div class="col-md-6">
									<div class="form-group">
										<div class="col-md-3" style="padding-left: 50px;padding-top: 10px;">
											{!! Form::label('state_id','Select State') !!}
										</div>
										<div class="col-md-9">
											{!! Form::select('state_id',['' => '--- Select State ---']+$countries,null,['class'=>'form-control']) !!}<br>											
										</div>
                              		</div>
                              	</div>

                              	<div class="col-md-6">
	                              	<div class="form-group">
										<div class="col-md-3" style="padding-left: 80px;padding-top: 10px;">
											{!! Form::label('city_id','Select City') !!}
										</div>
										<div class="col-md-9">
											{!! Form::select('city_id',['' => '--- Select City ---'],null,['class'=>'form-control']) !!}<br>
										</div>
	                              	</div>
	                            </div>
	                        </div><br>
    					</div>

    					<div class="form-actions">
                            <div class="row">
								<div class="col-md-6">
									<div class="row">
										<div class="col-md-offset-3 col-md-9">
											{!! Form::submit('SUBMIT',array('name' => 'submit','class' => 'btn btn-info', 'id' => 'form-submit', 'type' =>'submit' ,'style' => 'width: 100px;'))!!}
											{!! Form::button('CANCEL' , ['class' => 'btn btn-default', 'type' =>'button' ,'style' => 'width: 100px;'])!!}
                                        </div>
                                    </div>
                                </div>
                           		<div class="col-md-6"></div>
                            </div>
                        </div>

					{!! Form::close() !!}
				</div>
			</div>  
		</div>
	</div>
@endsection
@section('script')
	<script type="text/javascript">
		$(document).ready(function() {
			$("select[name='state_id']").change(function(){
				var state_id = $(this).val();
				var token = $("input[name='_token']").val();
				$.ajax({
					url: " {{ route('select-ajax') }}",
					method: 'POST',
					data: {state_id:state_id, _token:token},
					success: function(data) {
						$("select[name='city_id'").html(data.options);
			        }
			    });
			});
			$("#student_form").on('submit', function(e) {
				e.preventDefault();
				var formData = new FormData(this);
				$.ajax({
					type: "post",
					headers: {
					    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
					},
			        url: "{{ url('ajax-request') }}",
			        data: formData,
			        enctype: 'multipart/form-data',
			        cache:false,
					contentType: false,
					processData: false,
			        success: function(response) {
			        	if($.isEmptyObject(response.error)){
	                		$("#replace-view").html(response);
	                	} else {
               				printErrorMsg(response.error);
			        	}
			        },
			        error: function(data){
			        }
				});
			});
			function printErrorMsg (msg){
				$(".print-error-msg").find("ul").html('');
				$(".print-error-msg").css('display','block');
				$.each( msg, function( key, value ){
					$(".print-error-msg").find("ul").append('<li>'+value+'</li>');
				});
			}
			$('.js-example-basic-multiple').select2();
			$('#table').DataTable({
		        processing: true,
		        serverSide: true,
		        ajax: '{{ url('index') }}',
		        columns: [
		            {data: 'id', name: 'id'},
		            {data: 'first_name', name: 'first_name'},
		            {data: 'last_name', name: 'last_name'},
		            {data: 'profile_image', name: 'profile_image'},
		            {data: 'date_of_birth', name: 'date_of_birth'},
		            {data: 'skills', name: 'skills'},
		            {data: 'address', name: 'address'},
		            {data: 'about_me', name: 'about_me'},
		            {data: 'state_id', name: 'state_id'},
		            {data: 'city_id', name: 'city_id'}
		        ]
    		});
		});
	</script>
<script type="text/javascript">
	 CKEDITOR.replace( 'about_me' );
</script>
@endsection