<div class="container-fluid" id="replace-view"><!-- container-fluid start -->
    <div class="row bg-title">
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h2 class="page-title"><b>Display The Student Data</b></h2>
        </div>
    </div>
	<div class="row">
		<div class="col-sm-12">
			<div class="table-responsive">
				<table class="table table-hover manage-u-table" id="table">
					<thead>
						<tr style="font-size: 15px;">
							<th style="width: 100px;" class="text-center">No</th>
							<th class="text-center">First Name</th>
							<th class="text-center">Last Name</th>
							<th class="text-center">Profile Image</th>
							<th class="text-center">Date Of Birth</th>
							<th class="text-center">Gender</th>
							<th class="text-center">Skill</th>
							<th class="text-center">Address</th>
							<th class="text-center">About Me</th>
							<th class="text-center">State</th>
							<th class="text-center">City</th>	
						</tr>
					</thead>
					<tbody>
						<?php $no=1; ?>
						@foreach ($students as $student)
						<tr>
							<td class="text-center"><span class="font-muted">{{ $no++ }}</span></td>
							<td class="text-center"><span class="font-muted">{{ $student->first_name }}</span></td>
							<td class="text-center"><span class="font-muted">{{ $student->last_name }}</span></td>
							<td class="text-center"><span class="font-muted"><img style="height: 100px;width: 150px; padding-left: 50px;" alt="{{ $student->profile_image }}" src="{{ asset('/storage/upload').'/'.$student->profile_image }}"></span></td>
							<td class="text-center"><span class="font-muted">{{ $student->date_of_birth }}</span></td>
							<td class="text-center"><span class="font-muted">{{ $student->gender }}</span></td>
							<td class="text-center"><span class="font-muted">{{ $student->skills }}</span></td>
							<td class="text-center"><span class="font-muted">{{ $student->address }}</span></td>
							<td class="text-center"><span class="font-muted">{!! $student->about_me !!}</span></td>
							<td class="text-center"><span class="font-muted">{{ $student->state_id }}</span></td>
							<td class="text-center"><span class="font-muted">{{ $student->city_id }}</span></td>
						</tr>
					@endforeach
				</tbody>
			</table>
		</div>
	</div>
</div>
</div>
