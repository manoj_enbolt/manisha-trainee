<!DOCTYPE html>  
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">
<link rel="icon" type="image/png" sizes="16x16" href="{{ asset('plugins/images/favicon.png') }}">
<title>Ample Admin Template - The Ultimate Multipurpose admin template</title>
<!-- Bootstrap Core CSS -->
<link href="{{ asset('Theme/bootstrap/dist/css/bootstrap.min.css') }}" rel="stylesheet">
<!-- animation CSS -->
<link href="{{ asset('Theme/css/animate.css') }}" rel="stylesheet">
<!-- Custom CSS -->
<link href="{{ asset('Theme/css/style.css') }}" rel="stylesheet">
<!-- color CSS -->
<link href="{{ asset('Theme/css/colors/blue.css') }}" id="theme"  rel="stylesheet">
</head>
<body>
<!-- Preloader -->
<div class="preloader">
  <div class="cssload-speeding-wheel"></div>
</div>
<section id="wrapper" class="login-register">
  <div class="login-box login-sidebar">
    <div class="white-box">
      {!! Form::open(['url' => 'login','method' => 'post']) !!}
      @if (session('status'))
        <strong><div class="alert alert-danger" role="alert">{{ session('status') }}</div></strong>
      @endif
        <a href="javascript:void(0)" class="text-center db"><img src="{{ asset('plugins/images/admin-logo-dark.png') }}" alt="Home" /><br/><img src="{{ asset('plugins/images/admin-text-dark.png') }}" alt="Home" /></a><br><br>
        <h3 class="box-title m-b-0"><center>Sign In to Admin</center></h3>
        <div class="form-group m-t-40">
          <div class="col-xs-12">
            {{-- {!!Form::label('email','Email', ['class' => 'awesome']) !!}  --}}
            {!! Form::text('email','', ['class' => 'form-control','placeholder' => 'Email']) !!}<br>
            {{-- {{ $errors->has('email')?$errors->first('email'):'' }} --}}
            @if ($errors->has('email'))
              <strong><ul>{!! $errors->first('email','<li style="color:red">:message</li>') !!}</ul></strong>
            @endif
          </div>
        </div>
        <div class="form-group m-t-40">
          <div class="col-xs-12">
            {{-- {!! Form::label('password','Password',['class' => 'form-control']) !!} --}}
            {!! Form::password('password', ['class' => 'form-control','placeholder' => 'Password']) !!}<br>
            {{-- {{ $errors->has('password')?$errors->first('password'):'' }} --}}
            @if ($errors->has('password'))
              <strong><ul>{!! $errors->first('password','<li style="color:red">:message</li>') !!}</ul></strong>
            @endif
          </div>
        </div>
       <div class="form-group">
          <div class="col-md-12">
            {!! Form::checkbox('checkbox-signup','rememberme',['class' => 'checkbox checkbox-info pull-left p-t-0']) !!}
            {!! Form::label('rememberme','Remember me') !!}
            </div>
            <a href="{!! url('forgot-password'); !!}" id="to-recover" class="text-dark pull-right"><i class="fa fa-lock m-r-5"></i> Forgot pwd?</a>
           </div>
        <div class="form-group text-center m-t-20">
          <div class="col-xs-12">
           {!! Form::submit('Log In..!',['class' => 'btn btn-info btn-lg btn-block btn-rounded text-uppercase waves-effect waves-light']) !!}
          </div>
        </div>
        <div class="row">
          <div class="col-xs-12 col-sm-12 col-md-12 m-t-10 text-center">
            <div class="social"><a href="javascript:void(0)" class="btn  btn-facebook" data-toggle="tooltip"  title="Login with Facebook"> <i aria-hidden="true" class="fa fa-facebook"></i> </a> <a href="javascript:void(0)" class="btn btn-googleplus" data-toggle="tooltip"  title="Login with Google"> <i aria-hidden="true" class="fa fa-google-plus"></i> </a> </div>
          </div>
        </div>
        <div class="form-group m-b-0">
          <div class="col-sm-12 text-center">
            <p>Don't have an account? <a href='{!! url('/registration'); !!}' class="text-primary m-l-5"><b>Registration</b></a></p>
          </div>
        </div>
        {!! Form::close() !!}
    </div>
  </div>
</section>
<!-- jQuery -->
<script src="{{ asset('plugins/bower_components/jquery/dist/jquery.min.js') }}"></script>
<!-- Bootstrap Core JavaScript -->
<script src="{{ asset('Theme/bootstrap/dist/js/bootstrap.min.js') }}"></script>
<!-- Menu Plugin JavaScript -->
<script src="{{ asset('plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.js') }}"></script>

<!--slimscroll JavaScript -->
<script src="{{ asset('js/jquery.slimscroll.js') }}"></script>
<!--Wave Effects -->
<script src="{{ asset('js/waves.js') }}"></script>
<!-- Custom Theme JavaScript -->
<script src="{{ asset('js/custom.min.js') }}"></script>
<!--Style Switcher -->
<script src="{{ asset('plugins/bower_components/styleswitcher/jQuery.style.switcher.js') }}"></script>
</body>
</html>