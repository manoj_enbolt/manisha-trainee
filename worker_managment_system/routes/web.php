<?php

Route::get('/','PagesController@home');

Route::get('registration','RegistarationController@registration');
Route::post('admin/store','RegistarationController@store');

Route::get('forgot-password','LoginController@forgotpassword');

Route::get('manager_login','Manager_LoginController@manager_login');
Route::post('manager/check','Manager_LoginController@check');

Route::get('student/register', 'Student_RegisterController@myform');
Route::post('ajax-request', 'Student_RegisterController@ajaxRequestPost');
Route::get('index', 'Student_RegisterController@index');
Route::post('select-ajax', 'Student_RegisterController@selectAjax')->name('select-ajax');

Auth::routes();

Route::get('worker/get-department-wise-worker','WorkerController@getDepartmentWiseWorker');

Route::get('/home', 'HomeController@index')->name('home');

Route::resource('worker','WorkerController');

Route::resource('manager','ManagerController');

