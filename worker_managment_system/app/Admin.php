<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\Model;

class Admin extends Authenticatable
{
	use Notifiable;
	
    protected $fillable = [
        'name', 'email', 'password',
    ];
    public function is_admin()
    {
    	if($this->admin)
    	{
    		return true;
    	}
    	return false;
    }
}
