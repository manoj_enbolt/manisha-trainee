<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Auth;

class Manager extends Authenticatable
{
	use Notifiable;
    // protected $redirectTo = '/';

    protected $fillable = ['name', 'email','password','department','mobileno','image',];
   // protected function guard()
   //  {
   //    return Auth::guard('manager');
   //  }

   //  //Shows seller login form
   // public function showLoginForm()
   // {
   //     return view('posts.manager_login');
   // }
}