<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Auth;

class Student extends Authenticatable
{
    use Notifiable;

    protected $fillable = ['first_name','last_name','profile_image','date_of_birth','gender','skills','address','about_me','state_id','city_id',];
}
