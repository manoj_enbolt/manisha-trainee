<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Admin;

class LoginController extends Controller
{
	public function login()
  {
    return view('Login');
	}
  public function forgotpassword()
  {
    return view('forgot_password');
  }
	public function store(Request $request)
 	{
 		$this->validate($request, [
            'email' => 'required|string|email|max:100',
            'password' => 'required|string|min:6', 
  		]);
  		return redirect('/');
  	}

  	// public function logins(Request $request)
  	// {
  	// 	if(Auth::attempt([
  	// 		'email' => $request->email,
  	// 		'password' => $request->password,
  	// 	]))
  	// 	{
  	// 		$user = Admin::where('email',$request->email)->first();
  	// 		dd($user);
  	// 		if($user->is_admin())
  	// 		{
  	// 			return redirect('/');
  	// 		}
  	// 		echo 'not confirmed';
  	// 	}
  	// }
}