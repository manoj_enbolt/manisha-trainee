<?php
namespace App\Http\Controllers;

use App\Worker;
use Illuminate\Http\Request;

class WorkerController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:manager');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $workers = Worker::all();
        return view('posts.index',compact('workers'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('posts.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'name' => 'required|string|unique:workers',
            'dob' => 'required',
            'salary' => 'required',
            'mobileno' => 'required|digits:10:|unique:workers',
            'image' => 'mimes:jpeg,jpg,png,gif|required|max:10000',
            'department' => 'required',
        ]);
        if($request->hasFile('image'))
        {
            $filename = $request->image->getClientOriginalName();
            $request->image->storeAs('/public/upload',$filename);
        }
        $worker = $request->all();
        $worker['password'] = bcrypt($request->password);
        $worker['image'] = $request->image->getClientOriginalName();

        Worker::create($worker);
        return redirect()->route('worker.index')->with('success','Post Created Succesfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $worker = Worker::find($id);
        return view('posts.edit',compact('worker'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'name' => 'required|string',
            'dob' => 'required',
            'salary' => 'required',
            'mobileno' => 'required|digits:10',
            'image' => 'mimes:jpeg,jpg,png,gif|required|max:10000',
            'department' => 'required',
        ]);
        if($request->hasFile('image'))
        {
            $filename = $request->image->getClientOriginalName();
            $request->image->storeAs('/public/upload',$filename);
        }

        $worker = $request->all();
        $worker['password'] = bcrypt($request->password);
        $worker['image'] = $request->image->getClientOriginalName();

        Worker::find($id)->update($worker);
        return redirect()->route('worker.index')->with('success','Post update Succesfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Worker::find($id)->delete();
        return redirect()->route('worker.index')->with('success','Post Delete Succesfully');
    }

    public function getDepartmentWiseWorker(Request $request)
    {
        $user = \Auth::guard('manager')->user();
        
        $worker = new Worker;
        $workers = $worker->getDepartmentWiseWorker($user);  
        return view('posts.worker_view', ['workers' => $workers]);
        // dd($workers);
    }
}