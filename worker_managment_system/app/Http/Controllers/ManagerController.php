<?php

namespace App\Http\Controllers;
use App\Manager;
use Illuminate\Http\Request;
use Auth;

class ManagerController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $managers = Manager::all();
        return view('posts.manager_index',compact('managers'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('posts.manager_create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Responsew
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'name' => 'required|string',
            'email' => 'required|unique:managers',
            'password' => 'required',
            'department' => 'required',
            'mobileno' => 'required|digits:10',
            'image' => 'mimes:jpeg,jpg,png,gif|required|max:10000',   
        ]);
        if($request->hasFile('image'))
        {
            $filename = $request->image->getClientOriginalName();
            $request->image->storeAs('/public/upload',$filename);
        }
        $manager = $request->all();
        $manager['password'] = bcrypt($request->password);
        $manager['image'] = $request->image->getClientOriginalName();
        
        Manager::create($manager);
        return view('posts.manager_login');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $manager = Manager::find($id);
        return view('posts.manager_edit',compact('manager'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'name' => 'required|string',
            'email' => 'required',
            'password' => 'required',
            'department' => 'required',
            'mobileno' => 'required|digits:10',
            'image' => 'mimes:jpeg,jpg,png,gif|required|max:10000',   
        ]);
        if($request->hasFile('image'))
        {
            $filename = $request->image->getClientOriginalName();
            $request->image->storeAs('/public/upload',$filename);
        }

        $manager = $request->all();
        $manager['password'] = bcrypt($request->password);
        $manager['image'] = $request->image->getClientOriginalName();

        Manager::find($id)->update($manager);
        return redirect()->route('manager.index')->with('success','Post update Succesfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Manager::find($id)->delete();
        return redirect()->route('manager.index')->with('success','Post Delete Succesfully');
    }
}
