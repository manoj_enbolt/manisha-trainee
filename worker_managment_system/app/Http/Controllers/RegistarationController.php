<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Admin;

class RegistarationController extends Controller
{
	public function registration()
	{
		return view('registration');
	}

 	public function store(Request $request)
 	{
    // dd($request);
 		$this->validate($request, [
 			'name' => 'required|string|max:100',
      'email' => 'required|string|email|max:100|unique:admins',
      'password' => 'required|string|min:6', 
    ]);
  		
 		$response = Admin::create([
      'name' => $request['name'],
      'email' => $request['email'],
      'password' => bcrypt($request['password']),
    ]);
 		return redirect('login');	
 	}   
}
