<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Manager;
use Auth;

class Manager_LoginController extends Controller
{
    public function manager_login()
    {
    	return view('posts.manager_login');
    }
    
    public function check(Request $request)
    {
        $this->validate($request, [
            'email' => 'required|string|email|max:100',
            'password' => 'required|string',
        ]);

        if(Auth::guard('manager')->attempt([
            'email' => $request->email,
            'password' => $request->password,
        ])) 
        {
            // $user = Manager::where('email', $request->email)->first();
            return redirect('worker/get-department-wise-worker');
        }
        else
        {
           return redirect()->back()->withInput($request->only('email', 'remember'))->withErrors('Wrong password or this account not approved yet.');
            // echo 'not mached';
        }
                
    }
}