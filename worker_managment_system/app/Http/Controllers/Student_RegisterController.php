<?php

namespace App\Http\Controllers;

use App\Student;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Redirect;
use DataTables;
use DB;
use Auth;
use Validator;

class Student_RegisterController extends Controller
{
	/**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:manager');
    }
    public function index()
    {
        return Datatables::of(User::query())->make(true);
    }
     public function myform()
    {
    	$countries = DB::table('countries')->pluck("name","id")->all();
    	return view('student.student-register',compact('countries'));
    }

    public function selectAjax(Request $request)
    {
    	if($request->ajax())
    	{
    		$states = DB::table('states')->where('state_id',$request->state_id)->pluck("name","id")->all();
    		$data = view('student.ajax-select',compact('states'))->render();
    		return response()->json(['options'=>$data]);
    	}
    }

    public function ajaxRequestPost(Request $request)
	{
		$validator = Validator::make($request->all(), [
            'first_name' => 'required|regex:/^[a-zA-Z ]+$/|string|unique:students',
            'last_name' => 'required|regex:/^[a-zA-Z ]+$/|string',
            'profile_image' => 'mimes:jpeg,jpg,png,gif|required|max:10000',
            'date_of_birth' => 'required|date:Y-M-D',
            'gender' => 'required',
            'skills' => 'required',
            'address' => 'required',
            'about_me' => 'required',
            'state_id' => 'required',
            'city_id' => 'required',     
        ]);

        if ($validator->fails()) {
    		return response()->json(['error'=>$validator->errors()->all()]);
		}

		if($request->hasFile('profile_image'))
        {
            $filename = $request->profile_image->getClientOriginalName();
            $request->profile_image->storeAs('/public/upload',$filename);
        }

		$student = $request->all();
		$student['skills'] = implode(',', $request->skills);
		$student['profile_image'] = $request->profile_image->getClientOriginalName();

		Student::create($student);

		$students = Student::all();
		// return \DataTables::of(User::query())->make(true);
        return view('student.student_view',compact('students'));
    }
}