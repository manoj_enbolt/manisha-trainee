<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Worker extends Model
{
    protected $fillable = ['name', 'dob', 'salary','mobileno','image','department'];

    public function getDepartmentWiseWorker($user)
    {
    	return self::select('workers.*')
	    		->where('department','=',$user->department)
	    		->get();	
	    		
    }
}
